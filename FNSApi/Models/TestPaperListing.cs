﻿using System.Collections.Generic;

namespace FNSApi.Models
{
    public class TestPaperListing
    {
        public int id { get; set; }
        public string title { get; set; }
        public string thumbnail { get; set; }
        public string max_marks { get; set; }
        public string duration { get; set; }
        public string total_question { get; set; }
        public string date { get; set; }
        public string isAttempted { get; set; }
    }
}
