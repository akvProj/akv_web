﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FNSApi.Models
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    enum Pages
    {
        Home = 1,
        About = 2,
        Support = 3,
        Notices = 4,
        Personal = 5,
        OnlineTest = 6
    }
}