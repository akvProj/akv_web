﻿using System.Collections.Generic;

namespace FNSApi.Models
{
    public class QuestionModel
    {
        public int _id { get; set; }
        public string type { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public List<question> questions { get; set; }
    }

    public class question
    {
        public string name { get; set; }
        public string type { get; set; }
        public string title { get; set; }
        public string ques { get; set; }
        public string answer { get; set; }
        public List<choice> choices { get; set; }
    }

    public class choice
    {
        public string name { get; set; }
        public string title { get; set; }
        public string value { get; set; }
    }
}