﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using DAL;
using BLL;
using Core;
using FNSApi.Models;
using System.Drawing;
using System.IO;


namespace FNSApi.Controllers
{
    public class MOperationController : ApiController
    {
        private IRepository<tblStudent> _repository;

        public MOperationController()
        {
            this._repository = new Repository<tblStudent>();
        }

        public MOperationController(IRepository<tblStudent> repository)
        {
            this._repository = repository;
        }

        [HttpPost]
        public ResponseData Login(LoginViewModel model)
        {
            ResponseData res = new ResponseData();
            if (ModelState.IsValid)
            {
                try
                {
                    // TODO: Add insert logic here
                    tblStudent obj = _repository.GetAll().ToList().Where(x => x.Email == model.Email && x.Password == model.Password && x.IsActive == true).FirstOrDefault();//&& x.IsLoggedIn == false
                    if (obj != null)
                    {

                        obj.IsLoggedin = true;
                        obj.LastLoginDate = DateTime.Now;
                        _repository.Update(obj);

                        UserData objUserData = new UserData();
                        objUserData.user_id = obj.StudentID.ToString();
                        //res.coreRes = new APICoreRes { status = APICoreRes.ResStatus.Success,  userMessage = "Logged in successfully." };
                        res.status = ResStatus.Success;
                        res.userMessage = "Logged in successfully.";
                        res.Data = objUserData;
                        return res;
                    }
                    else
                    {

                        res.status = ResStatus.Fail;
                        res.userMessage = "Invalid credential.";
                        res.Data = string.Empty;
                        return res;
                    }
                }
                catch (Exception ex)
                {

                    res.status = ResStatus.Fail;
                    res.userMessage = "Invalid credential. Catch";
                    res.Data = ex.ToString();
                    return res;
                }
            }
            res.status = ResStatus.Fail;
            res.userMessage = "Invalid credential.";
            res.Data = string.Empty;
            return res;
        }

        [HttpPost]
        public ResponseData Logout(LoginViewModel model)
        {
            ResponseData res = new ResponseData();
            if (ModelState.IsValid)
            {
                try
                {
                    // TODO: Add insert logic here
                    tblStudent obj = _repository.GetAll().ToList().Where(x => x.Email == model.Email && x.Password == model.Password && x.IsActive == true).FirstOrDefault();//&& x.IsLoggedIn == false
                    if (obj != null)
                    {

                        obj.IsLoggedin = false;
                        _repository.Update(obj);


                        res.status = ResStatus.Success;
                        res.userMessage = "Logout successfully.";
                        res.Data = string.Empty;
                        return res;
                    }
                    else
                    {

                        res.status = ResStatus.Fail;
                        res.userMessage = "Invalid credential.";
                        res.Data = string.Empty;
                        return res;
                    }
                }
                catch (Exception ex)
                {
                    res.status = ResStatus.Fail;
                    res.userMessage = "Invalid credential.";
                    res.Data = string.Empty;
                    return res;
                }
            }
            res.status = ResStatus.Fail;
            res.userMessage = "Invalid credential.";
            res.Data = string.Empty;
            return res;
        }

        [HttpGet]
        public ResponseData GetPageData(string id)
        {
            ResponseData res = new ResponseData();
            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                switch (id)
                {
                    case ("home"):
                        data.Add(id, "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).||There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.");
                        break;
                    case ("about"):
                        data.Add(id, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum||Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.");
                        break;
                    case ("support"):
                        data.Add(id, "Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?||But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful");
                        break;
                    case ("term"):
                        data.Add(id, "Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?||At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga");
                        break;
                    case ("contact"):
                        data.Add(id, "Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.||On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour");
                        break;
                    default:
                        data.Add(id, "Informally the role of teacher may be taken on by anyone (e.g. when showing a colleague how to perform a specific task). In some countries, teaching young people of school age may be carried out in an informal setting, such as within the family (homeschooling), rather than in a formal setting such as a school or college. Some other professions may involve a significant amount of teaching (e.g. youth worker, pastor)||Teaching is a highly complex activity.[2] This is in part because teaching is a social practice, that takes place in a specific context (time, place, culture, socio-political-economic situation etc.) and therefore reflects the values of that specific context.[3] Factors that influence what is expected (or required) of teachers include history and tradition, social views about the purpose of education, accepted theories about learning");
                        break;
                }

                res.status = ResStatus.Success;
                res.userMessage = "Successful";
                res.Data = data;
                return res;
            }
            catch (Exception ex)
            {

                res.status = ResStatus.Fail;
                res.userMessage = "Fail to get data";
                res.Data = string.Empty;
                return res;
            }
        }

        [HttpGet]
        public ResponseData GetProfileData(string id)
        {
            ResponseData res = new ResponseData();
            try
            {
                tblStudent obj = _repository.GetAll().ToList().Where(x => x.UserName == id && x.IsActive == true).FirstOrDefault();//&& x.IsLoggedIn == false
                if (obj != null)
                {
                    ProfileData objProfileData = new ProfileData();

                    UserInfo objUserInfo = new UserInfo();
                    objUserInfo.user_id = obj.UserName;
                    objUserInfo.name = obj.StudentName;
                    objUserInfo.mailid = obj.Email;
                    objUserInfo.contact_no = obj.MobileNo == null ? "" : obj.MobileNo;
                    objUserInfo.address = obj.MailingAddress == null ? "" : obj.MailingAddress;
                    objUserInfo.dob = obj.DOB == null ? "" : obj.DOB;
                    objUserInfo.is_student_from_kiosk = false;//need to work
                    objUserInfo.profile_icon = "";//need to work

                    objProfileData.userInfo = objUserInfo;

                    ExamProfile ObjExamProfile = new ExamProfile();
                    ObjExamProfile.AKVRank = 0;
                    ObjExamProfile.PointsEarned = 0;
                    ObjExamProfile.TestPassed = 0;
                    ObjExamProfile.TotalTestAttempted = 0;

                    objProfileData.examProfile = ObjExamProfile;

                    NotificationOption objNotificationOption = new NotificationOption();
                    objNotificationOption.isallowEmailNotification = false;
                    objNotificationOption.isallowPhoneNotification = false;

                    objProfileData.notificationOption = objNotificationOption;

                    res.status = ResStatus.Success;
                    res.userMessage = "User profile fetched successfully.";
                    res.Data = objProfileData;
                    return res;
                }
                else
                {

                    res.status = ResStatus.Fail;
                    res.userMessage = "Invalid Userid.";
                    res.Data = string.Empty;
                    return res;
                }
            }
            catch (Exception ex)
            {

                res.status = ResStatus.Fail;
                res.userMessage = "Fail to get data";
                res.Data = string.Empty;
                return res;
            }
        }

        [HttpPost]
        public ResponseData RegisterStudent(StudentBasicModel objStudent)
        {
            ResponseData res = new ResponseData();
            try
            {
                if (ModelState.IsValid)
                {
                    tblStudent obj = _repository.GetAll().ToList().Where(x => x.Email == objStudent.Email).FirstOrDefault();
                    if (obj != null)
                    {

                        res.status = ResStatus.ValidationError;
                        res.userMessage = "Email already exists";
                        res.Data = objStudent;
                        return res;
                    }

                    string[] saAllowedCharacters = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };

                    tblStudent objStudentModel = new tblStudent();
                    objStudentModel.StudentName = objStudent.Name;

                    objStudentModel.MobileNo = objStudent.MobileNo;
                    objStudentModel.DOB = objStudent.DOB;

                    objStudentModel.CreatedBy = Convert.ToInt32(1);
                    objStudentModel.UserName = Guid.NewGuid().ToString().Replace("-", "").Substring(12);
                    objStudentModel.Email = objStudent.Email;
                    objStudentModel.Password = objStudent.Password;//CommonUtility.GenerateRandomOTP(8, saAllowedCharacters);
                    objStudentModel.IsActive = true;

                    _repository.Insert(objStudentModel);

                    res.status = ResStatus.Success;
                    res.userMessage = "Successful";
                    res.Data = objStudent;
                    return res;
                }

                res.status = ResStatus.ValidationError;
                res.userMessage = "Values are invalid";
                res.Data = objStudent;
                return res;
            }
            catch
            {

                res.status = ResStatus.Fail;
                res.userMessage = "Fail";
                res.Data = objStudent;
                return res;
            }
        }


        [HttpGet]
        public ResponseData GetQuestions(int testPaperId)
        {
            ResponseData res = new ResponseData();
            try
            {

                FinanceEduEntities context = new FinanceEduEntities();

                var ResultData = context.SP_QuestionMaster(testPaperId).ToList();

                QuestionModel data = new QuestionModel();
                data._id = ResultData.Select(z => z.TestPaperId).FirstOrDefault();
                data.type = ResultData.Select(z => z.Title).FirstOrDefault();
                data.title = ResultData.Select(z => z.TestPaperTitle).FirstOrDefault();
                data.description = ResultData.Select(z => z.TestPaperDescription).FirstOrDefault();

                List<question> questions = new List<question>();

                var ResultQuestions = ResultData.Select(p => new { p.QId, p.QuestionLevelID, p.QueTitle, p.ExtraNotesDescription }).Distinct().ToList();
                foreach (var ques in ResultQuestions)
                {
                    question objquestion = new question();
                    objquestion.name = Convert.ToString(ques.QId);
                    objquestion.type = "choice";
                    objquestion.title = ques.QueTitle;
                    objquestion.ques = ques.ExtraNotesDescription;
                    objquestion.answer = "";

                    List<choice> choices = new List<choice>();

                    var ResultChoice = ResultData.Where(z => z.QId == ques.QId).Select(z => new { z.options, z.value }).ToList();
                    foreach (var cho in ResultChoice)
                    {
                        if (!string.IsNullOrEmpty(cho.value))
                        {
                            choice objchoice = new choice();
                            objchoice.name = cho.value;
                            objchoice.title = cho.value;
                            objchoice.value = cho.options;
                            choices.Add(objchoice);
                        }
                    }

                    objquestion.choices = choices;
                    questions.Add(objquestion);
                }

                data.questions = questions;

                res.status = ResStatus.Success;
                res.userMessage = "Successful";
                res.Data = data;
                return res;

            }
            catch (Exception ex)
            {
                res.status = ResStatus.Fail;
                res.userMessage = "Fail to get data";
                return res;
            }
        }
        [HttpGet]
        public ResponseData GetTestPaperListing(int pageNumber, int studentId)
        {
            ResponseData res = new ResponseData();
            try
            {
                FinanceEduEntities context = new FinanceEduEntities();
                System.Data.Objects.ObjectParameter myOutputParamInt = new System.Data.Objects.ObjectParameter("TotalRecords", typeof(Int32));
                var ResultData = context.SP_GetTestPapers(pageNumber, 10, studentId, myOutputParamInt);

                List<TestPaperListing> objListings = new List<TestPaperListing>();
                foreach (var item in ResultData)
                {
                    TestPaperListing objListing = new TestPaperListing();
                    objListing.id = item.ID;
                    objListing.title = item.Title;
                    objListing.thumbnail = SiteSettings.SiteDomainName + "images/questionImg.png";
                    objListing.max_marks = Convert.ToString(item.MaxMarks);
                    objListing.duration = Convert.ToString(item.TestTimeinMinuts);
                    objListing.total_question = Convert.ToString(item.QuestionLimit);
                    objListing.date = Convert.ToString(item.Created);
                    objListing.isAttempted = item.isAttempted == 0 ? "false" : "true";

                    objListings.Add(objListing);
                }

                res.status = ResStatus.Success;
                res.userMessage = "Quiz list fetched successfully.";
                res.totalResults = Convert.ToString(Convert.ToInt32(myOutputParamInt.Value));
                res.Data = objListings;
                return res;
            }
            catch (Exception ex)
            {
                res.status = ResStatus.Fail;
                res.userMessage = ex.ToString();
                return res;
            }
        }
        private Image LoadImage(byte[] bytes)
        {
            //data:image/gif;base64,
            //this image is a single pixel (black)
            //byte[] bytes = Convert.FromBase64String("R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==");

            //TODO If this not works then we need to check this link.
            //https://forums.asp.net/t/2108893.aspx?c+Convert+base64+to+jpg+image+and+save

            Image image;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                image = Image.FromStream(ms);
            }

            return image;
        }

        public List<tblStudent> Get()
        {
            return _repository.GetAll().ToList();
        }

        public tblStudent Post(tblStudent usermaster)
        {
            return _repository.Insert(usermaster);
        }
    }
}
