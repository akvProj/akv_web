﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using DAL;
using BLL;


namespace FNSApi.Controllers
{
    public class AccountController : ApiController
    {
        private IRepository<tblUser> _repository;

        public AccountController()
        {
            this._repository = new Repository<tblUser>();
        }

        public AccountController(IRepository<tblUser> repository)
        {
            this._repository = repository;
        }
        public List<tblUser> Get()
        {

            return _repository.GetAll().ToList();

        }

        public tblUser Post(tblUser usermaster)
        {
            return _repository.Insert(usermaster);
        }
    }
}
