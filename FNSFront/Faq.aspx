﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Faq.aspx.cs" Inherits="FNSFront.Faq" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1.0, user-scalable=no">
    <title></title>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="fonts/styles.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/layout.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">

        <div class="wrapper">
            <header class="mobile-header navbar  visible-mobile-menu">
                <a class="navbar-brand" href="index.aspx">
                    <img src="images/logo.png" alt="" />
                </a>
                <div class="mobile-top-icons d-lg-none d-sm-block">
                    <div class="mobile-icon-sm "><span class="phone"><a href="tel:02890892714">
                        <img class="mr-2" src="images/icon_phone.svg" alt="" /></a></span></div>
                    <button type="button" class="navbar-toggle bar collapsed" data-toggle="collapse" data-target="" aria-expanded="false"></button>
                </div>
                <div class="nav-right logo-white" id="mobile-menu">
                    <a class="navbar-brand" href="index.aspx">
                        <img src="images/footer-logo.png" alt="" />
                    </a>
                    <button type="button" class="navbar-toggle navbar-toggle-close" data-toggle="collapse" data-target="" aria-expanded="false"></button>
                    <ul class="nav navbar-nav mobile-nav">

                        <li><a href="index.aspx">Home </a></li>

                        <li><a href="CourseOffered.aspx">Course Offered  </a></li>

                        <li><a href="index.aspx">Skills  </a></li>
                        <li><a href="studentcorner.aspx">Student Corner  </a></li>

                        <li><a href="index.aspx">News  </a></li>

                        <li><a href="contactus.aspx">Contact Us  </a></li>



                        <li class="nav-btn "><a href="KioskRegistration.aspx">Kiosk Register </a></li>
                        <li class="phone-nav"><a href="tel:87428 46490">
                            <img src="images/icon_phone_white.svg" alt="" />+91 87428 46490</a></li>
                    </ul>
                    <div class="clearfix"></div>
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-facebook-official"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="full-row footer-bottom">
                        <ul>
                            <div class="footer-bottom-menu"></div>
                            <li class="first">&copy;Copyright 2018 AKV Classes   </li>

                            <li><a href="#">Terms & Conditions </a></li>
                            <li><a href="#">Privacy Policy </a></li>
                            <li><a href="#">Cookies </a></li>
                        </ul>
                    </div>
                </div>
            </header>
            <nav class="navbar navbar-default navbar-expand-lg header-top hidden-mobile">
                <div class="container">
                    <div class="logo logo-desktop"><a href="index.aspx">
                        <img src="images/logo.png" alt="" />
                    </a></div>
                    <button id="navbar-toggle" class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                    <div class="collapse navbar-collapse js-navbar-collapse" id="navbarSupportedContent">
                        <div class="navbar-header">
                            <div class="logo-mobile logo"><a href="index.aspx">
                                <img src="images/footer-logo.svg" alt="" width="92">
                            </a></div>
                        </div>
                        <div class="navbar-custom">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item"><a class="nav-link" href="index.aspx"><span>Home </span></a></li>
                                <li class="nav-item"><a class="nav-link" href="CourseOffered.aspx"><span>Course Offered </span></a></li>
                                <li class="nav-item"><a class="nav-link" href="index.aspx"><span>Skills </span></a></li>
                                <li class="nav-item"><a class="nav-link" href="studentcorner.aspx"><span>Student Corner </span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="head-right d-none d-xl-block d-md-block">
                        <div class="phone">Speak to a venue specialist &nbsp; <b>+91 87428 46490</b></div>
                        <div class="right-menu">
                            <ul>
                                <li><a href="index.aspx"><span>News </span></a></li>
                                <li><a href="contactus.aspx"><span>Contact Us </span></a></li>
                                <li><a class="btn btn-primary btn-lg" href="KioskRegistration.aspx">Kiosk Register</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <section class="top full-banner over-hidden ">
                <div class=" row align-items-center  hero-banner text-center">
                    <div class=" col-lg-12">
                        <div class="align-self-center">
                            <h1>FAQs </h1>
                            <p>
                                With a little imagination (both ours and yours) we aim to create something unique,
          different and more  
          memorable for your business.
                            </p>
                        </div>
                    </div>
                </div>
                <div class=" full-img ">
                    <img src="images/banner-faq.jpg" alt="" />
                </div>
            </section>
            <div class=" row justify-content-center">
                <div class=" col-lg-10 col-sm-offset-1">
                    <div class="full-row imagin-better section-blue section-with-light-text text-center">
                        <h2>Imagine better </h2>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                            <br />
                            standard dummy text ever since the 1500s, when an unknown printer took a galley of type.
                        </p>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="full-row space80 no-space-mobile ">
                    <div class="visible-mobile block">
                        <div class="accordion section-gray-lighter " id="keyblock-acordian">
                            <div class="card">
                                <div class="card-header why-keyblock " id="one">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#Inspirational" aria-expanded="true" aria-controls="collapseOne">
                                            <i>
                                                <img src="images/group-icons/icon-inspiration.svg" alt="" />
                                            </i>
                                            <h3>Inspirational </h3>
                                        </button>
                                    </h5>
                                </div>
                                <div id="Inspirational" class="collapse show" aria-labelledby="one" data-parent="#keyblock-acordian">
                                    <div class="card-body why-keyblock">
                                        <p>Take time out from the office, in an inspirational setting. Why not take a ten minute tour of our galleries during the coffee break to refuel body and mind? </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header why-keyblock " id="two">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#creativity" aria-expanded="true" aria-controls="collapseOne">
                                            <i>
                                                <img src="images/group-icons/icon_creativity.svg" alt="" />
                                            </i>
                                            <h3>Creativity </h3>
                                        </button>
                                    </h5>
                                </div>
                                <div id="creativity" class="collapse" aria-labelledby="two" data-parent="#keyblock-acordian">
                                    <div class="card-body why-keyblock">
                                        <p>Take time out from the office, in an inspirational setting. Why not take a ten minute tour of our galleries during the coffee break to refuel body and mind? </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header why-keyblock " id="three">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#multipurpose" aria-expanded="true" aria-controls="collapseOne">
                                            <i>
                                                <img src="images/group-icons/icon_multipurpose.svg" alt="" />
                                            </i>
                                            <h3>Multipurpose </h3>
                                        </button>
                                    </h5>
                                </div>
                                <div id="multipurpose" class="collapse" aria-labelledby="three" data-parent="#keyblock-acordian">
                                    <div class="card-body why-keyblock">
                                        <p>Take time out from the office, in an inspirational setting. Why not take a ten minute tour of our galleries during the coffee break to refuel body and mind? </p>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="hidden-mobile">
                        <div class="row ">
                            <div class="col-lg-4">
                                <div class="full-row section-gray-lighter why-keyblock">
                                    <i>
                                        <img src="images/group-icons/icon-inspiration.svg" alt="" />
                                    </i>
                                    <h3>Inspirational </h3>
                                    <p>Take time out from the office, in an inspirational setting. Why not take a ten minute tour of our galleries during the coffee break to refuel body and mind? </p>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="full-row section-gray-lighter why-keyblock">
                                    <i>
                                        <img src="images/group-icons/icon_creativity.svg" alt="" />
                                    </i>
                                    <h3>Creativity </h3>
                                    <p>Take time out from the office, in an inspirational setting. Why not take a ten minute tour of our galleries during the coffee break to refuel body and mind? </p>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="full-row section-gray-lighter why-keyblock">
                                    <i>
                                        <img src="images/group-icons/icon_multipurpose.svg" alt="" />
                                    </i>
                                    <h3>Multipurpose </h3>
                                    <p>Take time out from the office, in an inspirational setting. Why not take a ten minute tour of our galleries during the coffee break to refuel body and mind? </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="full-row event-type-section ">
                    <h2>We Deals In </h2>
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                        <br />
                        standard dummy text ever since the 1500s, when an unknown printer took a galley of type.
                    </p>
                    <ul>
                        <li><i class="fa fa-check"></i><span>Education </span></li>
                        <li><i class="fa fa-check"></i><span>Product Launches </span></li>
                        <li><i class="fa fa-check"></i><span>Fashion Shows </span></li>
                        <li><i class="fa fa-check"></i><span>Awards Cermony </span></li>
                        <li><i class="fa fa-check"></i><span>Private Film Screening </span></li>
                        <li><i class="fa fa-check"></i><span>Rehearsal Space </span></li>

                    </ul>
                </div>
            </div>

            <div class="container faq ">
                <h2>FAQs </h2>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    <br />
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.
                </p>
                <span class="d-lg-none viewall d-xl-none d-done d-sm-block  d-md-block"><a href="#">VIEW ALL </a></span>
                <div class="accordion " id="faq">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">What are the advantages of Us ? </button>
                            </h5>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#faq">
                            <div class="card-body">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </p>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">What are some of the unique experiences you can create for my group? </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </p>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">What kinds of event branding opportunities are available? </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </p>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingfour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapsefour" aria-expanded="false" aria-controls="collapsefour">Do you have a donation program? </button>
                            </h5>
                        </div>
                        <div id="collapsefour" class="collapse" aria-labelledby="headingfour" data-parent="#faq">
                            <div class="card-body">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </p>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingfive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapsefive" aria-expanded="false" aria-controls="collapsefive">When can I expect to receive a response to my inquiry? </button>
                            </h5>
                        </div>
                        <div id="collapsefive" class="collapse" aria-labelledby="headingfive" data-parent="#faq">
                            <div class="card-body">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </p>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingsix">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapsesix" aria-expanded="false" aria-controls="collapsesix">What are your standard meeting room amenities? </button>
                            </h5>
                        </div>
                        <div id="collapsesix" class="collapse" aria-labelledby="headingsix" data-parent="#faq">
                            <div class="card-body">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </p>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingseven">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseseven" aria-expanded="false" aria-controls="collapseseven">What are your floor load limits? </button>
                            </h5>
                        </div>
                        <div id="collapseseven" class="collapse" aria-labelledby="headingseven" data-parent="#faq">
                            <div class="card-body">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </p>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <div class=" d-lg-none d-xl-none  d-done d-sm-block  d-md-block ">
                <div class="back-to-top ScrollTop"><a href="#">
                    <img src="images/up-icon.svg" alt="" width="40" />
                </a></div>
            </div>
            <div class="d-none d-lg-block d-xl-block scroll-position">
                <div class="back-to-top ScrollTop"><a href="#">
                    <img src="images/up-icon.svg" alt="" width="40" /></a></div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="footer-logo col-lg-3 "><a href="index.aspx">
                        <img src="images/footer-logo.png" alt="" /></a> </div>
                    <div class=" col-md-3 ">
                        <h3>QUICK LINK </h3>
                        <ul class="footer-link">
                            <li><a href="index.aspx">Home </a></li>

                            <li><a href="CourseOffered.aspx">Course Offered  </a></li>

                            <li><a href="index.aspx">Skills  </a></li>

                            <li><a href="index.aspx">News  </a></li>

                            <li><a href="contactus.aspx">Contact Us  </a></li>


                        </ul>
                    </div>
                    <div class=" col-lg-3  address-block">
                        <h3>ADDRESS  </h3>

                        <p class="address">
                        AKV classes, 3rd floor, Malwa Business Center, Sawant Singh Chouraha, Nimbahera, District: Chittorgarh(Raj.) 312601
                        </p>

                        <p class="phone-n">+91 87428 46490 </p>
                        <p class="email-id">info@akvclasses.com </p>



                    </div>
                    <div class="col-lg-3  text-right">
                        <h3>Contact Us  </h3>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3626.9748644687766!2d74.6831603!3d24.6245512!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x396616d33dcf4075%3A0x88f52458e1a7cf0e!2sAKV+CLASSES!5e0!3m2!1sen!2sin!4v1537990676066" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>


                    </div>
                </div>
                <div class="row">
                    <div class="copyright col-sm-6 ">
                        <div class="footer-bottom-menu">&copy;Copyright 2018 All Rights Reserved AKV Classes   </div>
                    </div>
                    <div class=" col-sm-6  text-right">
                        <ul class="social footer-social">
                            <li><a href="#"><i class="fa fa-facebook-official fa-2x"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram fa-2x"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter fa-2x"></i></a></li>
                        </ul>


                    </div>
                </div>
            </div>
        </footer>

        <script src="js/jquery-3.3.1.min.js"></script>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.reject.min.js"></script>
        <script src="js/support.js"></script>
    </form>
</body>
</html>
