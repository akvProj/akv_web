﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StudentCorner.aspx.cs" Inherits="FNSFront.StudentCorner" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <!-- Required meta tags -->
<meta charset="utf-8">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1.0, user-scalable=no">
<title></title>
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="fonts/styles.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="text-change/morphext.css" rel="stylesheet">
<link href="css/owl.carousel.min.css" rel="stylesheet">
<link href="css/mCustomScrollbar.css" rel="stylesheet">

<link href="css/layout.css" rel="stylesheet">

<link href="css/responsive.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
  
<div class="wrapper">
    <header class="mobile-header navbar  visible-mobile-menu"> <a class="navbar-brand" href="index.aspx"> <img src="images/logo.png" alt="" /> </a>
    <div class="mobile-top-icons d-lg-none d-sm-block">
      <div class="mobile-icon-sm "><span class="phone"> <a href="tel:02890892714"><img class="mr-2" src="images/icon_phone.svg" alt="" /></a></span></div>
      <button type="button" class="navbar-toggle bar collapsed" data-toggle="collapse" data-target="" aria-expanded="false"> </button>
    </div>
    <div class="nav-right logo-white" id="mobile-menu"> <a class="navbar-brand" href="index.aspx"> <img src="images/footer-logo.png" alt="" /> </a>
      <button type="button" class="navbar-toggle navbar-toggle-close" data-toggle="collapse" data-target="" aria-expanded="false"> </button>
      <ul class="nav navbar-nav mobile-nav">
	  
	   <li ><a href="index.aspx" > Home </a></li>

  <li ><a href="CourseOffered.aspx" > Course Offered  </a></li>

  <li ><a href="index.aspx" > Skills  </a></li>
          <li><a href="studentcorner.aspx">Student Corner  </a></li>

  <li ><a href="index.aspx" > News  </a></li>

  <li ><a href="contactus.aspx" > Contact Us  </a></li>
  
  
     
        <li class="nav-btn "><a href="KioskRegistration.aspx">Kiosk Register </a></li>
        <li class="phone-nav"><a href="tel:87428 46490"> <img src="images/icon_phone_white.svg" alt=""/>+91 87428 46490</a></li>
      </ul>
      <div class="clearfix"></div>
      <ul class="social">
        <li> <a href="#"> <i class="fa fa-facebook-official"> </i> </a> </li>
        <li> <a href="#"> <i class="fa fa-instagram"> </i> </a> </li>
        <li> <a href="#"> <i class="fa fa-youtube-play"> </i> </a> </li>
      </ul>
      <div class="clearfix"></div>
      <div class="full-row footer-bottom">
        <ul>
		        <div class="footer-bottom-menu"> </div>
          <li class="first"> &copy;Copyright 2018 AKV Classes   </li>
         
          <li> <a href="#"> Terms & Conditions </a> </li>
          <li> <a href="#">Privacy Policy </a> </li>
          <li> <a href="#"> Cookies </a> </li>
        </ul>
      </div>
    </div>
  </header>
  <nav class="navbar navbar-default navbar-expand-lg header-top hidden-mobile">
    <div class="container">
      <div class="logo logo-desktop"><a href="index.aspx"><img src="images/logo.png" alt="" /> </a></div>
      <button id="navbar-toggle" class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <div class="collapse navbar-collapse js-navbar-collapse" id="navbarSupportedContent">
        <div class="navbar-header">
          <div class="logo-mobile logo"><a href="index.aspx"><img src="images/footer-logo.svg" alt="" width="92"> </a></div>
        </div>
        <div class="navbar-custom">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item"><a class="nav-link" href="index.aspx"> <span> Home </span></a></li>
            <li class="nav-item"><a class="nav-link" href="CourseOffered.aspx"><span> Course Offered </span></a></li>
            <li class="nav-item"><a class="nav-link" href="index.aspx"> <span> Skills </span></a></li>
            <li class="nav-item"><a class="nav-link" href="studentcorner.aspx"> <span> Student Corner </span></a></li>
          </ul>
        </div>
      </div>
      <div class="head-right d-none d-xl-block d-md-block">
        <div class="phone">Speak to a venue specialist &nbsp; <b>+91 87428 46490</b></div>
        <div class="right-menu">
          <ul>
            <li><a href="index.aspx"><span> News </span> </a></li>
            <li><a href="contactus.aspx"><span> Contact Us </span> </a></li>
            <li><a class="btn btn-primary btn-lg" href="KioskRegistration.aspx">Kiosk Register</a></li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
<section class="top full-banner over-hidden ">
     <div class="container">
    <div class=" full-img "> <img src="images/student-corner.jpg" alt="" /> </div>
	 </div>
  </section>
  
  <div class="container">
  <div class=" row justify-content-center">
    <div class=" col-lg-10 col-sm-offset-1">
      <div class="full-row imagin-better section-orange section-with-light-text text-center">
        <h2>STUDENT CORNER </h2>
       
	   <ul class="nav-seems-tab"> 
	   <li> <a href="#what-student-says" class="takeme-toposition"> WHAT STUDENT SAYS </a> </li> 
	   <li> <a href="#latest-course"  class="takeme-toposition"> LATEST COURSE </a> </li> 
	   <li> <a href="#latest-notification"  class="takeme-toposition"> LATEST NOTIFICATION </a> </li> 
	   
	   </ul>
	   
      </div>
    </div>
  </div>
  </div>

  
    </div>
  <div class="full-row curv-blue-right space-100" id="what-student-says">

    <h2 class="text-center"> What Student Says  </h2>
	<p  class="text-center">Latest Testimonials. </p>
	  <div class="full-row bg-gray testimonial-section">
	<div class="container">
	<div id="student-testimonial" class="carousel slide" data-ride="carousel">

  <div class="carousel-inner">
      <div class="carousel-item active">
        <div class="row">
    
              <div class="col-sm-4 text-center "> <img src="images/student-testimonial.jpg"> </div>
              <div class="col-sm-8">
                <div class="caption">
                  <p class=" lead adjust2"> <span class="text-info"> Great place. </span>  <br/>
                किसी भी प्रतियोगिता परीक्षा कि तयारी के लिए मुझे घर से दूर जाना पड़ता था क्योंकि यहाँ (निम्बाहेडा ) में कोई भी अच्छी कोचिंग संस्था नही थी लकिन  जब से AKV classes निम्बहेडा में आया, मुझे घर से बहार जाने की जरुरत नही पड़ी क्योंकि यह संस्था अपने अप में ही सम्पूर्ण हैं  यहाँ के सब शिक्षक hard working हैं , study materials भी बहुत heplful हैं यहाँ के.

Thanks AKV Classes for giving  your valuable service to me
                  </p>
                  <blockquote class="adjust2">
                    <h4> Richa Nayak (IInd grade (science)) </h4>
                    <%--<small><cite title="Source Title"><i class="glyphicon glyphicon-globe"></i> www.example1.com</cite></small> --%></blockquote>
                </div>
           
          </div>
        </div>
      </div>
       <div class="carousel-item">
        <div class="row">
    
              <div class="col-sm-4 text-center "> <img src="images/student-testimonial.jpg"> </div>
              <div class="col-sm-8">
                <div class="caption">
                  <p class=" lead adjust2"> <span class="text-info"> This is great institute. </span>  <br/>
                सभी विषयों की अलग – अलग शिक्षक द्वारा coaching सिर्फ akv classes में ही होती हैं. </p>
                  <blockquote class="adjust2">
                    <h4> 	महेश कुमावत   (10th (RBSE))</h4>
                    <%--<small><cite title="Source Title"><i class="glyphicon glyphicon-globe"></i> www.example1.com</cite></small> --%></blockquote>
                </div>
           
          </div>
        </div>
      </div>
       <div class="carousel-item">
        <div class="row">
    
              <div class="col-sm-4 text-center "> <img src="images/student-testimonial.jpg"> </div>
              <div class="col-sm-8">
                <div class="caption">
                  <p class=" lead adjust2"> <span class="text-info"> This is great institute. </span>  <br/>
                Glad to join and gain an extra ordinary knowledge from experience faculty of AKV Classes.  </p>
                  <blockquote class="adjust2">
                    <h4> Mohsina ali   (IInd grade (English) )</h4>
                    <%--<small><cite title="Source Title"><i class="glyphicon glyphicon-globe"></i> www.example1.com</cite></small> --%></blockquote>
                </div>
           
          </div>
        </div>
      </div>
       <div class="carousel-item">
        <div class="row">
    
              <div class="col-sm-4 text-center "> <img src="images/student-testimonial.jpg"> </div>
              <div class="col-sm-8">
                <div class="caption">
                  <p class=" lead adjust2"> <span class="text-info"> This is great institute. </span>  <br/>
                हिंदी विषय की ऐसी अनुभवी शिक्षक के द्वारा शिक्षण मैंने अपनी लाइफ में नही ली थी, Thanks Mam.
.  </p>
                  <blockquote class="adjust2">
                    <h4>	प्रवेशिका जोशी (हिंदी)</h4>
                    <%--<small><cite title="Source Title"><i class="glyphicon glyphicon-globe"></i> www.example1.com</cite></small> --%></blockquote>
                </div>
           
          </div>
        </div>
      </div>
    <div class="carousel-item">
        <div class="row">
    
              <div class="col-sm-4 text-center "> <img src="images/student-testimonial.jpg"> </div>
              <div class="col-sm-8">
                <div class="caption">
                  <p class=" lead adjust2"> <span class="text-info"> This is great institute. </span>  <br/>
                बोर्ड (12th) परीक्षा को अच्छी तरह से पास करने के लिए AKV CLASSES और संस्था के शिक्षको ने मेरी बहुत मदद की.</p>
                  <blockquote class="adjust2">
                    <h4> अनिल कुमार (12th arts (RBSE)) </h4>
                    <%--<small><cite title="Source Title"><i class="glyphicon glyphicon-globe"></i> www.example1.com</cite></small> --%></blockquote>
                </div>
           
          </div>
        </div>
      </div>
	  
	    <div class="carousel-item">
        <div class="row">
    
              <div class="col-sm-4 text-center "> <img src="images/student-testimonial.jpg"> </div>
              <div class="col-sm-8">
                <div class="caption">
                  <p class=" lead adjust2"> <span class="text-info"> I got the success. </span>  <br/>
                AKV CLASSES के बेहतर CRASH COURSE (1 (1 )/(2 )  महिना ) के कारण ही में BSTC के exam में pass out हो सकी.</p>
                  <blockquote class="adjust2">
                    <h4> 	नीलिमा जोशी  (BSTC) </h4>
                    <%--<small><cite title="Source Title"><i class="glyphicon glyphicon-globe"></i> www.example1.com</cite></small> --%></blockquote>
                </div>
           
          </div>
        </div>
      </div>
	  
    <div class="carousel-item">
        <div class="row">
    
              <div class="col-sm-4 text-center "> <img src="images/student-testimonial.jpg"> </div>
              <div class="col-sm-8">
                <div class="caption">
                  <p class=" lead adjust2"> <span class="text-info"> Happy with AKV classes. </span>  <br/>
                संस्था के study materials हर तरह की परीक्षा के लिए बहुत ही helpful हैं</p>
                  <blockquote class="adjust2">
                    <h4> 	प्रकाश गोस्वामी  (राज. कांस्टेबल ) </h4>
                    <%--<small><cite title="Source Title"><i class="glyphicon glyphicon-globe"></i> www.example1.com</cite></small> --%></blockquote>
                </div>
           
          </div>
        </div>
      </div>
   
     
    </div>

    <ol class="carousel-indicators">
    <li data-target="#student-testimonial" data-slide-to="0" class="active"> <i class="fa fa-graduation-cap"></i></li>
    <li data-target="#student-testimonial" data-slide-to="1"><i class="fa fa-graduation-cap"></i></li>
    <li data-target="#student-testimonial" data-slide-to="2"><i class="fa fa-graduation-cap"></i></li>
        <li data-target="#student-testimonial" data-slide-to="3"><i class="fa fa-graduation-cap"></i></li>
    <li data-target="#student-testimonial" data-slide-to="4"><i class="fa fa-graduation-cap"></i></li>
        <li data-target="#student-testimonial" data-slide-to="5"><i class="fa fa-graduation-cap"></i></li>
    <li data-target="#student-testimonial" data-slide-to="6"><i class="fa fa-graduation-cap"></i></li>
  </ol>
    </div>

	
	 </div>
	 </div>
	 </div>
  
  <div class="wrapper">
    <div class=" container explore-spaces-block space-80" id="latest-course">
  
  <h2> Latest Courses   </h2>
  
  
    <div class="card-group row">
      <div class="col-md-4 col-lg-3">
        <div class="card gray-space-block ">
          <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
          <div class="card-body">
                                <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>8 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>20 </strong></li>
                                </ul>
                                <h5 class="card-title">RAS pre   </h5>

                                <p class="card-text">
                                    RAS के लिए होने वाले परीक्षा का आयोजन राजस्थान लोक सेवा आयोग (RPSC) द्वारा किया जाता है, यह राजस्थान सरकार का एक संगठन है जिसका संचालन राजस्थान सरकार के द्वारा किया जाता है | RAS अधिकारियों ने उप-जिला स्तर पर विभिन्न पदों की व्यवस्था की है जबकि राजस्व प्रशासन और कानून-व्यवस्था के रखरखाव के अलावा विभिन्न सरकारी सेवाओं को वितरित किया है |
RAS पद की नियुक्ति के लिए राजस्थान सरकार के संगठन राजस्थान लोक सेवा आयोग के द्वारा तीन चरण में परीक्षा लिया जाता है | पहले चरण में Preliminary Examination होता है जो objective होता है इस परीक्षा में उत्तीर्ण होने वाले अभियार्थी को Mains Examination के लिए चयनित किया जाता है | Mains में कुल चार पेपर होता है जो subjective होता है | इस परीक्षा को उतीर्ण करने वाले अभियार्थी को Personal Interview के लिए एक तारीख एवं समय दिया जाता है जो उसका आखरी चयन परिक्रिया होता है | इस परीक्षा में उत्तीर्ण होने पर अभियार्थी को RAS का पद दिया जाता है |
                                </p>
                                <p class="card-text"><small class="text-muted"><a href="CourseOffered.aspx">View Detail </a></small></p>
                            </div>
        </div>
      </div>
	  
	        <div class="col-md-4 col-lg-3">
        <div class="card gray-space-block ">
          <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
          <div class="card-body">
             <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>4 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>30 </strong></li>
                                </ul>
                                <h5 class="card-title">PATWARI </h5>
                                <p class="card-text">
                                    RSMSSB पटवारी का चयन करता है पटवारी गांवों के स्तर पर एक सरकारी कर्मचारी होता है। जिसके क्षेत्र में एक या एक से ज्यादा गांव आते है। और पटवारी का काम इन गांवो की ज़मीनों का पूरा विवरण रखते है।
•	पटवारी राजस्व के records को Update रखता है।
•	किसी भी ज़मीन को खरीदना या बेचना ये दोनों ही काम पटवारी की सहायता से सम्पन्न होता है।
•	पटवारी भूमि का आवंटन यानि कि ज़मीनों का बंटवारा करता है।
•	पटवारी आपदाओं के दौरान, आपदा प्रबन्धन अभियानों में सक्रिय रूप से अपना सहयोग देता है।
•	पटवारी एक पीढ़ी से दूसरी पीढ़ी के खेतों के हस्तांतरण यानि कि किसी भी खेत के मालिक का नाम बदलना पटवारी का काम होता है।
•	पटवारी विकलांग पेंशन, वृद्धावस्था, आय और जाति प्रमाण पत्र (caste certificate) बनवाने में आवेदकों की सहायता करता हैं।
                                </p>
            <p class="card-text"><small class="text-muted"> <a href="CourseOffered.aspx"> View Detail </a> </small></p>
          </div>
        </div>
      </div>
	  
	        <div class="col-md-4 col-lg-3">
        <div class="card gray-space-block ">
          <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
          <div class="card-body">
           <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>3 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>20 </strong></li>
                                </ul>
                                <h5 class="card-title"> SSC GD CONSTABLE    </h5>
                                <p class="card-text">
                                    SSC GD कांस्टेबल कर्मचारी चयन आयोग, कर्मचारी चयन आयोग बीएसएफ, सीआईएसएफ, आईटीबीपी, सीआरपीएफ और एसएसबी और राइफलमैन (जनरल ड्यूटी) में उम्मीदवारों के सहभागिता (सामान्य कर्तव्य) के रूप में परीक्षा आयोजित करता है।
सीमा सुरक्षा बल (BSF), केंद्रीय औद्योगिक सुरक्षा बल (CISF), केंद्रीय रिजर्व पुलिस बल (CRPF), इंडो तिब्बती सीमा पुलिस (ITBP), सशस्त्र सीमा बल (SSB), राष्ट्रीय जांच एजेंसी (NIA), सचिवालय सुरक्षा बल (SSF) और असम राइफल्स में राइफलमैन उक्त सभी विभागो में (जनरल ड्यूटी) GD कांस्टेबल का कार्य होता है 

                                </p>
            <p class="card-text"><small class="text-muted"> <a href="CourseOffered.aspx"> View Detail </a> </small></p>
          </div>
        </div>
      </div>
	  
	        <div class="col-md-4 col-lg-3">
        <div class="card gray-space-block ">
          <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
          <div class="card-body">
            <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>4 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>30 </strong></li>
                                </ul>
                                <h5 class="card-title">BANK P.O  </h5>
                                <p class="card-text">प्रोबेशनरी ऑफिसर की नियुक्ति के लिए राष्ट्रीय स्तर की भर्ती परीक्षा।</p>
            <p class="card-text"><small class="text-muted"> <a href="CourseOffered.aspx"> View Detail </a> </small></p>
          </div>
        </div>
      </div>
  
 
   
 
    </div>
  </div>
 


    <section class="top full-banner curv-blue-left wow animate-bg-left news-section">
  



    <div class="container " id="latest-notification">
  
     
      
            <div class=" d-flex align-items-center row">
 
              <div class="col-lg-12 "> 
			  
			      <div class="section-orange newsbox box box-left-position white-text box-event">
         
          <h2 class="white-text">Latest Notification </h2>
          <div class="newsstand mCustomScrollbar ">
          <ul >
		  <li>
<h4> RAS (mains) </h4>
		  RAS (mains) के course की भी शिक्षा बहुत जल्दी  akv classes द्वारा करवाई जाएगी. 
</li>

 <li>
<h4>Application Launch</h4>
		  AKV CLASSES का नया मेहमान :- akv classes एप्लीकेशन , जो बहुत जल्दी  
  LAUNCH होने वाली हैं.  
 
</li>


 <li>
<h4> Moving for Future </h4>
		 300% वृद्धि हुए प्रतियोगिता परीक्षा के बच्चो में, साल 2017 के मुकाबले . 
</li>


 <li>
<h4> Education for all </h4>
		 ACADEMIC भी दोनों ही माध्यम(ENGLISH/ HINDI ) के लिए शुरू कर दिया गया हैं, बच्चो की वृद्धि को देख कर. 
</li>



		  </ul>
		  
		  
        </div>
			  
			  </div>
			  </div>
      
      </div>
        

     
    </div>
  </section>
 </div>
  
  
 



<footer>
  <div class=" d-lg-none d-xl-none  d-done d-sm-block  d-md-block ">
    <div class="back-to-top ScrollTop"> <a href="#"> <img src="images/up-icon.svg" alt="" width="40" /> </a> </div>
  </div>
  <div class="d-none d-lg-block d-xl-block scroll-position">
    <div class="back-to-top ScrollTop"><a href="#"><img src="images/up-icon.svg" alt="" width="40" /></a></div>
  </div>
  <div class="container">
    <div class="row">
      <div class="footer-logo col-lg-3 "> <a href="index.aspx"><img src="images/footer-logo.png" alt=""  /></a> </div>
      <div class=" col-md-3 ">
	  <h3>QUICK LINK </h3>
        <ul class="footer-link">
          <li ><a href="index.aspx" > Home </a></li>

  <li ><a href="CourseOffered.aspx" > Course Offered  </a></li>

  <li ><a href="index.aspx" > Skills  </a></li>

  <li ><a href="index.aspx" > News  </a></li>

  <li ><a href="contactus.aspx" > Contact Us  </a></li>


        </ul>
      </div>
      <div class=" col-lg-3  address-block">
         <h3>ADDRESS  </h3>
		 
		 <p class="address"> 
AKV classes, 3rd floor, Malwa Business Center, Sawant Singh Chouraha, Nimbahera, District: Chittorgarh(Raj.) 312601 </p>

		 <p class="phone-n"> +91 87428 46490 </p>
		 <p class="email-id">info@akvclasses.com </p>


		
      </div>
      <div class="col-lg-3  text-right">
          <h3>Contact Us  </h3>
		  <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3626.9748644687766!2d74.6831603!3d24.6245512!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x396616d33dcf4075%3A0x88f52458e1a7cf0e!2sAKV+CLASSES!5e0!3m2!1sen!2sin!4v1537990676066" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
		  
		  
      </div>
    </div>
    <div class="row">
      <div class="copyright col-sm-6 ">
        <div class="footer-bottom-menu">&copy;Copyright 2018 All Rights Reserved AKV Classes   </div>
      </div>
	   <div class=" col-sm-6  text-right">
	  <ul class="social footer-social">
          <li> <a href="#"><i class="fa fa-facebook-official fa-2x"></i></a></li>
          <li> <a href="#"><i class="fa fa-instagram fa-2x"></i></a></li>
          <li> <a href="#"><i class="fa fa-twitter fa-2x"></i></a></li>
        </ul>
		
		
    </div>
    </div>
  </div>
</footer>


<script src="js/jquery-3.3.1.min.js"></script> 

<!-- Optional JavaScript --> 
<!-- jQuery first, then Popper.js, then Bootstrap JS --> 
<script src="js/popper.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/wow/wow.js"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/mCustomScrollbar.concat.min.js"></script> 


<script src="js/jquery.reject.min.js"></script> 

<script src="js/support.js"></script> 
<script >
    $(document).ready(function () {
        wow = new WOW(
              {
                  boxClass: 'wow',      // default
                  animateClass: 'animated', // default
                  offset: 0,          // default
                  mobile: true,       // default
                  live: true        // default
              }
            )
        wow.init();


    });
</script>
    </form>
</body>
</html>
