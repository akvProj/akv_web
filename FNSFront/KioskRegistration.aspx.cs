﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using Core;
using System.Text.RegularExpressions;
using DAL;

namespace FNSFront
{
    public partial class KioskRegistration : System.Web.UI.Page
    {
        FinanceEduEntities dbContext = new FinanceEduEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) { fillState(); }
        }

        void fillState()
        {
            var list = dbContext.States.ToList();

            if (list.Count > 0)
            {
                drpState.DataSource = list;
                drpState.DataTextField = "Name";
                drpState.DataValueField = "ID";
                drpState.DataBind();
            }
            drpState.Items.Insert(0, new ListItem("Select State", "0"));

        }
        void fillCity(int StateID)
        {
            if (StateID > 0)
            {
                var list = dbContext.Cities.Where(x => x.StateID == StateID).ToList();

                drpCity.Items.Clear();
                if (list.Count > 0)
                {
                    drpCity.DataSource = list;
                    drpCity.DataTextField = "Name";
                    drpCity.DataValueField = "ID";
                    drpCity.DataBind();
                }

                drpCity.Items.Insert(0, new ListItem("Select City", "0"));
            }
            else
            {

            }
        }

        protected void drpState_SelectedIndexChanged(object sender, EventArgs e)
        {
            int StID = Convert.ToInt32(drpState.SelectedItem.Value);
            fillCity(StID);
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            lblMessage.Text = "";
            try
            {
                if (txtKioskName.Text.Trim().Length > 0 && txtEmail.Text.Trim().Length > 0 && txtPhoneNo.Text.Trim().Length > 0 && txtFirstName.Text.Trim().Length > 0 && txtAddress.Text.Trim().Length > 0 && txtPassword.Text.Trim().Length > 0 && txtConfirmPassword.Text.Trim().Length > 0)
                {
                    if (txtPassword.Text.Trim() == txtConfirmPassword.Text.Trim())
                    {

                        if (txtPassword.Text.Trim().Length < 6)
                        {
                            lblMessage.Text = "Password should have at least 6 char length.";
                            return;
                        }

                        string strEmailReg = @"^([\w-\.]+)@((\[[0-9]{2,3}\.[0-9]{2,3}\.[0-9]{2,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{2,3})(\]?)$";

                        if (Regex.IsMatch(txtEmail.Text.Trim(), strEmailReg))
                        {


                            #region save kiosk detail


                            tblUser objUsers = dbContext.tblUsers.Where(x => x.Email == txtEmail.Text.Trim()).FirstOrDefault();

                            if (objUsers == null)
                            {


                                tblUser objUser = new tblUser();

                                objUser.FirstName = txtFirstName.Text.Trim();
                                objUser.LastName = txtLastName.Text.Trim();
                                objUser.Email = txtEmail.Text.Trim();
                                objUser.LoginPassword = txtPassword.Text.Trim();

                                objUser.PhoneNo = txtPhoneNo.Text.Trim();
                                objUser.StateID = Convert.ToInt32(drpState.SelectedItem.Value);
                                objUser.CityID = Convert.ToInt32(drpCity.SelectedItem.Value);
                                objUser.CountryID = 1;
                                objUser.Address = txtAddress.Text.Trim();
                                objUser.Created = DateTime.Now;
                                objUser.UserTypeId = 2;
                                objUser.IsActive = false;
                                objUser.IsLoggedIn = false;

                                dbContext.tblUsers.Add(objUser);
                                dbContext.SaveChanges();

                                KioskMaster objKioskMaster = new KioskMaster();

                                objKioskMaster.UserId = objUser.ID;
                                objKioskMaster.KioskName = txtKioskName.Text.Trim();
                                objKioskMaster.KioskregistrationNO = "";//model.KioskRegistrationNO;
                                objKioskMaster.Address = txtAddress.Text.Trim();
                                objKioskMaster.SCIdentity = Convert.ToString(txtSCName.Text.Trim());
                                objKioskMaster.DCIdentity = Convert.ToString(txtDCName.Text.Trim());
                                objKioskMaster.PaymentTransactionID = Convert.ToString(txtPaymentTransactionID.Text.Trim());
                                objKioskMaster.Created = DateTime.Now;

                                dbContext.KioskMasters.Add(objKioskMaster);
                                dbContext.SaveChanges();

                                lblMessage.Text = "Requset has been send, we will contact you soon..";

                                txtKioskName.Text = "";
                                txtFirstName.Text = "";
                                txtLastName.Text = "";
                                txtEmail.Text = "";
                                txtPhoneNo.Text = "";
                                txtAddress.Text = "";
                                txtSCName.Text = "";
                                txtDCName.Text = "";
                                txtPaymentTransactionID.Text = "";

                            }
                            else { lblMessage.Text = "Email already exist."; }
                            #endregion


                        }
                        else
                        {
                            lblMessage.Text = "Invalid Email";
                        }
                    }
                    else { lblMessage.Text = "Password/Confirm password should be match."; }
                }
                else
                {
                    lblMessage.Text = "Please fill required fields.";
                }
            }
            catch (Exception ex) { }
            //TempData["Success"] = "Request send Successfully!";
        }


    }
}