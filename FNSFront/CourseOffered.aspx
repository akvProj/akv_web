﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CourseOffered.aspx.cs" Inherits="FNSFront.CourseOffered" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,shrink-to-fit=no, user-scalable=no" />
    <title>AKV Classes</title>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="fonts/styles.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/mCustomScrollbar.css" rel="stylesheet">
    <link href="css/layout.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <style type="text/css">
        sup {
            vertical-align: super;
            font-size: smaller;
        }

        .spnshowhide {
            cursor: pointer !important;
            color: blue;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">

        <div class="wrapper">
            <header class="mobile-header navbar  visible-mobile-menu">
                <a class="navbar-brand" href="index.aspx">
                    <img src="images/logo.png" alt="" />
                </a>
                <div class="mobile-top-icons d-lg-none d-sm-block">
                    <div class="mobile-icon-sm ">
                        <span class="phone"><a href="tel:02890892714">
                            <img class="mr-2" src="images/icon_phone.svg" alt="" /></a></span>
                    </div>
                    <button type="button" class="navbar-toggle bar collapsed" data-toggle="collapse" data-target="" aria-expanded="false"></button>
                </div>
                <div class="nav-right logo-white" id="mobile-menu">
                    <a class="navbar-brand" href="index.aspx">
                        <img src="images/footer-logo.png" alt="" />
                    </a>
                    <button type="button" class="navbar-toggle navbar-toggle-close" data-toggle="collapse" data-target="" aria-expanded="false"></button>
                    <ul class="nav navbar-nav mobile-nav">

                        <li><a href="index.aspx">Home </a></li>

                        <li><a href="courseoffered.aspx">Course Offered  </a></li>

                        <li><a href="index.aspx">Skills  </a></li>
                        <li><a href="studentcorner.aspx">Student Corner  </a></li>

                        <li><a href="index.aspx">News  </a></li>

                        <li><a href="contactus.aspx">Contact Us  </a></li>



                        <li class="nav-btn "><a href="KioskRegistration.aspx">Kiosk Register </a></li>
                        <li class="phone-nav"><a href="tel:87428 46490">
                            <img src="images/icon_phone_white.svg" alt="" />+91 87428 46490</a></li>
                    </ul>
                    <div class="clearfix"></div>
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-facebook-official"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="full-row footer-bottom">
                        <ul>
                            <div class="footer-bottom-menu"></div>
                            <li class="first">&copy;Copyright 2018 AKV Classes   </li>

                            <li><a href="#">Terms & Conditions </a></li>
                            <li><a href="#">Privacy Policy </a></li>
                            <li><a href="#">Cookies </a></li>
                        </ul>
                    </div>
                </div>
            </header>
            <nav class="navbar navbar-default navbar-expand-lg header-top hidden-mobile">
                <div class="container">
                    <div class="logo logo-desktop">
                        <a href="index.aspx">
                            <img src="images/logo.png" alt="" />
                        </a>
                    </div>
                    <button id="navbar-toggle" class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                    <div class="collapse navbar-collapse js-navbar-collapse" id="navbarSupportedContent">
                        <div class="navbar-header">
                            <div class="logo-mobile logo">
                                <a href="index.aspx">
                                    <img src="images/footer-logo.svg" alt="" width="92">
                                </a>
                            </div>
                        </div>
                        <div class="navbar-custom">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item"><a class="nav-link" href="index.aspx"><span>Home </span></a></li>
                                <li class="nav-item"><a class="nav-link" href="CourseOffered.aspx"><span>Course Offered </span></a></li>
                                <li class="nav-item"><a class="nav-link" href="index.aspx"><span>Skills </span></a></li>
                                <li class="nav-item"><a class="nav-link" href="StudentCorner.aspx"><span>Student Corner </span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="head-right d-none d-xl-block d-md-block">
                        <div class="phone">Speak to a venue specialist &nbsp; <b>+91 87428 46490</b></div>
                        <div class="right-menu">
                            <ul>
                                <li><a href="index.aspx"><span>News </span></a></li>
                                <li><a href="contactus.aspx"><span>Contact Us </span></a></li>
                                <li><a class="btn btn-primary btn-lg" href="KioskRegistration.aspx">Kiosk Register</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>


        </div>
        <div class="full-row curv-blue-right">

            <div class=" container explore-spaces-block space-80">

                <h2>Competitive Courses </h2>


                <div class="card-group row">
                    <div class="col-md-4 col-lg-3">
                        <div class="card gray-space-block ">
                            <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
                            <div class="card-body">
                                <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>8 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>20 </strong></li>
                                </ul>
                                <h5 class="card-title">RAS pre   </h5>
                               
                                 <p class="card-text" fulltext="RAS के लिए होने वाले परीक्षा का आयोजन राजस्थान लोक सेवा आयोग (RPSC) द्वारा किया जाता है, यह राजस्थान सरकार का एक संगठन है जिसका संचालन राजस्थान सरकार के द्वारा 
                                    किया जाता है| RAS अधिकारियों ने उप-जिला स्तर पर विभिन्न पदों की व्यवस्था की है जबकि राजस्व प्रशासन और कानून-व्यवस्था के रखरखाव के अलावा विभिन्न सरकारी सेवाओं को वितरित किया है |
RAS पद की नियुक्ति के लिए राजस्थान सरकार के संगठन राजस्थान लोक सेवा आयोग के द्वारा तीन चरण में परीक्षा लिया जाता है | पहले चरण में Preliminary Examination होता है जो objective होता है इस परीक्षा में उत्तीर्ण होने वाले अभियार्थी को Mains Examination के लिए चयनित किया जाता है |
                                         Mains में कुल चार पेपर होता है जो subjective होता है | इस परीक्षा को उतीर्ण करने वाले अभियार्थी को Personal Interview के लिए एक तारीख एवं समय दिया जाता है जो उसका आखरी चयन परिक्रिया होता है | 
                                        इस परीक्षा में उत्तीर्ण होने पर अभियार्थी को RAS का पद दिया जाता है |"
                                    lesstext="RAS के लिए होने वाले परीक्षा का आयोजन राजस्थान लोक सेवा आयोग (RPSC) द्वारा किया जाता है, यह राजस्थान सरकार का एक संगठन है जिसका संचालन राजस्थान सरकार के द्वारा..">

                                    RAS के लिए होने वाले परीक्षा का आयोजन राजस्थान लोक सेवा आयोग (RPSC) द्वारा किया जाता है, यह राजस्थान सरकार का एक संगठन है जिसका संचालन राजस्थान सरकार के द्वारा.. 
                                    
                                </p>

                                <span class="spnshowhide less">show more</span>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-3">
                        <div class="card gray-space-block ">
                            <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
                            <div class="card-body">
                                <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>3 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>30 </strong></li>
                                </ul>
                                <h5 class="card-title">LDC </h5>
                                <p class="card-text" fulltext="कर्मचारी चयन आयोग SSC (एसएससी) हर साल भारत में विभिन्न मंत्रालयों और विभागों के सरकारी कार्यालयों में विभिन्न पदों पर उम्मीदवारों की भर्ती करती है। ये पद विभिन्न श्रेणियों के होते हैं 
                                    जो उम्मीदवारों की योग्यता पर निर्भर करते हैं। लोअर डिवीजन क्लर्क (एलडीसी)पद भी विभिन्न विभागों में एसएससी द्वारा दिए गये पदों में से एक है। इन पदों पर भर्ती होने वाले उम्मीदवारों का काम मुख्य रूप से संबंधित विभाग के कार्यालय में पारंपरिक ऑफिस वर्क करना होता है। 
                                    एलडीसी का कार्य कार्यालय के दैनिक कार्यों को करना होता है और उन पर कार्यालय के प्रतिदिन के कार्य को  सहज व सक्षमता से कार्यान्वित करने की जिम्मेदारी होती है। उनको अपने आवंटित कार्यालयों में सरल कार्य ही दिया जाता है 
                                    और इसके अलावा, कार्यालय में  डेटा और दस्तावेजों को उचित स्थान पर बनाए रखने की जिम्मेदारी भी उनकी होती है। आपको संबंधित कार्यालय या विभाग के लिए डेटा पूल या अन्य किसी सरकारी रिकार्ड्स से जानकारी निकलने की ज़िम्मेदारी भी निभानी होती हैं।
                                     इसके अतिरिक्त, विभिन्न विभागों में काम करने वाले स्टाफ के मासिक वेतन या एरिअर तैयार करने, स्टाफ के रिकार्ड्स को रजिस्टर में बनाए रखने, उनकी मासिक छुट्टीयो को चिह्नित करने और कभी-कभी अपने वरिष्ठों के लिए महत्वपूर्ण दस्तावेजो और फाइलो को भेजने 
                                    और प्राप्त करने के लिए भी कहा जा सकता है|"
                                    lesstext="कर्मचारी चयन आयोग SSC (एसएससी) हर साल भारत में विभिन्न मंत्रालयों और विभागों के सरकारी कार्यालयों में विभिन्न पदों पर उम्मीदवारों की भर्ती करती है। ये पद विभिन्न श्रेणियों के होते हैं">
                                    कर्मचारी चयन आयोग SSC (एसएससी) हर साल भारत में विभिन्न मंत्रालयों और विभागों के सरकारी कार्यालयों में विभिन्न पदों पर उम्मीदवारों की भर्ती करती है। ये पद विभिन्न श्रेणियों के होते हैं 
                                   


                                </p>
                                <span class="spnshowhide less">show more</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-3">
                        <div class="card gray-space-block ">
                            <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
                            <div class="card-body">
                                <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>4 1/2 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>30 </strong></li>
                                </ul>
                                <h5 class="card-title">SI (SUB-INSPECTOR)  </h5>
                                <p class="card-text" fulltext="राजस्थान पुलिस में सब इंस्पेक्टर के पद पर भर्ती निकली जाती है सब-इंस्पेक्टर एक वर्ग III पद है, कुछ मामलों में पुलिस इंस्पेक्टर या डीएसपी को रिपोर्टिंग। वे यातायात शाखा, अपराध शाखा, दूरसंचार शाखा, फोरेंसिक विज्ञान प्रयोगशाला, योजना प्रशिक्षण और कल्याण शाखा, एटीएस शाखा, या राज्य सशस्त्र कंसबुलरी में काम कर सकते हैं।
कर्तव्यों में मुख्य रूप से कानून और व्यवस्था के रख-रखाव और ज्यादातर मामलों में अपराध की रोकथाम शामिल है। अक्सर कर्तव्य में वीआईपी सुरक्षा और भीड़ नियंत्रण शामिल होता है। पुलिस स्टेशन पर, वे सुनिश्चित करते हैं कि एफआईआर पंजीकृत हैं और पूछताछ और जांच का पालन किया जाता है। वे सबूत इकट्ठा करते हैं और अदालतों में आपराधिक मामलों को दर्ज करते हैं।
योग्यता -
किसी भी स्ट्रीम में डिग्री
देवनागरी लिपि में लिखे हिंदी का कामकाजी ज्ञानराजस्थानी संस्कृति का ज्ञान।"
                                    lesstext="राजस्थान पुलिस में सब इंस्पेक्टर के पद पर भर्ती निकली जाती है सब-इंस्पेक्टर एक वर्ग III पद है, कुछ मामलों में पुलिस इंस्पेक्टर या डीएसपी को रिपोर्टिंग। वे यातायात शाखा, अपराध शाखा, दूरसंचार शाखा, फोरेंसिक..">
                                    राजस्थान पुलिस में सब इंस्पेक्टर के पद पर भर्ती निकली जाती है सब-इंस्पेक्टर एक वर्ग III पद है, कुछ मामलों में पुलिस इंस्पेक्टर या डीएसपी को रिपोर्टिंग। वे यातायात शाखा, अपराध शाखा, दूरसंचार शाखा, फोरेंसिक.. 

                                    
                                </p>
                                <span class="spnshowhide less">show more</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-3">
                        <div class="card gray-space-block ">
                            <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
                            <div class="card-body">
                                <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>4 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>30 </strong></li>
                                </ul>
                                <h5 class="card-title">PATWARI </h5>
                                <p class="card-text" fulltext="RSMSSB पटवारी का चयन करता है पटवारी गांवों के स्तर पर एक सरकारी कर्मचारी होता है। जिसके क्षेत्र में एक या एक से ज्यादा गांव आते है। और पटवारी का काम इन गांवो की ज़मीनों का पूरा विवरण रखते है।
•	पटवारी राजस्व के records को Update रखता है।
•	किसी भी ज़मीन को खरीदना या बेचना ये दोनों ही काम पटवारी की सहायता से सम्पन्न होता है।
•	पटवारी भूमि का आवंटन यानि कि ज़मीनों का बंटवारा करता है।
•	पटवारी आपदाओं के दौरान, आपदा प्रबन्धन अभियानों में सक्रिय रूप से अपना सहयोग देता है।
•	पटवारी एक पीढ़ी से दूसरी पीढ़ी के खेतों के हस्तांतरण यानि कि किसी भी खेत के मालिक का नाम बदलना पटवारी का काम होता है।
•	पटवारी विकलांग पेंशन, वृद्धावस्था, आय और जाति प्रमाण पत्र (caste certificate) बनवाने में आवेदकों की सहायता करता हैं।" lesstext="RSMSSB पटवारी का चयन करता है पटवारी गांवों के स्तर पर एक सरकारी कर्मचारी होता है। जिसके क्षेत्र में एक या एक से ज्यादा गांव आते है। और पटवारी का काम इन गांवो की ज़मीनों का..">
                                    RSMSSB पटवारी का चयन करता है पटवारी गांवों के स्तर पर एक सरकारी कर्मचारी होता है। जिसके क्षेत्र में एक या एक से ज्यादा गांव आते है। और पटवारी का काम इन गांवो की ज़मीनों का..
                                </p>
                                <span class="spnshowhide less">show more</span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="card-group row">
                    <div class="col-md-4 col-lg-3">
                        <div class="card gray-space-block ">
                            <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
                            <div class="card-body">
                                <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>3 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>20 </strong></li>
                                </ul>
                                <h5 class="card-title">SSC GD CONSTABLE    </h5>
                                <p class="card-text" fulltext="SSC GD कांस्टेबल कर्मचारी चयन आयोग, कर्मचारी चयन आयोग बीएसएफ, सीआईएसएफ, आईटीबीपी, सीआरपीएफ और एसएसबी और राइफलमैन (जनरल ड्यूटी) में उम्मीदवारों के सहभागिता (सामान्य कर्तव्य) के रूप में परीक्षा आयोजित करता है।
सीमा सुरक्षा बल (BSF), केंद्रीय औद्योगिक सुरक्षा बल (CISF), केंद्रीय रिजर्व पुलिस बल (CRPF), इंडो तिब्बती सीमा पुलिस (ITBP), सशस्त्र सीमा बल (SSB), राष्ट्रीय जांच एजेंसी (NIA), सचिवालय सुरक्षा बल (SSF) और असम राइफल्स में राइफलमैन उक्त सभी विभागो में (जनरल ड्यूटी) GD कांस्टेबल का कार्य होता है " lesstext="SSC GD कांस्टेबल कर्मचारी चयन आयोग, कर्मचारी चयन आयोग बीएसएफ, सीआईएसएफ, आईटीबीपी, सीआरपीएफ और एसएसबी और राइफलमैन (जनरल ड्यूटी) में उम्मीदवारों के..">
                                    
                                    SSC GD कांस्टेबल कर्मचारी चयन आयोग, कर्मचारी चयन आयोग बीएसएफ, सीआईएसएफ, आईटीबीपी, सीआरपीएफ और एसएसबी और राइफलमैन (जनरल ड्यूटी) में उम्मीदवारों के..
                                </p>
                                <span class="spnshowhide less">show more</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-3">
                        <div class="card gray-space-block ">
                            <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
                            <div class="card-body">
                                <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>4 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>30 </strong></li>
                                </ul>
                                <h5 class="card-title">1 <sup>st</sup> GRADE TEACHER </h5>
                                <p class="card-text">RPSC द्वारा प्रथम श्रेणी शिक्षक के विभिन्न पदों पर भर्ती निकली जाती है उम्मीदवारों को b.ed के साथ स्नातक की डिग्री होनी चाहिए मान्यता प्राप्त विश्वविद्यालय से।  अभ्यर्थियों को बीएड में योग्य होना चाहिए</p>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-3">
                        <div class="card gray-space-block ">
                            <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
                            <div class="card-body">
                                <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>4 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>30 </strong></li>
                                </ul>
                                <h5 class="card-title">II <sup>nd</sup>GRADE TEACHER   </h5>
                                <p class="card-text" fulltext="RPSC द्वारा द्वितीय श्रेणी शिक्षक के विभिन्न पदों पर भर्ती निकली जाती है राष्ट्रीय विषय के लिए राष्ट्रीय शिक्षा परिषद द्वारा मान्यता प्राप्त शिक्षा में वैकल्पिक विषय और डिग्री या डिप्लोमा के रूप में 
                                    संबंधित विषय के साथ स्नातक या समकक्ष परीक्षाऔरदेवनागरी लिपि और राजस्थानी संस्कृति के ज्ञान में लिखे गए हिंदी का ज्ञान।                                                                                                                           वैकल्पिक विषयों के रूप में निम्नलिखित विषयों में से कम से कम दो के साथ स्नातक या समकक्ष परीक्षा: भौतिकी, रसायन विज्ञान, प्राणीशास्त्र, वनस्पति विज्ञान, सूक्ष्म जीवविज्ञान, बायो-टेक्नोलॉजी और बायो-कैमिस्ट्री और शिक्षा में डिग्री या डिप्लोमा शिक्षा के लिए राष्ट्रीय परिषद द्वारा मान्यता प्राप्तऔरदेवनागरी लिपि और राजस्थानी संस्कृति के ज्ञान में लिखे गए हिंदी का ज्ञान।                                     विषयों से कम से कम दो विषयों के साथ स्नातक या समकक्ष परीक्षा- 
                                    इतिहास, भूगोल, अर्थशास्त्र, राजनीति विज्ञान, समाजशास्त्र, लोक प्रशासन और दर्शनशास्त्र वैकल्पिक विषयों के रूप मेंऔरराजस्थान सरकार द्वारा मान्यता प्राप्त शिक्षा में डिग्री या डिप्लोमा।औरदेवनागरी लिपि और राजस्थानी संस्कृति के ज्ञान में लिखे गए हिंदी का ज्ञान।" lesstext="RPSC द्वारा द्वितीय श्रेणी शिक्षक के विभिन्न पदों पर भर्ती निकली जाती है राष्ट्रीय विषय के लिए राष्ट्रीय शिक्षा परिषद द्वारा मान्यता प्राप्त शिक्षा में वैकल्पिक विषय और डिग्री या डिप्लोमा के रूप में..">
                                    RPSC द्वारा द्वितीय श्रेणी शिक्षक के विभिन्न पदों पर भर्ती निकली जाती है राष्ट्रीय विषय के लिए राष्ट्रीय शिक्षा परिषद द्वारा मान्यता प्राप्त शिक्षा में वैकल्पिक विषय और डिग्री या डिप्लोमा के रूप में..
                                </p>
                                <span class="spnshowhide less">show more</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-3">
                        <div class="card gray-space-block ">
                            <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
                            <div class="card-body">
                                <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>4 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>30 </strong></li>
                                </ul>
                                <h5 class="card-title">3 <sup>rd</sup> GRADE TEACHER </h5>
                                <p class="card-text" fulltext=" RPSC द्वारा तृतीय श्रेणी शिक्षक के विभिन्न पदों पर भर्ती निकली जाती है 
i) प्राथमिक और उच्च प्राथमिक स्तर के लिए उम्मीदवारों को कम से कम 60% अंकों के साथ                     आरईईटी / आरटीईटी पास करना चाहिए। राजस्थान के आरक्षित श्रेणी के उम्मीदवारों के पास 36% अंक होना चाहिए।
ii) कक्षा 6 - 8 (द्वितीय स्तर) के लिए

एलिमेंट्री एजुकेशन में स्नातक और 2 साल का डिप्लोमा कोर्स। या 50% अंकों के साथ स्नातक के साथ 1 वर्ष बीएड। या                                                                                                                                                   एनसीटीई के अनुसार 45% अंक और 1 वर्ष बीएड के साथ स्नातक। या
प्राथमिक शिक्षा में न्यूनतम 50% अंक और 4 वर्ष स्नातक की डिग्री के साथ 12 वीं। या
12 वीं न्यूनतम 50% कुल और 4 साल कला / विज्ञान / कला शिक्षा / विज्ञान शिक्षा में स्नातक की डिग्री। या
50% अंकों में स्नातक की डिग्री और 1 वर्ष बैचलर इन एजुकेशन (बीएड) (विशेष शिक्षा)।" lesstext="RPSC द्वारा तृतीय श्रेणी शिक्षक के विभिन्न पदों पर भर्ती निकली जाती है 
i) प्राथमिक और उच्च प्राथमिक स्तर के लिए उम्मीदवारों को कम से कम 60% अंकों के साथ आरईईटी / आरटीईटी..">
                                   
                                    RPSC द्वारा तृतीय श्रेणी शिक्षक के विभिन्न पदों पर भर्ती निकली जाती है 
i) प्राथमिक और उच्च प्राथमिक स्तर के लिए उम्मीदवारों को कम से कम 60% अंकों के साथ आरईईटी / आरटीईटी..
                                </p>
                                <span class="spnshowhide less">show more</span>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="card-group row">
                    <div class="col-md-4 col-lg-3">
                        <div class="card gray-space-block ">
                            <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
                            <div class="card-body">
                                <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>4 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>20 </strong></li>
                                </ul>
                                <h5 class="card-title">GRAM SEVAK     </h5>
                                <p class="card-text">
                                    ग्राम सेवक कोग्राम पंचायत सचिव के रूप में जाना जाता है। वह अपने दैनिक कामकाज में गांव सरपंच की सहायता के लिए सरकार द्वारा एक गांव में संलग्न है
कार्य: सरपंच की सहायता करें और उनकी कार्यप्रणाली में सहायता और सलाह दें। गांव के संबंध में रिकॉर्ड और दस्तावेज बनाए रखें गांव में होने वाली घटनाओं के बारे में उच्च अधिकारियों को समय-समय पर रिपोर्ट करता है


                                </p>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-3">
                        <div class="card gray-space-block ">
                            <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
                            <div class="card-body">
                                <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>4 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>30 </strong></li>
                                </ul>
                                <h5 class="card-title">BANK P.O  </h5>
                                <p class="card-text">प्रोबेशनरी ऑफिसर की नियुक्ति के लिए राष्ट्रीय स्तर की भर्ती परीक्षा।</p>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-3">
                        <div class="card gray-space-block ">
                            <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
                            <div class="card-body">
                                <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>3 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>30 </strong></li>
                                </ul>
                                <h5 class="card-title">RAILWAY ALP    </h5>
                                <p class="card-text">
                                    वह ड्राइविंग या ट्रेन का संचालन करने में लोको पायलट की सहायता करता है। 
                                </p>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-3">
                        <div class="card gray-space-block ">
                            <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
                            <div class="card-body">
                                <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>3 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>30 </strong></li>
                                </ul>
                                <h5 class="card-title">RAILWAY GROUP D  </h5>
                                <p class="card-text">
                                    Cabinman, Keyman, Fitter, Leverman, Gangman, Pointsman, Group D – Store, Porter, Group D – Engg,Shunter, Helper-II (Electrical), Switchman, Helper-II (Mechanical), Trackman, Helper-II (S & T), Welder

                                </p>

                            </div>
                        </div>
                    </div>


                </div>
            </div>


        </div>
        <div class="full-row curv-blue-right">

            <div class=" container explore-spaces-block space-80">

                <h2>Academic Courses </h2>


                <div class="card-group row">
                    <div class="col-md-4 col-lg-3">
                        <div class="card gray-space-block ">
                            <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
                            <div class="card-body">
                                <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>8 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>25 </strong></li>
                                </ul>
                                <h5 class="card-title">9 <sup>th</sup> standard  </h5>
                                <p class="card-text">अंग्रेजी, हिंदी, विज्ञान, सामाजिक विज्ञान ,गंणित सभी विषयों का शिक्षण अनुभवी शिक्षक द्वारा करवाया जाता हैं / दोनों माध्यम ENGLISH/ हिंदी की शिक्षा प्रधान की जाती हैं/</p>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-3">
                        <div class="card gray-space-block ">
                            <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
                            <div class="card-body">
                                <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>8 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>25 </strong></li>
                                </ul>
                                <h5 class="card-title">10 <sup>th</sup> standard </h5>
                                <p class="card-text">अंग्रेजी, हिंदी, विज्ञान, सामाजिक विज्ञान ,गंणित सभी विषयों का शिक्षण अनुभवी शिक्षक द्वारा करवाया जाता हैं / दोनों माध्यम ENGLISH/ हिंदी की शिक्षा प्रधान की जाती हैं/ यह माध्यमिक बोर्ड कक्षा होने के कारण इस कक्षा के बच्चो पर विशेष ध्यान दिया जाता / इसके अलावा बोर्ड परीक्षा पर में मेरिट आने पर या 85% से अधिक लाने पर संस्था की तरफ से पुरूस्कार भी वितरण किये जाते हैं / </p>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-3">
                        <div class="card gray-space-block ">
                            <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
                            <div class="card-body">
                                <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>8 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>25 </strong></li>
                                </ul>
                                <h5 class="card-title">11 <sup>th</sup> standard </h5>
                                <p class="card-text">कला (ARTS), विज्ञान (SCIENCE) कोर्सेज के अंतर्गत विषयों की शिक्षा प्रधान की जाती हैं/ इस कक्षा में भी दोनों ही माध्यम ENGLISH/हिंदी चलवाते हैं / संस्था की तरफ से 90% से अधिक अंक लाने पर पुरुस्कार वितरण करते हैं /</p>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-3">
                        <div class="card gray-space-block ">
                            <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
                            <div class="card-body">
                                <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>8 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>25 </strong></li>
                                </ul>
                                <h5 class="card-title">12 <sup>th</sup> standard  </h5>
                                <p class="card-text">कला (ARTS), विज्ञान (SCIENCE) कोर्सेज के अंतर्गत विषयों की शिक्षा प्रधान की जाती हैं/ इस कक्षा में भी दोनों ही माध्यम ENGLISH/हिंदी चलवाते हैं / संस्था की तरफ से 90% से अधिक अंक लाने पर पुरुस्कार वितरण करते हैं /</p>

                            </div>
                        </div>
                    </div>




                </div>
            </div>


        </div>

        <footer>
            <div class=" d-lg-none d-xl-none  d-done d-sm-block  d-md-block ">
                <div class="back-to-top ScrollTop">
                    <a href="#">
                        <img src="images/up-icon.svg" alt="" width="40" />
                    </a>
                </div>
            </div>
            <div class="d-none d-lg-block d-xl-block scroll-position">
                <div class="back-to-top ScrollTop">
                    <a href="#">
                        <img src="images/up-icon.svg" alt="" width="40" /></a>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="footer-logo col-lg-3 ">
                        <a href="index.aspx">
                            <img src="images/footer-logo.png" alt="" /></a>
                    </div>
                    <div class=" col-md-3 ">
                        <h3>QUICK LINK </h3>
                        <ul class="footer-link">
                            <li><a href="index.aspx">Home </a></li>

                            <li><a href="index.aspx">Course Offered  </a></li>

                            <li><a href="index.aspx">Skills  </a></li>

                            <li><a href="index.aspx">News  </a></li>

                            <li><a href="contactus.aspx">Contact Us  </a></li>


                        </ul>
                    </div>
                    <div class=" col-lg-3  address-block">
                        <h3>ADDRESS  </h3>

                        <p class="address">
                            AKV classes, 3rd floor, Malwa Business Center, Sawant Singh Chouraha, Nimbahera, District: Chittorgarh(Raj.) 312601
                        </p>

                        <p class="phone-n">+91 87428 46490 </p>
                        <p class="email-id">info@akvclasses.com </p>



                    </div>
                    <div class="col-lg-3  text-right">
                        <h3>Contact Us  </h3>

                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3626.9748644687766!2d74.6831603!3d24.6245512!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x396616d33dcf4075%3A0x88f52458e1a7cf0e!2sAKV+CLASSES!5e0!3m2!1sen!2sin!4v1537990676066" width="100%" height="200" frameborder="0" style="border: 0" allowfullscreen></iframe>

                    </div>
                </div>
                <div class="row">
                    <div class="copyright col-sm-6 ">
                        <div class="footer-bottom-menu">&copy;Copyright 2018 All Rights Reserved AKV Classes   </div>
                    </div>
                    <div class=" col-sm-6  text-right">
                        <ul class="social footer-social">
                            <li><a href="#"><i class="fa fa-facebook-official fa-2x"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram fa-2x"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter fa-2x"></i></a></li>
                        </ul>


                    </div>
                </div>
            </div>
        </footer>


        <script src="js/jquery-3.3.1.min.js"></script>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/wow/wow.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/mCustomScrollbar.concat.min.js"></script>


        <script src="js/jquery.reject.min.js"></script>

        <script src="js/support.js"></script>
        <script>
            $(document).ready(function () {
                wow = new WOW(
                      {
                          boxClass: 'wow',      // default
                          animateClass: 'animated', // default
                          offset: 0,          // default
                          mobile: true,       // default
                          live: true        // default
                      }
                    )
                wow.init();

                $('span.spnshowhide').click(function () {

                    var prs = $(this).prev('p');

                    if ($(this).hasClass('less')) {
                        
                      
                        prs.text(prs.attr('fulltext'));

                        $(this).text('show less').removeClass('less').addClass('more');

                    }
                    else {
                        
                        
                        prs.text(prs.attr('lesstext'));

                        $(this).text('show more').removeClass('more').addClass('less');
                    }
                });

            });
        </script>
    </form>
</body>
</html>
