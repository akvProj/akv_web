﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FNSFront.Startup))]
namespace FNSFront
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
