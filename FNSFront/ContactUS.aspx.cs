﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core;
using System.Text.RegularExpressions;


namespace FNSFront
{
    public partial class ContactUS : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            lblMessage.Text = "";
            try
            {
                if (txtEmail.Text.Trim().Length > 0 && txtContactSubject.Text.Trim().Length > 0 && txtFirstName.Text.Trim().Length > 0 && txtEnquiry.Text.Trim().Length > 0)
                {
                    string strEmailReg = @"^([\w-\.]+)@((\[[0-9]{2,3}\.[0-9]{2,3}\.[0-9]{2,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{2,3})(\]?)$";

                    if (Regex.IsMatch(txtEmail.Text.Trim(), strEmailReg))
                    {
                        string msg = "Hi Admin, </br></br> There is new enquiry from " + txtFirstName.Text.Trim() + "</br></br>";
                        msg += txtEnquiry.Text.Trim();
                        if (txtPhoneNumber.Text.Trim().Length > 0) {
                            msg += " </br></br> and contact number is-" + txtPhoneNumber.Text.Trim();
                        }
                        msg += "</br></br>Thank You.";
                        clsMail obj = new clsMail();
                        obj.To = SiteSettings.ToEmails;
                        obj.From = txtEmail.Text.Trim();
                        obj.FromName = txtFirstName.Text.Trim() + " " + Convert.ToString(txtLastName.Text.Trim());
                        obj.Subject = txtContactSubject.Text.Trim();
                        obj.MailBody = msg;
                        obj.MailBodyManualSupply = true;
                        obj.Send();
                        lblMessage.Text = "Thank you. Requset has been send, we will contact you soon..";

                        txtEmail.Text = "";
                        txtFirstName.Text = "";
                        txtLastName.Text = "";
                        txtContactSubject.Text = "";
                        txtEnquiry.Text = "";
                        txtPhoneNumber.Text = "";
                    }
                    else
                    {
                        lblMessage.Text = "Invalid Email";
                    }
                }
                else {
                    lblMessage.Text = "Please fill required fields.";
                }
            }
            catch (Exception ex) { lblMessage.Text = ex.Message; }
            //TempData["Success"] = "Request send Successfully!";
        }
    }
}