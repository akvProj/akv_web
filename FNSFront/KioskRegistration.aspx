﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="KioskRegistration.aspx.cs" Inherits="FNSFront.KioskRegistration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1.0, user-scalable=no">
    <title>Kiosk Registration</title>
    <link rel="icon" href="~/images/favicon.ico" type="image/x-icon">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="~/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="~/fonts/styles.css" rel="stylesheet">
    <link href="~/css/animate.css" rel="stylesheet">
    <link rel="stylesheet" href="~/css/bootstrap-select.css">
    <link href="~/css/owl.carousel.min.css" rel="stylesheet">
    <link href="~/css/layout.css" rel="stylesheet">
    <link href="~/css/responsive.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <header class="mobile-header navbar  visible-mobile-menu">
                <a class="navbar-brand" href="index.aspx">
                    <img src="~/images/logo.png" alt="" />
                </a>
                <div class="mobile-top-icons d-lg-none d-sm-block">
                    <div class="mobile-icon-sm ">
                        <span class="phone"><a href="tel:02890892714">
                            <img class="mr-2" src="~/images/icon_phone.svg" alt="" /></a></span>
                    </div>
                    <button type="button" class="navbar-toggle bar collapsed" data-toggle="collapse" data-target="" aria-expanded="false"></button>
                </div>
                <div class="nav-right logo-white" id="mobile-menu">
                    <a class="navbar-brand" href="index.aspx">
                        <img src="~/images/footer-logo.png" alt="" />
                    </a>
                    <button type="button" class="navbar-toggle navbar-toggle-close" data-toggle="collapse" data-target="" aria-expanded="false"></button>
                    <ul class="nav navbar-nav mobile-nav">

                        <li><a href="index.aspx">Home </a></li>

                        <li><a href="CourseOffered.aspx">Course Offered  </a></li>

                        <li><a href="index.aspx">Skills  </a></li>
                        <li><a href="studentcorner.aspx">Student Corner  </a></li>

                        <li><a href="index.aspx">News  </a></li>

                        <li><a href="contactus.aspx">Contact Us  </a></li>



                        <li class="nav-btn "><a href="KioskRegistration.aspx">Kiosk Register </a></li>
                        <li class="phone-nav"><a href="tel:87428 46490">
                            <img src="~/images/icon_phone_white.svg" alt="" />+91 87428 46490</a></li>
                    </ul>
                    <div class="clearfix"></div>
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-facebook-official"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="full-row footer-bottom">
                        <ul>
                            <div class="footer-bottom-menu"></div>
                            <li class="first">&copy;Copyright 2018 AKV Classes   </li>

                            <li><a href="#">Terms & Conditions </a></li>
                            <li><a href="#">Privacy Policy </a></li>
                            <li><a href="#">Cookies </a></li>
                        </ul>
                    </div>
                </div>
            </header>
            <nav class="navbar navbar-default navbar-expand-lg header-top hidden-mobile">
                <div class="container">
                    <div class="logo logo-desktop">
                        <a href="index.aspx">
                            <img src="images/logo.png" alt="" />
                        </a>
                    </div>
                    <button id="navbar-toggle" class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                    <div class="collapse navbar-collapse js-navbar-collapse" id="navbarSupportedContent">
                        <div class="navbar-header">
                            <div class="logo-mobile logo">
                                <a href="index.aspx">
                                    <img src="~/images/footer-logo.svg" alt="" width="92">
                                </a>
                            </div>
                        </div>
                        <div class="navbar-custom">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item"><a class="nav-link" href="index.aspx"><span>Home </span></a></li>
                                <li class="nav-item"><a class="nav-link" href="CourseOffered.aspx"><span>Course Offered </span></a></li>
                                <li class="nav-item"><a class="nav-link" href="index.aspx"><span>Skills </span></a></li>
                                <li class="nav-item"><a class="nav-link" href="studentcorner.aspx"><span>Student Corner </span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="head-right d-none d-xl-block d-md-block">
                        <div class="phone">Speak to a venue specialist &nbsp; <b>+91 87428 46490</b></div>
                        <div class="right-menu">
                            <ul>
                                <li><a href="index.aspx"><span>News </span></a></li>
                                <li><a href="contactus.aspx"><span>Contact Us </span></a></li>
                                <li><a class="btn btn-primary btn-lg" href="KioskRegistration.aspx">Kiosk Register</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="make-an-enquiry-section">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-7  followup ">
                            <h2>Kiosk Registration </h2>
                            <p>Once you put details with us, we ativate you and get your space. </p>

                            <asp:Label ID="lblMessage" runat="Server" ForeColor="Red"></asp:Label>



                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Kiosk Name </label>

                                        <asp:TextBox ID="txtKioskName" runat="server" class="form-control" MaxLength="150"></asp:TextBox>

                                    </div>

                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Kiosk Person First Name</label>

                                        <asp:TextBox ID="txtFirstName" runat="server" class="form-control" MaxLength="50"></asp:TextBox>


                                    </div>
                                    <div class="col-md-6">
                                        <label>Kiosk Person Last Name</label>

                                        <asp:TextBox ID="txtLastName" runat="server" class="form-control" MaxLength="50"></asp:TextBox>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Email Address </label>

                                        <asp:TextBox ID="txtEmail" runat="server" class="form-control" MaxLength="50"></asp:TextBox>

                                    </div>
                                    <div class="col-md-6">
                                        <label>Phone Number </label>

                                        <asp:TextBox ID="txtPhoneNo" runat="server" class="form-control" MaxLength="12"></asp:TextBox>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Password </label>

                                        <asp:TextBox ID="txtPassword" runat="server" class="form-control" TextMode="Password" MaxLength="20"></asp:TextBox>

                                    </div>
                                    <div class="col-md-6">
                                        <label>Confirm Password </label>

                                        <asp:TextBox ID="txtConfirmPassword" runat="server" class="form-control" TextMode="Password" MaxLength="50"></asp:TextBox>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>State </label>
                                        <asp:DropDownList ID="drpState" runat="server" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="drpState_SelectedIndexChanged">
                                            <asp:ListItem Text="select" Value="Select">Select State</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                    <div class="col-md-6">
                                        <label>City </label>
                                        <asp:DropDownList ID="drpCity" runat="server" class="form-control">
                                            <asp:ListItem Text="Select City" Value="0">Select City</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Address </label>

                                        <asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" MaxLength="200"></asp:TextBox>


                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>SC Name </label>

                                        <asp:TextBox ID="txtSCName" runat="server" class="form-control" MaxLength="40"></asp:TextBox>

                                    </div>
                                    <div class="col-md-6">
                                        <label>DC Name </label>

                                        <asp:TextBox ID="txtDCName" runat="server" class="form-control" MaxLength="40"></asp:TextBox>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Payment Transaction ID </label>

                                        <asp:TextBox ID="txtPaymentTransactionID" runat="server" class="form-control" MaxLength="40"></asp:TextBox>


                                    </div>

                                </div>
                            </div>
                            <%-- <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <asp:CheckBox ID="chkPrivatePolicy" runat="server" class="custom-control-input" Checked="true" />
                                    <label class="custom-control-label" for="customCheck1">I agree to be contacted by email with the <a href="#"> Privacy Policy </a> </label>
                                </div>
                            </div>--%>
                            <div class="form-group ">

                                <asp:Button class="btn btn-info btn-lg min-w-210" ID="btnSubmit" runat="server" Text="Send Request" OnClick="btnSubmit_Click" />
                            </div>


                        </div>
                        <div class="col-xl-5 contactus">
                            <h2>Contact us </h2>
                            <p>
                                Questions about Anything? Just have your
                                <br>
                                people call call or email Us
                            </p>
                            <div class="full-row contact-person">
                                <p>
                                    <i class="fa fa-map-marker"></i>AKV classes, 3rd floor, Malwa Business Center, Sawant Singh Chouraha, Nimbahera, District: Chittorgarh(Raj.) 312601
                                </p>
                                <p><i class="fa fa-phone"></i>+91 87428 46490 </p>
                                <p><i class="fa fa-envelope"></i><a href="mailto:info@akvclasses.com">info@akvclasses.com</a></p>
                                <br />
                            </div>
                            <div class="full-row contact-person">
                                Make you payment and move with us.<br />
                                <p>Please add your Email Address in Reference note when you make payment.</p>
                                <br />
                            </div>
                            <div class="full-row row">

                                
                                <div class="col-md-6">
                                    <strong>Paytm Details</strong><br />
                                    <br />

                                    
                                   
                                    
                                    <img alt="" src="images/KD_paytm.jpeg" height="250px" width="250px" />
                                    <br />
                                     OR<br />
                                    Mobile Number- 8742846490<br />
                                </div>
                                <div class="col-md-6">
                                     <strong>BHIM UPI</strong><br /><br /><br />
                                    <img alt="" src="images/KD_UPI.jpg" />
                                </div>
                                <%--    <br />
                                    <strong>Bank Account Details</strong><br />
                                    <br />
                                    A/C No- 265211100000742<br />
                                    A/C Holder Name- Kuldeep Singh Shaktawat<br />
                                    Bank Name- Andhra Bank<br />
                                    IFSC Code- ANDB0002652<br />
                                    Branch- Nimbahera, Chittorgarh<br />--%>
                            </div>
                            <div class="needto-talk full-row ">
                                <p>
                                    <strong>Not quite ready to talk to someone? </strong><span>Check out our comprehensive
                                    <strong><a href="faq.aspx">FAQs </a></strong>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <div class=" d-lg-none d-xl-none  d-done d-sm-block  d-md-block ">
                <div class="back-to-top ScrollTop">
                    <a href="#">
                        <img src="~/images/up-icon.svg" alt="" width="40" />
                    </a>
                </div>
            </div>
            <div class="d-none d-lg-block d-xl-block scroll-position">
                <div class="back-to-top ScrollTop">
                    <a href="#">
                        <img src="~/images/up-icon.svg" alt="" width="40" /></a>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="footer-logo col-lg-3 ">
                        <a href="index.aspx">
                            <img src="~/images/footer-logo.png" alt="" /></a>
                    </div>
                    <div class=" col-md-3 ">
                        <h3>QUICK LINK </h3>
                        <ul class="footer-link">
                            <li><a href="Index.aspx">Home </a></li>

                            <li><a href="CourseOffered.aspx">Course Offered  </a></li>

                            <li><a href="index.aspx">Skills  </a></li>

                            <li><a href="index.aspx">News  </a></li>

                            <li><a href="contactus.aspx">Contact Us  </a></li>


                        </ul>
                    </div>
                    <div class=" col-lg-3  address-block">
                        <h3>ADDRESS  </h3>

                        <p class="address">
                            AKV classes, 3rd floor, Malwa Business Center, Sawant Singh Chouraha, Nimbahera, District: Chittorgarh(Raj.) 312601
                        </p>

                        <p class="phone-n">+91 87428 46490 </p>
                        <p class="email-id">info@akvclasses.com </p>



                    </div>
                    <div class="col-lg-3  text-right">
                        <h3>Contact Us  </h3>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3626.9748644687766!2d74.6831603!3d24.6245512!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x396616d33dcf4075%3A0x88f52458e1a7cf0e!2sAKV+CLASSES!5e0!3m2!1sen!2sin!4v1537990676066" width="100%" height="200" frameborder="0" style="border: 0" allowfullscreen></iframe>


                    </div>
                </div>
                <div class="row">
                    <div class="copyright col-sm-6 ">
                        <div class="footer-bottom-menu">&copy;Copyright 2018 All Rights Reserved AKV Classes   </div>
                    </div>
                    <div class=" col-sm-6  text-right">
                        <ul class="social footer-social">
                            <li><a href="#"><i class="fa fa-facebook-official fa-2x"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram fa-2x"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter fa-2x"></i></a></li>
                        </ul>


                    </div>
                </div>
            </div>
        </footer>

        <script src="~/js/jquery-3.3.1.min.js"></script>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="~/js/popper.min.js"></script>
        <script src="~/js/bootstrap.min.js"></script>
        <script src="~/js/owl.carousel.min.js"></script>
        <script src="~/js/jquery.reject.min.js"></script>
        <script src="~/js/bootstrap-select.js"></script>
        <script src="~/js/support.js"></script>


    </form>
</body>
</html>
