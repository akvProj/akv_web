﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="FNSFront.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,shrink-to-fit=no, user-scalable=no" />
    <title>AKV Classes</title>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="fonts/styles.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/mCustomScrollbar.css" rel="stylesheet">
    <link href="css/layout.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <style>
        sup {
            vertical-align: super;
            font-size: smaller;
        }
        .carousel p:before {
            display:none !important;
        }
         .carousel p:after {
            display:none !important;
        }
         .carousel p {
            line-height:1.5 !important;
        }


        .member-detail p {
            padding:0px 0 5px 0;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">

        <div class="wrapper">
            <header class="mobile-header navbar  visible-mobile-menu">
                <a class="navbar-brand" href="index.aspx">
                    <img src="images/logo.png" alt="" />
                </a>
                <div class="mobile-top-icons d-lg-none d-sm-block">
                    <div class="mobile-icon-sm ">
                        <span class="phone"><a href="tel:02890892714">
                            <img class="mr-2" src="images/icon_phone.svg" alt="" /></a></span>
                    </div>
                    <button type="button" class="navbar-toggle bar collapsed" data-toggle="collapse" data-target="" aria-expanded="false"></button>
                </div>
                <div class="nav-right logo-white" id="mobile-menu">
                    <a class="navbar-brand" href="index.aspx">
                        <img src="images/footer-logo.png" alt="" />
                    </a>
                    <button type="button" class="navbar-toggle navbar-toggle-close" data-toggle="collapse" data-target="" aria-expanded="false"></button>
                    <ul class="nav navbar-nav mobile-nav">

                        <li><a href="CourseOffered.aspx">Home </a></li>

                        <li><a href="CourseOffered.aspx">Course Offered  </a></li>

                        <li><a href="index.aspx">Skills  </a></li>
                        <li><a href="studentcorner.aspx">Student Corner  </a></li>

                        <li><a href="index.aspx">News  </a></li>

                        <li><a href="contactus.aspx">Contact Us  </a></li>



                        <li class="nav-btn "><a href="KioskRegistration.aspx">Kiosk Register </a></li>
                        <li class="phone-nav"><a href="tel:87428 46490">
                            <img src="images/icon_phone_white.svg" alt="" />+91 87428 46490</a></li>
                    </ul>
                    <div class="clearfix"></div>
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-facebook-official"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="full-row footer-bottom">
                        <ul>
                            <div class="footer-bottom-menu"></div>
                            <li class="first">&copy;Copyright 2018 AKV Classes   </li>

                            <li><a href="#">Terms & Conditions </a></li>
                            <li><a href="#">Privacy Policy </a></li>
                            <li><a href="#">Cookies </a></li>
                        </ul>
                    </div>
                </div>
            </header>
            <nav class="navbar navbar-default navbar-expand-lg header-top hidden-mobile">
                <div class="container">
                    <div class="logo logo-desktop">
                        <a href="index.aspx">
                            <img src="images/logo.png" alt="" />
                        </a>
                    </div>
                    <button id="navbar-toggle" class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                    <div class="collapse navbar-collapse js-navbar-collapse" id="navbarSupportedContent">
                        <div class="navbar-header">
                            <div class="logo-mobile logo">
                                <a href="index.aspx">
                                    <img src="images/footer-logo.svg" alt="" width="92">
                                </a>
                            </div>
                        </div>
                        <div class="navbar-custom">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item"><a class="nav-link" href="index.aspx"><span>Home </span></a></li>
                                <li class="nav-item"><a class="nav-link" href="CourseOffered.aspx"><span>Course Offered </span></a></li>
                                <li class="nav-item"><a class="nav-link" href="index.aspx"><span>Skills </span></a></li>
                                <li class="nav-item"><a class="nav-link" href="StudentCorner.aspx"><span>Student Corner </span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="head-right d-none d-xl-block d-md-block">
                        <div class="phone">Speak to a venue specialist &nbsp; <b>+91 87428 46490</b></div>
                        <div class="right-menu">
                            <ul>
                                <li><a href="index.aspx"><span>News </span></a></li>
                                <li><a href="contactus.aspx"><span>Contact Us </span></a></li>
                                <li><a class="btn btn-primary btn-lg" href="KioskRegistration.aspx">Kiosk Register</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>

            <section class="top full-banner curv-orange-left wow animate-bg-left">
                <div class="container">
                    <div class="banner-home row flex-row-reverse m-0">
                        <div class="banner-image col-lg-8  p-0">

                            <img src="images/home-banner.jpg" alt="" />

                        </div>
                        <div class="banner-box col-lg-4 p-0 ">
                            <div class="section-forest box box-left-position white-text box-event  d-flex">
                                <div class="align-self-center">

                                    <h1 class="white-text">Best Educaton <small>For your better future </small></h1>
                                    <div class="intro-text">
                                        <p>
                                            We provides always our best services for our 
students and  always try to  achieve our student's trust 
and satisfaction.
                                        </p>


                                        <a class="btn-outline-info btn-lg btn" href="#">Know More </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="row d-flex justify-content-end">
                        <div class="col-lg-8 thoughts carousel">
                            <p>
                                We provides always our best services for our students and  always try
 to  achieve our student's trust and satisfaction.
                            </p>
                            <h4>Jaswant Singh Shaktawat  <span>Chairman</span> </h4>

                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="full-row curv-blue-right">

            <div class="container meet-the-team space-80">

                <h2>Our Faculties </h2>

                <div id="student-testimonial" class="carousel slide" data-ride="carousel">

                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="member-list full-row ">
                                        <picture class="full-img"> <img src="images/Kuldeep_Shaktavat.png" alt="" height="350px" width="369px"> </picture>
                                        <div class="member-detail full-row">
                                            <h3>Kuldeep Singh Shaktawat </h3>
                                            <p >
                                                Qualification –B.TECH (CS) 
                 
                                            </p>
                                            <p>Faculty -GEOGRAPHY</p>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="member-list full-row ">
                                        <picture class="full-img"> <img src="images/RAJENDRA_ PANWAR.png" alt="" height="350px" width="369px"> </picture>
                                        <div class="member-detail full-row">
                                            <h3>Rajendra Panwar </h3>
                                            <p >
                                                Qualification –D.F.L (ENGLISH) 
                 
                                            </p>
                                            <p>Faculty -ENGLISH </p>
                                         
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="member-list full-row ">
                                        <picture class="full-img"> <img src="images/RICHA_AMETA.png" alt="" height="350px" width="369px"> </picture>
                                        <div class="member-detail full-row">
                                            <h3>Richa Ameta </h3>
                                            <p >
                                                Qualification –M.A , B.ED (HINDI) 
                 

                                            </p>
                                            <p>Faculty -HINDI</p>
                                          
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="member-list full-row ">
                                        <picture class="full-img"> <img src="images/SHWETA_SEN.png" alt="" height="350px" width="369px"> </picture>
                                        <div class="member-detail full-row">
                                            <h3>Shweta Sen </h3>
                                            <p >
                                                Qualification –B.SC (MATHS)
                  
                                            </p>

                                        <p>Faculty -MATHS</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="member-list full-row ">
                                        <picture class="full-img"> <img src="images/PAYAL_LOHAR.png" alt="" height="350px" width="369px"> </picture>
                                        <div class="member-detail full-row">
                                            <h3>Payal Lohar </h3>
                                            <p >
                                                Qualification –B.Sc.
                 
                                            </p>

                                           <p>Faculty -Science</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                     <div class="member-list full-row ">
                                        <picture class="full-img"> <img src="images/bhagchand.jpg" alt="" height="350px" width="369px"> </picture>
                                        <div class="member-detail full-row">
                                            <h3>Bhagchand Choudhary </h3>
                                            <p >
                                                Qualification –MA (History)
                  
                                            </p>

                                        <p>Faculty -History</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="member-list full-row ">
                                        <picture class="full-img"> <img src="images/harish.jpg" alt="" height="350px" width="369px"> </picture>
                                        <div class="member-detail full-row">
                                            <h3>Harish Nagar </h3>
                                            <p >
                                                Qualification –B.TECH (ME)
                  
                                            </p>

                                        <p>Faculty -politics</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                   <div class="member-list full-row ">
                                        <picture class="full-img"> <img src="images/riya.jpg" alt="" height="350px" width="369px"> </picture>
                                        <div class="member-detail full-row">
                                            <h3>Riya Singh</h3>
                                            <p >
                                                Qualification –BA (geography)
                  
                                            </p>

                                        <p>Faculty -geography</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="member-list full-row ">
                                        <picture class="full-img"> <img src="images/vikas_chandra.png" alt="vikas chandra gechan" height="350px" width="369px"> </picture>
                                        <div class="member-detail full-row">
                                            <h3>Vikas Chandra Gechan </h3>
                                            <p >
                                                Qualification –BA (Geography)
                 
                                            </p>

                                           <p>Faculty -Geography</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                     <div class="member-list full-row ">
                                        <picture class="full-img"> <img src="images/vishnu_kumar.jpg" alt="vishnu kumar" height="350px" width="369px"> </picture>
                                        <div class="member-detail full-row">
                                            <h3>Vishnu Kumar </h3>
                                            <p >
                                                Qualification –B.TECH (ME)
                  
                                            </p>

                                        <p>Faculty -Maths, Reasoning</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                   
                                </div>
                                <div class="col-lg-3">
                                  
                                </div>

                            </div>
                        </div>

                    </div>
                   
               

                </div>
            </div>

        </div>

        <div class="wrapper">

            <div class=" container explore-spaces-block space-80">

                <h2>Courses Offered  </h2>


                <div class="card-group row">
                    <div class="col-md-4 col-lg-3">
                        <div class="card gray-space-block ">
                            <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
                            <div class="card-body">
                                <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>8 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>20 </strong></li>
                                </ul>
                                <h5 class="card-title">RAS pre   </h5>

                                <p class="card-text">
                                    RAS के लिए होने वाले परीक्षा का आयोजन राजस्थान लोक सेवा आयोग (RPSC) द्वारा किया जाता है, यह राजस्थान सरकार का एक संगठन है जिसका संचालन राजस्थान सरकार के द्वारा किया जाता है | RAS अधिकारियों ने उप-जिला स्तर पर...
                                </p>
                                <p class="card-text"><small class="text-muted"><a href="CourseOffered.aspx">View Detail </a></small></p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-3">
                        <div class="card gray-space-block ">
                            <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
                            <div class="card-body">
                                <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>3 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>30 </strong></li>
                                </ul>
                                <h5 class="card-title">LDC </h5>
                                <p class="card-text">कर्मचारी चयन आयोग SSC (एसएससी) हर साल भारत में विभिन्न मंत्रालयों और विभागों के सरकारी कार्यालयों में विभिन्न पदों पर उम्मीदवारों की भर्ती करती है। ये पद विभिन्न श्रेणियों के होते हैं जो उम्मीदवारों की योग्यता पर निर्भर करते हैं...</p>
                                <p class="card-text"><small class="text-muted"><a href="CourseOffered.aspx">View Detail </a></small></p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-3">
                        <div class="card gray-space-block ">
                            <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt=""> </a> </picture>
                            <div class="card-body">
                                <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>4 1/2Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>30 </strong></li>
                                </ul>
                                <h5 class="card-title">SI (SUB-INSPECTOR)  </h5>
                                <p class="card-text">
                                    राजस्थान पुलिस में सब इंस्पेक्टर के पद पर भर्ती निकली जाती है सब-इंस्पेक्टर एक वर्ग III पद है, कुछ मामलों में पुलिस इंस्पेक्टर या डीएसपी को रिपोर्टिंग। वे यातायात शाखा, अपराध शाखा, दूरसंचार शाखा, फोरेंसिक विज्ञान प्रयोगशाला, योजना प्रशिक्षण...

                                </p>
                                <p class="card-text"><small class="text-muted"><a href="CourseOffered.aspx">View Detail </a></small></p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-3">
                        <div class="card gray-space-block ">
                            <picture class="full-image block-with-hover "> <a href="#">
            <div class="overlay"></div>
            <img class="card-img-full" src="images/course.jpg" alt="" /> </a> </picture>
                            <div class="card-body">
                                <ul>
                                    <li><i class="fa fa-compass"></i>Course Duration : <strong>8 Months </strong></li>
                                    <li><i class="fa fa-expand"></i>Class Size : <strong>25 </strong></li>
                                </ul>
                                <h5 class="card-title">12 <sup>th</sup> standard  </h5>
                                <p class="card-text">कला (ARTS), विज्ञान (SCIENCE) कोर्सेज के अंतर्गत विषयों की शिक्षा प्रधान की जाती हैं/ इस कक्षा में भी दोनों ही माध्यम ENGLISH/हिंदी चलवाते हैं / संस्था की तरफ से 90% से अधिक अंक लाने पर पुरुस्कार वितरण करते हैं...</p>
                                <p class="card-text"><small class="text-muted"><a href="CourseOffered.aspx">View Detail </a></small></p>
                            </div>
                        </div>
                    </div>




                </div>


            </div>

            <section class="top full-banner curv-blue-left wow animate-bg-left news-section">




                <div class="container ">



                    <div class=" d-flex align-items-center row">

                        <div class="col-lg-5 pr-0">

                            <div class="section-orange newsbox box box-left-position white-text box-event">

                                <h2 class="white-text">News Stand </h2>
                                <div class="newsstand mCustomScrollbar ">
                                    <ul>
                                        <li>
                                            <h4>News Title </h4>
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                        </li>

                                        <li>
                                            <h4>News Title </h4>
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                        </li>


                                        <li>
                                            <h4>News Title </h4>
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                        </li>


                                        <li>
                                            <h4>News Title </h4>
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                        </li>



                                    </ul>


                                </div>

                            </div>
                        </div>

                        <div class="col-lg-7 pl-5 thoughts carousel section-gray-light d-flex ">
                            <div class="row">
                                <div class="col-8">
                                    <div class="align-self-center">


                                        <p style="font-size:20px;"> <span style="font-size:16px;">‘Poor people have big TV, Rich people have big Library ‘  this Quote is by a famous author and enterpreneur Jim Rohn.</span>
                                             <br />
Success is the two important pillar of AKV Classes.AKV classes faculties were highly graduated and experience. They impower students day by day to think their right career path. 
From my experience, whether it is competative or academics I advice to every candidate to joinsuch type of hub or tower  which fulfill your dreams.
                                        </p>
                                        <h4>Jaswant Singh Shaktawat <span>Chairman</span> </h4>

                                    </div>
                                </div>
                                <div class="col-4">
                                    <img style="height: 300px; width: 300px;" src="images/ChairMan_pic.jpeg" alt="" /></div>
                            </div>
                        </div>
                    </div>



                </div>
            </section>
        </div>

        <footer>
            <div class=" d-lg-none d-xl-none  d-done d-sm-block  d-md-block ">
                <div class="back-to-top ScrollTop">
                    <a href="#">
                        <img src="images/up-icon.svg" alt="" width="40" />
                    </a>
                </div>
            </div>
            <div class="d-none d-lg-block d-xl-block scroll-position">
                <div class="back-to-top ScrollTop">
                    <a href="#">
                        <img src="images/up-icon.svg" alt="" width="40" /></a>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="footer-logo col-lg-3 ">
                        <a href="index.aspx">
                            <img src="images/footer-logo.png" alt="" /></a>
                    </div>
                    <div class=" col-md-3 ">
                        <h3>QUICK LINK </h3>
                        <ul class="footer-link">
                            <li><a href="index.aspx">Home </a></li>

                            <li><a href="CourseOffered.aspx">Course Offered  </a></li>

                            <li><a href="index.aspx">Skills  </a></li>

                            <li><a href="index.aspx">News  </a></li>

                            <li><a href="contactus.aspx">Contact Us  </a></li>


                        </ul>
                    </div>
                    <div class=" col-lg-3  address-block">
                        <h3>ADDRESS  </h3>

                        <p class="address">
                            AKV classes, 3rd floor, Malwa Business Center, Sawant Singh Chouraha, Nimbahera, District: Chittorgarh(Raj.) 312601
                        </p>

                        <p class="phone-n">+91 87428 46490 </p>
                        <p class="email-id">info@akvclasses.com </p>



                    </div>
                    <div class="col-lg-3  text-right">
                        <h3>Contact Us  </h3>

                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3626.9748644687766!2d74.6831603!3d24.6245512!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x396616d33dcf4075%3A0x88f52458e1a7cf0e!2sAKV+CLASSES!5e0!3m2!1sen!2sin!4v1537990676066" width="100%" height="200" frameborder="0" style="border: 0" allowfullscreen></iframe>

                    </div>
                </div>
                <div class="row">
                    <div class="copyright col-sm-6 ">
                        <div class="footer-bottom-menu">&copy;Copyright 2018 All Rights Reserved AKV Classes   </div>
                    </div>
                    <div class=" col-sm-6  text-right">
                        <ul class="social footer-social">
                            <li><a href="#"><i class="fa fa-facebook-official fa-2x"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram fa-2x"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter fa-2x"></i></a></li>
                        </ul>


                    </div>
                </div>
            </div>
        </footer>


        <script src="js/jquery-3.3.1.min.js"></script>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/wow/wow.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/mCustomScrollbar.concat.min.js"></script>


        <script src="js/jquery.reject.min.js"></script>

        <script src="js/support.js"></script>
        <script>
            $(document).ready(function () {
                wow = new WOW(
                      {
                          boxClass: 'wow',      // default
                          animateClass: 'animated', // default
                          offset: 0,          // default
                          mobile: true,       // default
                          live: true        // default
                      }
                    )
                wow.init();


            });
        </script>
    </form>
</body>
</html>
