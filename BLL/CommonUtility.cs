﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DAL;
using System.Data.Objects.SqlClient;

namespace BLL
{
    public static class CommonUtility
    {
        public static IEnumerable<SelectListItem> GetState()
        {
            FinanceEduEntities dbContext = new FinanceEduEntities();
            var state = (from st in dbContext.States where st.IsActive == true && st.CountryID == 1 select new SelectListItem { Text = st.Name, Value = SqlFunctions.StringConvert((double)st.ID).Trim() }).AsEnumerable();
            return state;
        }

        public static IEnumerable<SelectListItem> GetCity(int StateId)
        {
            FinanceEduEntities dbContext = new FinanceEduEntities();
            var cities = (from city in dbContext.Cities where city.StateID == StateId select new SelectListItem { Text = city.Name, Value = SqlFunctions.StringConvert((double)city.ID).Trim() }).AsEnumerable();
            return cities;
        }

        public static IEnumerable<SelectListItem> GetAddressProof()
        {
            FinanceEduEntities dbContext = new FinanceEduEntities();
            var addressProof = (from ap in dbContext.AddressProofMasters where ap.IsActive == true select new SelectListItem { Text = ap.Title, Value = SqlFunctions.StringConvert((double)ap.ID).Trim() }).AsEnumerable();
            return addressProof;
        }

        public static IEnumerable<SelectListItem> GetCourse()
        {
            FinanceEduEntities dbContext = new FinanceEduEntities();
            var Course = (from cs in dbContext.Courses select new SelectListItem { Text = cs.CourseName, Value = SqlFunctions.StringConvert((double)cs.CourseId).Trim() }).AsEnumerable();
            return Course;
        }

        public static IEnumerable<SelectListItem> GetSubject()
        {
            FinanceEduEntities dbContext = new FinanceEduEntities();
            var Subject = (from sb in dbContext.Subjects where sb.IsActive == true select new SelectListItem { Text = sb.Name, Value = SqlFunctions.StringConvert((double)sb.ID).Trim() }).AsEnumerable();
            return Subject;
        }

        public static IEnumerable<SelectListItem> GetTopic()
        {
            FinanceEduEntities dbContext = new FinanceEduEntities();
            var Topic = (from tp in dbContext.Topics select new SelectListItem { Text = tp.Name, Value = SqlFunctions.StringConvert((double)tp.ID).Trim() }).AsEnumerable();
            return Topic;
        }

        public static IEnumerable<SelectListItem> GetContentType()
        {
            FinanceEduEntities dbContext = new FinanceEduEntities();
            var ContentType = (from ctm in dbContext.ContentTypeMasters select new SelectListItem { Text = ctm.Title, Value = SqlFunctions.StringConvert((double)ctm.ID).Trim() }).AsEnumerable();
            return ContentType;
        }

        public static IEnumerable<SelectListItem> GetGender()
        {
            List<GenderMaster> objGenderMaster = new GenderMaster().GenderList();
            var Gender = (from gen in objGenderMaster select new SelectListItem { Text = gen.Name, Value = gen.ID }).AsEnumerable();
            return Gender;
        }
        public static IEnumerable<SelectListItem> GetCourseType()
        {
            FinanceEduEntities dbContext = new FinanceEduEntities();
            var CourseType = (from ctm in dbContext.CourseTypes select new SelectListItem { Text = ctm.Name, Value = SqlFunctions.StringConvert((double)ctm.ID).Trim() }).AsEnumerable();
            return CourseType;
        }

        public static IEnumerable<SelectListItem> GetQuestionLevel()
        {
            FinanceEduEntities dbContext = new FinanceEduEntities();
            var QuestionLvl = (from ql in dbContext.QuestionLevels select new SelectListItem { Text = ql.Title, Value = SqlFunctions.StringConvert((double)ql.ID).Trim() }).AsEnumerable();
            return QuestionLvl;
        }
        public static IEnumerable<SelectListItem> GetTestType()
        {
            FinanceEduEntities dbContext = new FinanceEduEntities();
            var TestTypes = (from ql in dbContext.TestTypes select new SelectListItem { Text = ql.Title, Value = SqlFunctions.StringConvert((double)ql.ID).Trim() }).AsEnumerable();
            return TestTypes;
        }

        public static string GenerateRandomOTP(int iOTPLength, string[] saAllowedCharacters)
        {
            string sOTP = String.Empty;
            string sTempChars = String.Empty;

            Random rand = new Random();
            for (int i = 0; i < iOTPLength; i++)
            {
                int p = rand.Next(0, saAllowedCharacters.Length);
                sTempChars = saAllowedCharacters[rand.Next(0, saAllowedCharacters.Length)];
                sOTP += sTempChars;
            }
            return sOTP;
        }
        public static IEnumerable<SelectListItem> GetNotificationType()
        {
            FinanceEduEntities dbContext = new FinanceEduEntities();
            var NotificationType = (from nt in dbContext.NotificationTypeMasters select new SelectListItem { Text = nt.Title, Value = SqlFunctions.StringConvert((double)nt.ID).Trim() }).AsEnumerable();

            return NotificationType;
        }

        public static IEnumerable<SelectListItem> GetQualificationLevel()
        {
            FinanceEduEntities dbContext = new FinanceEduEntities();
            var QualificationLvl = (from qlm in dbContext.QualificationLevelMasters select new SelectListItem { Text = qlm.Title, Value = SqlFunctions.StringConvert((double)qlm.ID).Trim() }).AsEnumerable();
            return QualificationLvl;
        }

        public static IEnumerable<SelectListItem> GetTeacher()
        {
            FinanceEduEntities dbContext = new FinanceEduEntities();
            var teacher = (from t in dbContext.Teachers select new SelectListItem { Text = t.tblUser.FirstName + " " + t.tblUser.LastName, Value = SqlFunctions.StringConvert((double)t.ID).Trim() }).AsEnumerable();
            //var Teacher = (from ctm in dbContext.tblUsers where ctm.UserTypeId == 5 select new SelectListItem { Text = ctm.FirstName + " " + ctm.LastName, Value = SqlFunctions.StringConvert((double)ctm.ID).Trim() }).AsEnumerable();
            return teacher;
        }
        public static IEnumerable<SelectListItem> GetBatch()
        {
            FinanceEduEntities dbContext = new FinanceEduEntities();
            var batch = (from t in dbContext.BatchDetails select new SelectListItem { Text = t.Title, Value = SqlFunctions.StringConvert((double)t.ID).Trim() }).AsEnumerable();
            
            return batch;
        }

        public static IEnumerable<SelectListItem> GetTimeFromHH()
        {
            List<SelectListItem> lstobj = new List<SelectListItem>();

            for (int i = 5; i < 21; i++)
            {
                string sv = i.ToString().PadLeft(2, '0');
                SelectListItem item = new SelectListItem();
                item.Text = sv;
                item.Value = sv;
                lstobj.Add(item);
            }

            return lstobj;
        }

        public static IEnumerable<SelectListItem> GetTimeFromMM()
        {
            List<SelectListItem> lstobj = new List<SelectListItem>();

            SelectListItem item = new SelectListItem();
            item.Text = "00";
            item.Value = "00";
            lstobj.Add(item);

            SelectListItem item1 = new SelectListItem();
            item1.Text = "15";
            item1.Value = "15";
            lstobj.Add(item1);
            SelectListItem item2 = new SelectListItem();
            item2.Text = "30";
            item2.Value = "30";
            lstobj.Add(item2);
            SelectListItem item3 = new SelectListItem();
            item3.Text = "45";
            item3.Value = "45";
            lstobj.Add(item3);


            return lstobj;
        }

        public static IEnumerable<SelectListItem> GetTimeToHH()
        {
            List<SelectListItem> lstobj = new List<SelectListItem>();

            for (int i = 5; i < 21; i++)
            {
                string sv = i.ToString().PadLeft(2, '0');
                SelectListItem item = new SelectListItem();
                item.Text = sv;
                item.Value = sv;
                lstobj.Add(item);
            }

            return lstobj;
        }

        public static IEnumerable<SelectListItem> GetTimeToMM()
        {

            List<SelectListItem> lstobj = new List<SelectListItem>();

            SelectListItem item = new SelectListItem();
            item.Text = "00";
            item.Value = "00";
            lstobj.Add(item);

            SelectListItem item1 = new SelectListItem();
            item1.Text = "15";
            item1.Value = "15";
            lstobj.Add(item1);
            SelectListItem item2 = new SelectListItem();
            item2.Text = "30";
            item2.Value = "30";
            lstobj.Add(item2);
            SelectListItem item3 = new SelectListItem();
            item3.Text = "45";
            item3.Value = "45";
            lstobj.Add(item3);


            return lstobj;
        }

    }
}
