﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class UserMaster
    {
        public int ID { get; set; }
        public string UserLogo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string Address { get; set; }
        public int? City { get; set; }
        public int? State { get; set; }
        public Nullable<int> Country { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public Nullable<int> UserTypeId { get; set; }
        public Nullable<bool> IsLoggedIn { get; set; }
        public Nullable<System.DateTime> LastLoginDate { get; set; }
        public string DeviceTokenID { get; set; }

        public UserMaster fillEntityToClass(tblUser obj)
        {

            UserMaster objUserMaster = new UserMaster();

            objUserMaster.ID = obj.ID;
            objUserMaster.FirstName = obj.FirstName;
            objUserMaster.LastName = obj.LastName;
            objUserMaster.Email = obj.Email;
            objUserMaster.PhoneNo = obj.PhoneNo;
            objUserMaster.Address = obj.Address;
            objUserMaster.City = obj.CityID;
            objUserMaster.State = obj.StateID;
            objUserMaster.UserTypeId = obj.UserTypeId;
            objUserMaster.LastLoginDate = obj.LastLoginDate;
            objUserMaster.DeviceTokenID = obj.DeviceTokenID;

            return objUserMaster;

        }
    }

    public class GenderMaster
    {
        public string ID { get; set; }
        public string Name { get; set; }

        public List<GenderMaster> GenderList()
        {
            List<GenderMaster> listGen = new List<GenderMaster>();
            GenderMaster male = new GenderMaster() { ID = "1", Name = "Male" };
            listGen.Add(male);
            GenderMaster female = new GenderMaster() { ID = "2", Name = "Female" };
            listGen.Add(female);

            return listGen;
        }
    }

}
