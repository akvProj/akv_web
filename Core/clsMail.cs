﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net.Mail;
using System.Net;

namespace Core
{
    public class clsMail
    {
        #region " Class Data "
        private string _From;
        private string _FromName;
        private string _To;
        private string _ToList;
        private string _Subject;
        private string _CC;
        private string _CCList;
        private string _BCC;
        private string _TemplateDoc;
        private string[] _ArrValues;
        private string _BCCList;
        private bool _MailBodyManualSupply;
        private string _attachment;
        private string _MailBody;
        private System.Net.Mail.MailMessage myEmail;
        #endregion

        #region " Constructors-Destructors "
        public clsMail()
        {  //set defaults
            myEmail = new System.Net.Mail.MailMessage();
            _MailBodyManualSupply = false;
        }
        #endregion

        #region " Properties "

        public string From
        {
            set { _From = value; }
        }
        public string FromName
        {
            set { _FromName = value; }
        }
        public string attachment
        {
            set { _attachment = value; }
        }
        public string To
        {
            set { _To = value; }
        }
        public string Subject
        {
            set { _Subject = value; }
        }
        public string CC
        {
            set { _CC = value; }
        }
        public string BCC
        {
            set { _BCC = value; }
        }
        public bool MailBodyManualSupply
        {
            set { _MailBodyManualSupply = value; }
        }
        public string MailBody
        {
            set { _MailBody = value; }
        }
        //FILE NAME OF TEMPLATE ( MUST RESIDE IN ../EMAILTEMPLAES/ FOLDER )
        public string EmailTemplateFileName
        {
            set { _TemplateDoc = value; }
        }
        //ARRAY OF VALUES TO REPLACE VARS IN TEMPLATE
        public string[] ValueArray
        {
            set { _ArrValues = value; }
        }

        #endregion

        #region SEND EMAIL

        public void Send()
        {
            myEmail.IsBodyHtml = true;

            //set mandatory properties 
            //_From = ConfigurationManager.AppSettings["mailFrom"];
            if (string.IsNullOrEmpty(_FromName)) _FromName = _From;
            myEmail.From = new MailAddress(_From, _FromName);
            myEmail.Subject = _Subject;

            //---Set recipients in To List 
            _ToList = _To.Replace(";", ",");
            if (_ToList != "")
            {
                string[] arr = _ToList.Split(',');
                myEmail.To.Clear();
                if (arr.Length > 0)
                {
                    foreach (string address in arr)
                    {
                        myEmail.To.Add(new MailAddress(address));
                    }
                }
                else
                {
                    myEmail.To.Add(new MailAddress(_ToList));
                }
            }

            //---Set recipients in CC List 
            _CCList = string.IsNullOrEmpty(_CC) ? "" : _CC.Replace(";", ",");
            if (_CCList != "")
            {
                string[] arr = _CCList.Split(',');
                myEmail.CC.Clear();
                if (arr.Length > 0)
                {
                    foreach (string address in arr)
                    {
                        myEmail.CC.Add(new MailAddress(address));
                    }
                }
                else
                {
                    myEmail.CC.Add(new MailAddress(_CCList));
                }
            }

            //---Set recipients in BCC List 
            _BCCList = string.IsNullOrEmpty(_BCC) ? "" : _BCC.Replace(";", ",");
            if (_BCCList != "")
            {
                string[] arr = _BCCList.Split(',');
                myEmail.Bcc.Clear();
                if (arr.Length > 0)
                {
                    foreach (string address in arr)
                    {
                        myEmail.Bcc.Add(new MailAddress(address));
                    }
                }
                else
                {
                    myEmail.Bcc.Add(new MailAddress(_BCCList));
                }
            }

            //set mail body 
            if (_MailBodyManualSupply)
            {
                myEmail.Body = _MailBody;
            }
            else
            {
                myEmail.Body = GetHtml(_TemplateDoc);
                //& GetHtml("EML_Footer.htm") 
            }

            // set attachment 
            if (_attachment != null)
            {
                Attachment objAttach = new Attachment(_attachment);
                myEmail.Attachments.Add(objAttach);
            }

            //Send mail 
            //SmtpClient client = new SmtpClient();

            SmtpClient client = new SmtpClient(SiteSettings.EmailHost, Convert.ToInt32(SiteSettings.EmailPort));

            //client.UseDefaultCredentials = false;
            //client.EnableSsl = true;
            client.Credentials = new NetworkCredential(SiteSettings.EmailUserName, SiteSettings.EmailPassword);

            client.Send(myEmail);
        }
        #endregion

        #region GetHtml
        public string GetHtml(string argTemplateDocument)
        {
            int i;
            StreamReader filePtr;
            string fileData = "";

            filePtr = File.OpenText(HttpContext.Current.Request.PhysicalApplicationPath + "\\EmailTemplate\\" + argTemplateDocument);
            //filePtr = File.OpenText(argTemplateDocument);
            fileData = filePtr.ReadToEnd();

            if ((_ArrValues == null))
            {
                filePtr.Close();
                filePtr = null;
                return fileData;

            }
            else
            {
                for (i = _ArrValues.GetLowerBound(0); i <= _ArrValues.GetUpperBound(0); i++)
                {
                    fileData = fileData.Replace("@v" + i.ToString() + "@", (string)_ArrValues[i]);
                }
                filePtr.Close();
                filePtr = null;
                return fileData;
            }


        }
        #endregion
    }
}
