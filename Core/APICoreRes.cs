﻿
using System;
namespace Core
{
    /// <summary>
    /// 
    /// </summary>
    public class APICoreRes
    {
        public ResStatus status { get; set; }
        public string userMessage { get; set; }
        public string TokenList { get; set; }
        public DateTime RequestTime { get; set; }
        public DateTime ResponseTime { get; set; }

    }

    public class ResponseData
    {
        public ResStatus status { get; set; }
        public string userMessage { get; set; }
        //public APICoreRes coreRes = new APICoreRes();
        public string totalResults { get; set; }
        public dynamic Data { get; set; }
    }

    public class ProfileData
    {
        public UserInfo userInfo { get; set; }
        public ExamProfile examProfile { get; set; }
        public NotificationOption notificationOption { get; set; }

    }
    public class UserInfo
    {
        public string user_id { get; set; }
        public string name { get; set; }
        public string mailid { get; set; }
        public string contact_no { get; set; }
        public string profile_icon { get; set; }
        public string dob { get; set; }
        public string address { get; set; }
        public bool is_student_from_kiosk { get; set; }

    }
    public class ExamProfile
    {
        public int TotalTestAttempted { get; set; }
        public int PointsEarned { get; set; }
        public int TestPassed { get; set; }
        public int AKVRank { get; set; }

    }
    public class NotificationOption
    {
        public bool isallowPhoneNotification { get; set; }
        public bool isallowEmailNotification { get; set; }
    }
    public class UserData {
        public string user_id { get; set; }
    }
    public enum ResStatus
    {
        Success = 1,
        Fail = 0,
        ValidationError = 2
    }
}
