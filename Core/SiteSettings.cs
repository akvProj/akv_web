﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class SiteSettings
    {
        public static string ThumbnailImagePath
        {
            get { return ConfigurationManager.AppSettings["ThumbnailImagePath"].ToString(); }
        }
        public static string UserLogoImagePath
        {
            get { return ConfigurationManager.AppSettings["UserLogoImagePath"].ToString(); }
        }
        public static string StudentPicImagePath
        {
            get { return ConfigurationManager.AppSettings["StudentPicImagePath"].ToString(); }
        }
        public static string ContentFilePath
        {
            get { return ConfigurationManager.AppSettings["ContentFilePath"].ToString(); }
        }
        public static string NotificationImagePath
        {
            get { return ConfigurationManager.AppSettings["NotificationImagePath"].ToString(); }
        }
        public static string QuestionImagePath
        {
            get { return ConfigurationManager.AppSettings["QuestionImagePath"].ToString(); }
        }
        public static string EmailHost
        {
            get { return ConfigurationManager.AppSettings["EmailHost"].ToString(); }
        }
        public static string EmailPort
        {
            get { return ConfigurationManager.AppSettings["EmailPort"].ToString(); }
        }
        public static string EmailUserName
        {
            get { return ConfigurationManager.AppSettings["EmailUserName"].ToString(); }

        }
        public static string EmailPassword
        {
            get { return ConfigurationManager.AppSettings["EmailPassword"].ToString(); }
        }
        public static string ToEmails
        {
            get { return ConfigurationManager.AppSettings["ToEmails"].ToString(); }
        }
        public static string SiteDomainName
        {
            get { return ConfigurationManager.AppSettings["SiteDomainName"].ToString(); }
        }
    }
}