//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class GenerateTestPaper
    {
        public int ID { get; set; }
        public Nullable<int> CourseTypeID { get; set; }
        public Nullable<int> CourseID { get; set; }
        public Nullable<int> SubjectID { get; set; }
        public Nullable<int> TopicID { get; set; }
        public Nullable<int> TestTypeID { get; set; }
        public string Title { get; set; }
        public Nullable<int> QuestionLimit { get; set; }
        public Nullable<int> QuestionLevelID { get; set; }
        public Nullable<int> TestTimeinMinuts { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public string Description { get; set; }
        public Nullable<int> Max_Mark { get; set; }
    }
}
