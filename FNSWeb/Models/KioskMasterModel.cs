﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FNSWeb.Models
{
    public class KioskMasterModel
    {
        public int ID { get; set; }
        [Required]
        [Display(Name = "Kiosk Name")]
        public string KioskName { get; set; }
        
        [Display(Name = "LatLong")]
        public string LatLong { get; set; }
        
        [Display(Name = "Thumbnail")]
        public string Thumbnail { get; set; }
        //[Required]
        [Display(Name = "Kiosk Registration No")]
        public string KioskRegistrationNO { get; set; }

        [Display(Name = "DC Name")]
        public string DCIdentity { get; set; }
        [Display(Name = "SC Name")]
        public string SCIdentity { get; set; }
        [Display(Name = "Payment Transaction ID")]
        public string PaymentTransactionID { get; set; }
        [Required]
        [Display(Name = "Kiosk Address")]
        [DataType(DataType.MultilineText)]
        public string Address { get; set; }
        public HttpPostedFileBase ThumbnailImageFile { get; set; }
        public HttpPostedFileBase UserLogoImageFile { get; set; }
        public Nullable<System.DateTime> Created { get; set; }

        public UserMasterModel UserDetail { get; set; }

    }
}