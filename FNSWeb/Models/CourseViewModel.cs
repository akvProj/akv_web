﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FNSWeb.Models
{
    public class CourseViewModel
    {
        public int ID { get; set; }
        [Required]
        [Display(Name = "Course Name")]
        public string CourseName { get; set; }
        [Display(Name = "Description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Course Type")]
        public int CourseTypeID { get; set; }
        [Display(Name = "Course Type")]
        public string CourseType { get; set; }
        [Required]
        [Display(Name = "Course Time in Month")]
        public int CourseTimeinMonth { get; set; }
        [Required]
        [Display(Name = "Course Fees")]
        public decimal CoursePriceAmount { get; set; }
        public DateTime Created { get; set; }

        public List<SubjectViewModel> lstSubjectViewModel { get; set; }
    }
}