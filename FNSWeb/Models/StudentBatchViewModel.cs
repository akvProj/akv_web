﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FNSWeb.Models
{
    public class StudentBatchViewModel
    {
        public int ID { get; set; }

        public int StudentID { get; set; }

        public int BatchID { get; set; }

        
        [Display(Name = "Course Taken Amount")]
        public decimal CourseTakenAmount { get; set; }
        
        [Display(Name = "Deposit Amount")]
        public decimal DepositAmount { get; set; }

        public Nullable<bool> IsStarted { get; set; }
        public Nullable<bool> IsJoined { get; set; }
        public DateTime Created { get; set; }

    }
}