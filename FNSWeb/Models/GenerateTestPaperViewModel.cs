﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FNSWeb.Models
{
    public class GenerateTestPaperViewModel
    {
        public int ID { get; set; }
        public int? CourseID { get; set; }
        public int? SubjectID { get; set; }
        public int? TopicID { get; set; }
        public int? CourseTypeID { get; set; }
        public int? TestTypeID { get; set; }
        public int? QuestionLevelID { get; set; }

        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }
        [Display(Name = "Description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Questions in Test Paper")]
        public int QuestionLimit { get; set; }

        [Required]
        [Display(Name = "Test completion in Mins")]
        public int TestTimeinMinuts { get; set; }

        [Display(Name = "Course Type")]
        public string courseTypeName { get; set; }
        [Display(Name = "Question Level")]
        public string questionLevelName { get; set; }
        [Display(Name = "Course Name")]
        public string courseName { get; set; }
        [Display(Name = "Subject Name")]
        public string subjectName { get; set; }
        [Display(Name = "Topic Name")]
        public string topicName { get; set; }
        [Display(Name = "Test Type")]
        public string testTypeName { get; set; }
        [Display(Name = "Created On")]
        public DateTime Created { get; set; }
    }
}