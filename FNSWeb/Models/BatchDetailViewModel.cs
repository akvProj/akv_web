﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace FNSWeb.Models
{
    public class BatchDetailViewModel
    {
        public int ID { get; set; }

        [Required]
        public int CourseID { get; set; }

        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Display(Name = "Batch Time")]
        public string TimeFromHH { get; set; }
        public string TimeFromMM { get; set; }

       


        [Display(Name = "Course Name")]
        public string CourseName { get; set; }

        [Required]
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }

        [Required]
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }

        [Display(Name = "Is Started")]
        public bool IsStarted { get; set; }

        [Display(Name = "Is Closed")]
        public bool IsClosed { get; set; }

        [Display(Name = "Created On")]
        public DateTime Created { get; set; }
    }
}