﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FNSWeb.Models
{
    public class QuestionMasterViewModel
    {
        public int ID { get; set; }
        [Required]
        [Display(Name = "Course")]
        public int CourseID { get; set; }
        [Required]
        [Display(Name = "Subject")]
        public int SubjectID { get; set; }
        [Required]
        [Display(Name = "Topic")]
        public int TopicID { get; set; }
        [Required]
        [Display(Name = "Teacher")]
        public int TeacherId { get; set; }
        [Required]
        [Display(Name = "Question Level")]
        public int QuestionLevelID { get; set; }
        [Required]
        [Display(Name = "Question Title")]
        public string QueTitle { get; set; }

        [Display(Name = "Extra Notes")]
        public string ExtraNotesDescription { get; set; }
        [Required]
        [Display(Name = "Question Opt_1")]
        public string QueOpt_1 { get; set; }
        [Required]
        [Display(Name = "Question Opt_2")]
        public string QueOpt_2 { get; set; }
        [Required]
        [Display(Name = "Question Opt_3")]
        public string QueOpt_3 { get; set; }
        [Required]
        [Display(Name = "Question Opt_4")]
        public string QueOpt_4 { get; set; }

        [Display(Name = "Question Opt_5")]
        public string QueOpt_5 { get; set; }

        [Required]
        [Display(Name = "Correct Option No")]
        public int CorrectOptionNo { get; set; }

        [Display(Name = "Correct Option Explanation")]
        public string CorrectOptionExplanation { get; set; }

        public Nullable<bool> IsActive { get; set; }
        public DateTime Created { get; set; }
        public string questionImage { get; set; }
        public HttpPostedFileBase questionImageFile { get; set; }
        public string answerOpt_1_Image { get; set; }
        public HttpPostedFileBase answerOpt_1_ImageFile { get; set; }
        public string answerOpt_2_Image { get; set; }
        public HttpPostedFileBase answerOpt_2_ImageFile { get; set; }
        public string answerOpt_3_Image { get; set; }
        public HttpPostedFileBase answerOpt_3_ImageFile { get; set; }
        public string answerOpt_4_Image { get; set; }
        public HttpPostedFileBase answerOpt_4_ImageFile { get; set; }
        public string correct_Opt_Image { get; set; }
        public HttpPostedFileBase correct_Opt_ImageFile { get; set; }
        public QuestionLevelViewModel questionLevelViewModel { get; set; }
        public TeacherViewModel teacherViewModel { get; set; }
        public CourseViewModel courseViewModel { get; set; }
        public SubjectViewModel subjectViewModel { get; set; }
        public TopicViewModel topicViewModel { get; set; }
    }
}