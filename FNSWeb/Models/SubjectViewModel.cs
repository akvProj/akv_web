﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FNSWeb.Models
{
    public class SubjectViewModel
    {

        public int ID { get; set; }
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
        public DateTime Created { get; set; }

        public List<CourseViewModel> lstCourseViewModel { get; set; }
        public List<TopicViewModel> lstTopicViewModel { get; set; }
        public List<TeacherViewModel> lstTeacherViewModel { get; set; } 
    }
}