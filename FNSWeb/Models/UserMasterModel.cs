﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace FNSWeb.Models
{
    public class UserMasterModel
    {
        public int ID { get; set; }
        public string UserLogo { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        [System.Web.Mvc.Remote("IsEmailExist", "UserMaster", AdditionalFields = "ID", HttpMethod = "GET", ErrorMessage = "This Email already exist")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "PhoneNo")]
        [Phone]
        public string PhoneNo { get; set; }
        public string Address { get; set; }
        [Required]
        [Display(Name = "City")]
        public int? City { get; set; }
        [Required]
        [Display(Name = "State")]
        public int? State { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
        public Nullable<int> Country { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public Nullable<int> UserTypeId { get; set; }
        public Nullable<bool> IsLoggedIn { get; set; }
        public Nullable<System.DateTime> LastLoginDate { get; set; }
        public HttpPostedFileBase tblUserLogoImageFile { get; set; }
        public string DeviceTokenID { get; set; }
    }
}
