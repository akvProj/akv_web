﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FNSWeb.Models
{
    
    public class StudentViewModel
    {
        public int StudentID { get; set; }
        [Required]
        [Display(Name = "Student Name")]
        public string StudentName { get; set; }
        [Required]
        [Display(Name = "Mother Name")]
        public string MotherName { get; set; }
        [Required]
        [Display(Name = "Father Name")]
        public string FatherName { get; set; }
        [Required]
        [Display(Name = "Mobile No")]
        [Phone]
        public string MobileNo { get; set; }
        [Required]
        [Display(Name = "Date of Birth")]
        public string DOB { get; set; }
        [Display(Name = "Mailing Address")]
        public string MailingAddress { get; set; }
        [Display(Name = "Permanent Address")]
        public string PermanentAddress { get; set; }
        [Display(Name = "Address Proof")]
        public Nullable<int> AddressProofID { get; set; }
        [Display(Name = "Address ProofT ext")]
        public string AddressProofText { get; set; }
        public string CreatedBy { get; set; }
        
        [Display(Name = "User Name")]
        public string UserName { get; set; }
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }
        //[Required]
        //[DataType(DataType.Password)]
        //[Display(Name = "Password")]
        public string Password { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CityID { get; set; }
        public Nullable<int> StateID { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public string StudentPic { get; set; }
        public HttpPostedFileBase StudentPicImageFile { get; set; }
        public Nullable<int> Gender { get; set; }
    }

    

}