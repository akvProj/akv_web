﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FNSWeb.Models
{
    public class ContentMasterViewModel
    {
        public int ID { get; set; }
        [Required]
        [Display(Name = "Course")]
        public int CourseID { get; set; }
        [Required]
        [Display(Name = "Subject")]
        public int SubjectID { get; set; }
        [Required]
        [Display(Name = "Topic")]
        public int TopicID { get; set; }
        [Required]
        [Display(Name = "Teacher")]
        public int TeacherId { get; set; }
        [Display(Name = "Content Type Master")]
        public int ContentTypeMasterID { get; set; }

        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }

        public string FileName { get; set; }

        [Display(Name = "File Sequence No")]
        public int SequenceNo { get; set; }

        public Nullable<bool> IsActive { get; set; }
        public DateTime Created { get; set; }
        public HttpPostedFileBase[] ContentDocument { get; set; }
        public TeacherViewModel teacherViewModel { get; set; }
        public CourseViewModel courseViewModel { get; set; }
        public SubjectViewModel subjectViewModel { get; set; }
        public TopicViewModel topicViewModel { get; set; }
        public ContentTypeviewmodel contentTypeviewmodel { get; set; }
    }
}