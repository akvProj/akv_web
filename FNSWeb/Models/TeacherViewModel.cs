﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FNSWeb.Models
{
    public class TeacherViewModel
    {
        public int ID { get; set; }
        public int UserID { get; set; }
       // [Required]
        [Display(Name = "Qualification Level")]
        public int QualificationLevelID { get; set; }
        //[Required]
        [Display(Name = "Qualification Detail")]
        public string QualificationDetail { get; set; }
       
        [Required]
        [Display(Name = "Experience Detail")]
        [DataType(DataType.MultilineText)]
        public string ExperienceDetail { get; set; }
       // [Required]
        [Display(Name = "Date of birth")]
        public string DOB { get; set; }
        public DateTime Created { get; set; }
        public HttpPostedFileBase UserLogoImageFile { get; set; }

        public QualificationLevelViewModel qualificationLevelViewModel { get; set; }
        public List<SubjectViewModel> lstSubjectViewModel { get; set; }
        public UserMasterModel userMasterModel { get; set; } 
    }
}