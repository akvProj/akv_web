﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FNSWeb.Models
{
    public class NotificationMasterViewModel
    {

        public int ID { get; set; }
        [Display(Name = "Image")]
        public string Image { get; set; }
        [Required]
        [Display(Name = "Title")]
        public string Title { get; set; }
        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }

        public string NotificationTypeMasterID { get; set; }

        public bool IsActive { get; set; }

        [Display(Name = "Created On")]
        public DateTime Created { get; set; }

        public HttpPostedFileBase ImageFile { get; set; }

        public NotificationTypeViewModel notificationTypeViewModel { get; set; }
    }
}