﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using DAL;
using FNSWeb.Models;

namespace FNSWeb.Controllers
{
    public class AccountController : Controller
    {
        private IRepository<tblUser> _repository;
        public AccountController()
        {
            this._repository = new Repository<tblUser>();
        }
        // GET: Account/Create
        public ActionResult Login()
        {
            FinanceEduEntities context = new FinanceEduEntities();
            System.Data.Objects.ObjectParameter myOutputParamInt = new System.Data.Objects.ObjectParameter("TotalRecords", typeof(Int32));
            var ResultData = context.SP_GetTestPapers(1, 10, 1, myOutputParamInt);

            Session.Clear();
            Session.Abandon();

            return View();
        }

        // POST: Account/Create
        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    // TODO: Add insert logic here

                    tblUser obj = _repository.GetAll().ToList().Where(x => x.Email == model.Email && x.LoginPassword == model.Password && x.IsActive == true).FirstOrDefault();//&& x.IsLoggedIn == false
                    if (obj != null)
                    {

                        obj.IsLoggedIn = true;
                        obj.LastLoginDate = DateTime.Now;
                        _repository.Update(obj);

                        UserMaster objUserMaster = new UserMaster().fillEntityToClass(obj);

                        Session["UserMaster"] = objUserMaster;

                        return RedirectToAction("Index", "Dashboard");
                    }
                    else
                        return View();
                }
                catch (Exception ex)
                {
                    return View(ex.Message + ex.InnerException);
                }
            }
            return View();
        }
        // GET: Account/Create
        public ActionResult ForgotPassword()
        {
            return View();
        }

        // POST: Account/Create
        [HttpPost]
        public ActionResult ForgotPassword(ForgotViewModel model)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
