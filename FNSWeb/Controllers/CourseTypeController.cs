﻿using DAL;
using FNSWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;

namespace FNSWeb.Controllers
{
    [SessionAuthorize]
    public class CourseTypeController : Controller
    {
        private IRepository<CourseType> _repositoryCourseType;

        public CourseTypeController()
        {
            this._repositoryCourseType = new Repository<CourseType>();
        }
        // GET: /Course Type/
        public ActionResult Index()
        {
            List<CourseType> listObj = _repositoryCourseType.GetAll().ToList();
            return View(listObj);
        }

        //
        // GET: /Course Type/Details/5
        public ActionResult Details(int id)
        {
            CourseTypeViewModel model = new CourseTypeViewModel();
            CourseType objCourseType = _repositoryCourseType.GetById(id);
            model.Name = objCourseType.Name;
            model.Description = objCourseType.Description;
            model.Created = Convert.ToDateTime(objCourseType.Created);

            return View(model);
        }

        //
        // GET: /Course Type/Create
        public ActionResult Create()
        {
            CourseTypeViewModel model = new CourseTypeViewModel();
            return View(model);
        }

        //
        // POST: /Course Type/Create
        [HttpPost]
        public ActionResult Create(CourseTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    CourseType objCourseType = new CourseType();

                    objCourseType.Name = model.Name;
                    objCourseType.Description = model.Description;

                    objCourseType.Created = DateTime.Now;

                    _repositoryCourseType.Insert(objCourseType);

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }

        //
        // GET: /Course Type/Edit/5
        public ActionResult Edit(int id)
        {
            CourseType objCourseType = _repositoryCourseType.GetById(id);

            CourseTypeViewModel model = new CourseTypeViewModel();
            model.ID = objCourseType.ID;
            model.Name = objCourseType.Name;
            model.Description = objCourseType.Description;

            model.Created = Convert.ToDateTime(objCourseType.Created);

            return View(model);
        }

        //
        // POST: /Course Type/Edit/5
        [HttpPost]
        public ActionResult Edit(CourseTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    CourseType objEdit = _repositoryCourseType.GetById(model.ID);

                    objEdit.Name = model.Name;
                    objEdit.Description = model.Description;

                    _repositoryCourseType.Update(objEdit);

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }
    }
}