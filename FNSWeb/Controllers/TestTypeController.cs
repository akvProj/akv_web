﻿using BLL;
using Core;
using DAL;
using FNSWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FNSWeb.Controllers
{
    [SessionAuthorize]
    public class TestTypeController : Controller
    {
        private IRepository<TestType> _repositoryTestType;

        public TestTypeController()
        {
            this._repositoryTestType = new Repository<TestType>();
        }
        //
        // GET: /TestType/
        public ActionResult Index()
        {
            List<TestType> listObj = _repositoryTestType.GetAll().ToList();
            return View(listObj);
        }

        //
        // GET: /TestType/Details/5
        public ActionResult Details(int id)
        {
            TestTypeViewModel model = new TestTypeViewModel();
            TestType objTestType = _repositoryTestType.GetById(id);
            model.ID = objTestType.ID;
            model.Title = objTestType.Title;
            model.Created = Convert.ToDateTime(objTestType.Created);

            return View(model);
        }

        //
        // GET: /TestType/Create
        public ActionResult Create()
        {
            TestTypeViewModel model = new TestTypeViewModel();
            return View(model);
        }

        //
        // POST: /TestType/Create
        [HttpPost]
        public ActionResult Create(TestTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    TestType objTestType = new TestType();

                    objTestType.Title = model.Title;

                    objTestType.Created = DateTime.Now;

                    _repositoryTestType.Insert(objTestType);

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }

        //
        // GET: /TestType/Edit/5
        public ActionResult Edit(int id)
        {
            TestType objTestType = _repositoryTestType.GetById(id);

            TestTypeViewModel model = new TestTypeViewModel();
            model.ID = objTestType.ID;
            model.Title = objTestType.Title;
            return View(model);
        }

        //
        // POST: /TestType/Edit/5
        [HttpPost]
        public ActionResult Edit(TestTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    TestType objEdit = _repositoryTestType.GetById(model.ID);

                    objEdit.Title = model.Title;

                    _repositoryTestType.Update(objEdit);

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }
    }
}