﻿using BLL;
using Core;
using DAL;
using FNSWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FNSWeb.Controllers
{
    [SessionAuthorize]
    public class UserMasterController: Controller
    {
        private IRepository<tblUser> _repositoryUser;

        public UserMasterController()
        {
            this._repositoryUser = new Repository<tblUser>();
        }
        //
        // GET: /tblUser/
        public ActionResult Index()
        {
            List<tblUser> listObj = _repositoryUser.GetAll().ToList();
            return View(listObj);
        }

        //
        // GET: /tblUser/Details/5
        public ActionResult Details(int id)
        {
            tblUser objUser = _repositoryUser.GetById(id);
            return View(objUser);
        }

        //
        // GET: /tblUser/Create
        public ActionResult Create()
        {
            UserMasterModel objUser = new UserMasterModel();
            return View(objUser);
        }

        //
        // POST: /tblUser/Create
        [HttpPost]
        public ActionResult Create(UserMasterModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string[] saAllowedCharacters = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };

                    tblUser objUser = new tblUser();

                    objUser.FirstName = model.FirstName;
                    objUser.LastName = model.LastName;
                    objUser.Email = model.Email;
                    objUser.LoginPassword = CommonUtility.GenerateRandomOTP(8, saAllowedCharacters);
                    objUser.UserLogo = model.UserLogo;
                    objUser.PhoneNo = model.PhoneNo;
                    objUser.StateID = model.State;
                    objUser.CityID = model.City;
                    objUser.CountryID = 1;
                    objUser.Address = model.Address;
                    objUser.Created = DateTime.Now;
                    objUser.UserTypeId = 2;
                    objUser.IsActive = true;
                    objUser.IsLoggedIn = false;

                    _repositoryUser.Insert(objUser);

                    string FileExtension = Path.GetExtension(model.tblUserLogoImageFile.FileName);
                    objUser.UserLogo = Convert.ToString(objUser.ID) + "-" + "UserLogo" + FileExtension;
                    string UploadPath = Server.MapPath(SiteSettings.UserLogoImagePath);
                    string FilePath = UploadPath + objUser.UserLogo;
                    model.tblUserLogoImageFile.SaveAs(FilePath);

                    _repositoryUser.Update(objUser);
                    
                    
                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }

        //
        // GET: /tblUser/Edit/5
        public ActionResult Edit(int id)
        {
            tblUser objUser = _repositoryUser.GetById(id);

            UserMasterModel userModel = new UserMasterModel();
            userModel.ID = objUser.ID;
            userModel.FirstName = objUser.FirstName;
            userModel.LastName = objUser.LastName;
            userModel.Email = objUser.Email;
            userModel.PhoneNo = objUser.PhoneNo;
            userModel.Address = objUser.Address;
            userModel.City = objUser.CityID;
            userModel.State = objUser.StateID;
            userModel.UserLogo = objUser.UserLogo;

            return View(userModel);
        }

        //
        // POST: /tblUser/Edit/5
        [HttpPost]
        public ActionResult Edit(UserMasterModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    tblUser objUser = _repositoryUser.GetById(model.ID);

                    objUser.FirstName = model.FirstName;
                    objUser.LastName = model.LastName;
                    objUser.Email = model.Email;
                    objUser.PhoneNo = model.PhoneNo;
                    objUser.StateID = model.State;
                    objUser.CityID = model.City;
                    objUser.Address = model.Address;

                    if (model.tblUserLogoImageFile != null)
                    {
                        string FileExtension = Path.GetExtension(model.tblUserLogoImageFile.FileName);
                        objUser.UserLogo = Convert.ToString(objUser.ID) + "-" + "UserLogo" + FileExtension;
                        string UploadPath = Server.MapPath(SiteSettings.UserLogoImagePath);
                        string FilePath = UploadPath + objUser.UserLogo;
                        model.tblUserLogoImageFile.SaveAs(FilePath);
                    }
                    _repositoryUser.Update(objUser);

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }

        [HttpGet]
        public JsonResult IsEmailExist()
        {
            string fieldName = Request.QueryString.Keys[0];
            string Email = Request.QueryString[fieldName];
            string UserId = Request.QueryString.Keys[1];
            int ID = Request.QueryString[UserId] == "undefined" ? 0 : Convert.ToInt32(Request.QueryString[UserId]);
            FinanceEduEntities dbContext = new FinanceEduEntities();
            bool isExist = false;
            //var exist = dbContext.tblUsers.Where(u => u.Email == Email).Count() != 0;
            int exist = 0;
            if (ID > 0)
            {
                exist = dbContext.tblUsers.Where(u => u.ID != ID && u.Email.ToUpper().Equals(Email.ToUpper())).Count();
            }
            else
            {
                exist = dbContext.tblUsers.Where(u => u.Email.ToUpper().Equals(Email.ToUpper())).Count();
            }
            if (exist > 0)
            {
                isExist = true;
            }
            return Json(!isExist, JsonRequestBehavior.AllowGet);
        }
    }
}
