﻿using BLL;
using Core;
using DAL;
using FNSWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FNSWeb.Controllers
{
    [SessionAuthorize]
    public class ContentMasterController : Controller
    {
        private IRepository<ContentMaster> _repositoryContent;
        

        public ContentMasterController()
        {
            this._repositoryContent = new Repository<ContentMaster>();
            
        }
        //
        // GET: /ContentMaster/
        public ActionResult Index()
        {
            List<ContentMaster> listObj = _repositoryContent.GetAll().ToList();
            return View(listObj);
        }

        //
        // GET: /ContentMaster/Details/5
        public ActionResult Details(int id)
        {
            ContentMaster objContent = _repositoryContent.GetById(id);
            return View(objContent);
        }

        //
        // GET: /ContentMaster/Create
        public ActionResult Create()
        {
            ContentMasterViewModel model = new ContentMasterViewModel();
            return View(model);
        }

        //
        // POST: /ContentMaster/Create
        [HttpPost]
        public ActionResult Create(ContentMasterViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ContentMaster objContentMaster = new ContentMaster();

                    objContentMaster.CourseID = model.CourseID;
                    objContentMaster.SubjectID = model.SubjectID;
                    objContentMaster.TopicID = model.TopicID;
                    objContentMaster.TeacherId = model.TeacherId;
                    //objContentMaster.ContentTypeMasterID = model.ContentTypeMasterID;
                    objContentMaster.Title = model.Title;
                    objContentMaster.Description = model.Description;
                    //objContentMaster.SequenceNo = model.SequenceNo;
                    objContentMaster.IsActive = true;
                    objContentMaster.Created = DateTime.Now;

                    _repositoryContent.Insert(objContentMaster);
                    string filesName = string.Empty;
                    foreach (HttpPostedFileBase file in model.ContentDocument)
                    {
                        //string FileExtension = Path.GetExtension(file.FileName);
                        string prefix = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 8);

                        objContentMaster.FileName = Convert.ToString(objContentMaster.ID) + "_" + prefix + file.FileName;
                        string UploadPath = Server.MapPath(SiteSettings.ContentFilePath);
                        string FilePath = UploadPath + objContentMaster.FileName;
                        filesName += objContentMaster.FileName + "|";
                        file.SaveAs(FilePath);
                    }
                    objContentMaster.FileName = filesName; 
                    _repositoryContent.Update(objContentMaster);

                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    throw ex;
                    //return View(model);
                }
            }
            return View(model);
        }

        //
        // GET: /ContentMaster/Edit/5
        public ActionResult Edit(int id)
        {
            ContentMaster model = _repositoryContent.GetById(id);

            ContentMasterViewModel objContentMaster = new ContentMasterViewModel();
            objContentMaster.ID = model.ID;
            objContentMaster.CourseID = Convert.ToInt32(model.CourseID);
            objContentMaster.SubjectID = Convert.ToInt32(model.SubjectID);
            objContentMaster.TopicID = Convert.ToInt32(model.TopicID);
            objContentMaster.TeacherId = Convert.ToInt32(model.TeacherId);
            //objContentMaster.ContentTypeMasterID = Convert.ToInt32(model.ContentTypeMasterID);
            objContentMaster.Title = model.Title;
            objContentMaster.Description = model.Description;
            //objContentMaster.SequenceNo = Convert.ToInt32(model.SequenceNo);
            objContentMaster.FileName = model.FileName;
            objContentMaster.IsActive = model.IsActive;
            

            return View(objContentMaster);
        }

        //
        // POST: /ContentMaster/Edit/5
        [HttpPost]
        public ActionResult Edit(ContentMasterViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ContentMaster objEdit = _repositoryContent.GetById(model.ID);

                    objEdit.CourseID = model.CourseID;
                    objEdit.SubjectID = model.SubjectID;
                    objEdit.TopicID = model.TopicID;
                    objEdit.TeacherId = model.TeacherId;
                    //objEdit.ContentTypeMasterID = model.ContentTypeMasterID;
                    objEdit.Title = model.Title;
                    objEdit.Description = model.Description;
                    //objEdit.SequenceNo = model.SequenceNo;

                    if (model.ContentDocument[0] != null)
                    {
                        string filesName = string.Empty;
                        foreach (HttpPostedFileBase file in model.ContentDocument)
                        {
                            string prefix = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 8);
                            //string FileExtension = Path.GetExtension(file.FileName);
                            model.FileName = Convert.ToString(model.ID) + "_" + prefix + file.FileName;
                            string UploadPath = Server.MapPath(SiteSettings.ContentFilePath);
                            string FilePath = UploadPath + model.FileName;
                            filesName += model.FileName + "|";
                            file.SaveAs(FilePath);
                        }
                        objEdit.FileName += filesName;
                    }
                    _repositoryContent.Update(objEdit);

                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    throw ex;
                    //return View(model);
                }
            }
            return View(model);
        }
       
        public ActionResult DeleteFile(int id, string FileName, string Type)
        {
            ContentMaster model = _repositoryContent.GetById(id);
            model.FileName = model.FileName.Replace(FileName + "|", "");
            if (Type == "1")
            {
                if (System.IO.File.Exists(Server.MapPath(SiteSettings.ContentFilePath + FileName)))
                {
                    System.IO.File.Delete(Server.MapPath(SiteSettings.ContentFilePath + FileName));
                }
                _repositoryContent.Update(model);
            }
            else if (Type == "2")
            {
                string FileExtension = Path.GetExtension(FileName);
                string FilePath = Server.MapPath(SiteSettings.ContentFilePath + FileName);
                //if (FileExtension.ToLower() == ".pdf" || FileExtension.ToLower() == ".jpg" || FileExtension.ToLower() == ".txt")
                //{
                //    //GetReport(FilePath);
                //}
                //else
                //{
                    System.IO.FileInfo file = new System.IO.FileInfo(FilePath);
                    if (file.Exists)
                    {
                        Response.Clear();
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                        Response.AddHeader("Content-Length", file.Length.ToString());
                        Response.ContentType = "application/octet-stream";
                        Response.WriteFile(file.FullName);
                        Response.End();
                    }
                //}
            }
            return Redirect("/ContentMaster/Edit?id=" + id);
        }

        public FileResult GetReport(string file)
        {
            string FilePath = Server.MapPath(SiteSettings.ContentFilePath + file);
            byte[] FileBytes = System.IO.File.ReadAllBytes(FilePath);
            string FileExtension = Path.GetExtension(file);
            return File(FileBytes, "application/pdf");
        }    
    }
}
