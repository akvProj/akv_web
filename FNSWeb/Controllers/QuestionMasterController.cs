﻿using BLL;
using Core;
using DAL;
using FNSWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FNSWeb.Controllers
{
    [SessionAuthorize]
    public class QuestionMasterController : Controller
    {
        private IRepository<QuestionMaster> _repositoryContent;


        public QuestionMasterController()
        {
            this._repositoryContent = new Repository<QuestionMaster>();

        }
        //
        // GET: /QuestionMaster/
        public ActionResult Index()
        {
            List<QuestionMaster> listObj = _repositoryContent.GetAll().ToList();
            return View(listObj);
        }

        //
        // GET: /QuestionMaster/Details/5
        public ActionResult Details(int id)
        {
            QuestionMaster objContent = _repositoryContent.GetById(id);
            return View(objContent);
        }

        //
        // GET: /QuestionMaster/Create
        public ActionResult Create()
        {
            QuestionMasterViewModel model = new QuestionMasterViewModel();
            return View(model);
        }

        //
        // POST: /QuestionMaster/Create
        [HttpPost]
        public ActionResult Create(QuestionMasterViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    QuestionMaster objQuestionMaster = new QuestionMaster();

                    objQuestionMaster.CourseID = model.CourseID;
                    objQuestionMaster.SubjectID = model.SubjectID;
                    objQuestionMaster.TopicID = model.TopicID;
                    objQuestionMaster.QuestionLevelID = model.QuestionLevelID;
                    objQuestionMaster.TeacherId = model.TeacherId;
                    objQuestionMaster.QueTitle = model.QueTitle;
                    objQuestionMaster.ExtraNotesDescription = "";// model.ExtraNotesDescription;
                    objQuestionMaster.QueOpt_1 = model.QueOpt_1;
                    objQuestionMaster.QueOpt_2 = model.QueOpt_2;
                    objQuestionMaster.QueOpt_3 = model.QueOpt_3;
                    objQuestionMaster.QueOpt_4 = model.QueOpt_4;
                    objQuestionMaster.QueOpt_5 = "";// model.QueOpt_5;
                    objQuestionMaster.CorrectOptionNo = model.CorrectOptionNo;
                    objQuestionMaster.CorrectOptionExplanation = model.CorrectOptionExplanation;
                    objQuestionMaster.IsActive = true;
                    objQuestionMaster.Created = DateTime.Now;

                    if (model.questionImageFile != null)
                    {
                        string FileExtension = Path.GetExtension(model.questionImageFile.FileName);
                        string gd = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10);
                        model.questionImage = "questionImage" + gd + FileExtension;
                        string UploadPath = Server.MapPath(SiteSettings.QuestionImagePath);
                        string FilePath = UploadPath + model.questionImage;
                        model.questionImageFile.SaveAs(FilePath);
                        objQuestionMaster.questionImage = model.questionImage;
                    }

                    if (model.answerOpt_1_ImageFile != null)
                    {
                        string FileExtension = Path.GetExtension(model.answerOpt_1_ImageFile.FileName);
                        string gd = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10);
                        model.answerOpt_1_Image = "answerImage" + gd + FileExtension;
                        string UploadPath = Server.MapPath(SiteSettings.QuestionImagePath);
                        string FilePath = UploadPath + model.answerOpt_1_Image;
                        model.answerOpt_1_ImageFile.SaveAs(FilePath);
                        objQuestionMaster.answerOpt_1_Image = model.answerOpt_1_Image;
                    }

                    if (model.answerOpt_2_ImageFile != null)
                    {
                        string FileExtension = Path.GetExtension(model.answerOpt_2_ImageFile.FileName);
                        string gd = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10);
                        model.answerOpt_2_Image = "answerImage" + gd + FileExtension;
                        string UploadPath = Server.MapPath(SiteSettings.QuestionImagePath);
                        string FilePath = UploadPath + model.answerOpt_2_Image;
                        model.answerOpt_2_ImageFile.SaveAs(FilePath);
                        objQuestionMaster.answerOpt_2_Image = model.answerOpt_2_Image;
                    }
                    if (model.answerOpt_3_ImageFile != null)
                    {
                        string FileExtension = Path.GetExtension(model.answerOpt_3_ImageFile.FileName);
                        string gd = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10);
                        model.answerOpt_3_Image = "answerImage" + gd + FileExtension;
                        string UploadPath = Server.MapPath(SiteSettings.QuestionImagePath);
                        string FilePath = UploadPath + model.answerOpt_3_Image;
                        model.answerOpt_3_ImageFile.SaveAs(FilePath);
                        objQuestionMaster.answerOpt_3_Image = model.answerOpt_3_Image;
                    }
                    if (model.answerOpt_4_ImageFile != null)
                    {
                        string FileExtension = Path.GetExtension(model.answerOpt_4_ImageFile.FileName);
                        string gd = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10);
                        model.answerOpt_4_Image = "answerImage" + gd + FileExtension;
                        string UploadPath = Server.MapPath(SiteSettings.QuestionImagePath);
                        string FilePath = UploadPath + model.answerOpt_4_Image;
                        model.answerOpt_4_ImageFile.SaveAs(FilePath);
                        objQuestionMaster.answerOpt_4_Image = model.answerOpt_4_Image;
                    }
                    if (model.correct_Opt_ImageFile != null)
                    {
                        string FileExtension = Path.GetExtension(model.correct_Opt_ImageFile.FileName);
                        string gd = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10);
                        model.correct_Opt_Image = "answerImage" + gd + FileExtension;
                        string UploadPath = Server.MapPath(SiteSettings.QuestionImagePath);
                        string FilePath = UploadPath + model.correct_Opt_Image;
                        model.correct_Opt_ImageFile.SaveAs(FilePath);
                        objQuestionMaster.correct_Opt_Image = model.correct_Opt_Image;
                    }
                    _repositoryContent.Insert(objQuestionMaster);

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }

        //
        // GET: /QuestionMaster/Edit/5
        public ActionResult Edit(int id)
        {
            QuestionMaster model = _repositoryContent.GetById(id);

            QuestionMasterViewModel objQuestionMaster = new QuestionMasterViewModel();
            objQuestionMaster.ID = model.ID;
            objQuestionMaster.CourseID = Convert.ToInt32(model.CourseID);
            objQuestionMaster.SubjectID = Convert.ToInt32(model.SubjectID);
            objQuestionMaster.TopicID = Convert.ToInt32(model.TopicID);
            objQuestionMaster.QuestionLevelID = Convert.ToInt32(model.QuestionLevelID);
            objQuestionMaster.TeacherId = Convert.ToInt32(model.TeacherId);
            objQuestionMaster.QueTitle = model.QueTitle;
            objQuestionMaster.ExtraNotesDescription = "";// model.ExtraNotesDescription;
            objQuestionMaster.QueOpt_1 = model.QueOpt_1;
            objQuestionMaster.QueOpt_2 = model.QueOpt_2;
            objQuestionMaster.QueOpt_3 = model.QueOpt_3;
            objQuestionMaster.QueOpt_4 = model.QueOpt_4;
            objQuestionMaster.QueOpt_5 = "";// model.QueOpt_5;
            model.questionImage = objQuestionMaster.questionImage;

            model.answerOpt_1_Image = objQuestionMaster.answerOpt_1_Image;
            model.answerOpt_2_Image = objQuestionMaster.answerOpt_2_Image;
            model.answerOpt_3_Image = objQuestionMaster.answerOpt_3_Image;
            model.answerOpt_4_Image = objQuestionMaster.answerOpt_4_Image;
            model.correct_Opt_Image = objQuestionMaster.correct_Opt_Image;

            objQuestionMaster.CorrectOptionNo = Convert.ToInt32(model.CorrectOptionNo);
            objQuestionMaster.CorrectOptionExplanation = model.CorrectOptionExplanation;
            objQuestionMaster.IsActive = model.IsActive;


            return View(objQuestionMaster);
        }

        //
        // POST: /QuestionMaster/Edit/5
        [HttpPost]
        public ActionResult Edit(QuestionMasterViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    QuestionMaster objEdit = _repositoryContent.GetById(model.ID);

                    objEdit.CourseID = model.CourseID;
                    objEdit.SubjectID = model.SubjectID;
                    objEdit.TopicID = model.TopicID;
                    objEdit.QuestionLevelID = model.QuestionLevelID;
                    objEdit.TeacherId = model.TeacherId;
                    objEdit.QueTitle = model.QueTitle;
                    objEdit.ExtraNotesDescription = "";// model.ExtraNotesDescription;
                    objEdit.QueOpt_1 = model.QueOpt_1;
                    objEdit.QueOpt_2 = model.QueOpt_2;
                    objEdit.QueOpt_3 = model.QueOpt_3;
                    objEdit.QueOpt_4 = model.QueOpt_4;
                    objEdit.QueOpt_5 = "";// model.QueOpt_5;
                    objEdit.CorrectOptionNo = model.CorrectOptionNo;
                    objEdit.CorrectOptionExplanation = model.CorrectOptionExplanation;
                    objEdit.IsActive = model.IsActive;

                    if (model.questionImageFile != null)
                    {

                        string FileExtension = Path.GetExtension(model.questionImageFile.FileName);
                        string gd = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10);
                        model.questionImage = "questionImage" + gd + FileExtension;
                        string UploadPath = Server.MapPath(SiteSettings.QuestionImagePath);
                        string FilePath = UploadPath + model.questionImage;
                        model.questionImageFile.SaveAs(FilePath);

                        objEdit.questionImage = model.questionImage;
                    }

                    if (model.answerOpt_1_ImageFile != null)
                    {
                        string FileExtension = Path.GetExtension(model.answerOpt_1_ImageFile.FileName);
                        string gd = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10);
                        model.answerOpt_1_Image = "answerImage" + gd + FileExtension;
                        string UploadPath = Server.MapPath(SiteSettings.QuestionImagePath);
                        string FilePath = UploadPath + model.answerOpt_1_Image;
                        model.answerOpt_1_ImageFile.SaveAs(FilePath);
                        objEdit.answerOpt_1_Image = model.answerOpt_1_Image;
                    }

                    if (model.answerOpt_2_ImageFile != null)
                    {
                        string FileExtension = Path.GetExtension(model.answerOpt_2_ImageFile.FileName);
                        string gd = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10);
                        model.answerOpt_2_Image = "answerImage" + gd + FileExtension;
                        string UploadPath = Server.MapPath(SiteSettings.QuestionImagePath);
                        string FilePath = UploadPath + model.answerOpt_2_Image;
                        model.answerOpt_2_ImageFile.SaveAs(FilePath);
                        objEdit.answerOpt_2_Image = model.answerOpt_2_Image;
                    }
                    if (model.answerOpt_3_ImageFile != null)
                    {
                        string FileExtension = Path.GetExtension(model.answerOpt_3_ImageFile.FileName);
                        string gd = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10);
                        model.answerOpt_3_Image = "answerImage" + gd + FileExtension;
                        string UploadPath = Server.MapPath(SiteSettings.QuestionImagePath);
                        string FilePath = UploadPath + model.answerOpt_3_Image;
                        model.answerOpt_3_ImageFile.SaveAs(FilePath);
                        objEdit.answerOpt_3_Image = model.answerOpt_3_Image;
                    }
                    if (model.answerOpt_4_ImageFile != null)
                    {
                        string FileExtension = Path.GetExtension(model.answerOpt_4_ImageFile.FileName);
                        string gd = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10);
                        model.answerOpt_4_Image = "answerImage" + gd + FileExtension;
                        string UploadPath = Server.MapPath(SiteSettings.QuestionImagePath);
                        string FilePath = UploadPath + model.answerOpt_4_Image;
                        model.answerOpt_4_ImageFile.SaveAs(FilePath);
                        objEdit.answerOpt_4_Image = model.answerOpt_4_Image;
                    }
                    if (model.correct_Opt_ImageFile != null)
                    {
                        string FileExtension = Path.GetExtension(model.correct_Opt_ImageFile.FileName);
                        string gd = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10);
                        model.correct_Opt_Image = "answerImage" + gd + FileExtension;
                        string UploadPath = Server.MapPath(SiteSettings.QuestionImagePath);
                        string FilePath = UploadPath + model.correct_Opt_Image;
                        model.correct_Opt_ImageFile.SaveAs(FilePath);
                        objEdit.correct_Opt_Image = model.correct_Opt_Image;
                    }

                    _repositoryContent.Update(objEdit);


                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }
    }
}
