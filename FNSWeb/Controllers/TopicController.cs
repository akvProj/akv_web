﻿using BLL;
using Core;
using DAL;
using FNSWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FNSWeb.Controllers
{
    [SessionAuthorize]
    public class TopicController : Controller
    {
        private IRepository<Topic> _repositoryTopic;


        public TopicController()
        {
            this._repositoryTopic = new Repository<Topic>();

        }
        //
        // GET: /Topic/
        public ActionResult Index()
        {
            List<Topic> listObj = _repositoryTopic.GetAll().ToList();
            return View(listObj);
        }

        //
        // GET: /Topic/Details/5
        public ActionResult Details(int id)
        {
            TopicViewModel model = new TopicViewModel();
            Topic objTopic = _repositoryTopic.GetById(id);
            model.ID = objTopic.ID;
            model.Name = objTopic.Name;
            model.Description = objTopic.Description;
            model.SubjectName = objTopic.Subject.Name;
            model.Created = Convert.ToDateTime(objTopic.Created);

            return View(model);
        }

        //
        // GET: /Topic/Create
        public ActionResult Create()
        {
            TopicViewModel model = new TopicViewModel();
            return View(model);
        }

        //
        // POST: /Topic/Create
        [HttpPost]
        public ActionResult Create(TopicViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Topic objTopic = new Topic();

                    objTopic.Name = model.Name;
                    objTopic.Description = model.Description;
                    objTopic.SubjectID = Convert.ToInt32(model.SubjectID);
                    objTopic.Created = DateTime.Now;

                    _repositoryTopic.Insert(objTopic);

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }

        //
        // GET: /Topic/Edit/5
        public ActionResult Edit(int id)
        {
            Topic objTopic = _repositoryTopic.GetById(id);

            TopicViewModel model = new TopicViewModel();
            model.ID = objTopic.ID;
            model.Name = objTopic.Name;
            model.Description = objTopic.Description;
            model.SubjectID = Convert.ToString(objTopic.SubjectID);
            model.Created = Convert.ToDateTime(objTopic.Created);

            return View(model);
        }

        //
        // POST: /Topic/Edit/5
        [HttpPost]
        public ActionResult Edit(TopicViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Topic objEdit = _repositoryTopic.GetById(model.ID);

                    objEdit.Name = model.Name;
                    objEdit.Description = model.Description;
                    objEdit.SubjectID = Convert.ToInt32(model.SubjectID);
                    _repositoryTopic.Update(objEdit);

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }
    }
}