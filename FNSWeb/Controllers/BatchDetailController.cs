﻿using BLL;
using Core;
using DAL;
using FNSWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FNSWeb.Controllers
{
    [SessionAuthorize]
    public class BatchDetailController : Controller
    {
        private IRepository<BatchDetail> _repositoryBatchDetail;
        public BatchDetailController()
        {
            this._repositoryBatchDetail = new Repository<BatchDetail>();
        }
        //
        // GET: /BatchDetail/
        public ActionResult Index()
        {
            List<BatchDetail> listObj = _repositoryBatchDetail.GetAll().ToList();
            return View(listObj);
        }

        //
        // GET: /BatchDetail/Create
        public ActionResult Create()
        {
            BatchDetailViewModel model = new BatchDetailViewModel();
            return View(model);
        }
        //
        // GET: /BatchDetail/Details/5
        public ActionResult Details(int id)
        {
            BatchDetailViewModel model = new BatchDetailViewModel();
            BatchDetail objBatchDetail = _repositoryBatchDetail.GetById(id);

            model.ID = objBatchDetail.ID;
            model.Title = objBatchDetail.Title;
            model.CourseName = objBatchDetail.Course.CourseName;
            model.StartDate = Convert.ToDateTime(objBatchDetail.StartDate);
            model.EndDate = Convert.ToDateTime(objBatchDetail.EndDate);

            if (objBatchDetail.TimeFrom != null && objBatchDetail.TimeFrom.Length > 0 && objBatchDetail.TimeFrom.Contains(":"))
            {
                model.TimeFromHH = objBatchDetail.TimeFrom.ToString().Split(':')[0];
                model.TimeFromMM = objBatchDetail.TimeFrom.ToString().Split(':')[1];
            }

            model.IsStarted = Convert.ToBoolean(objBatchDetail.IsStarted);
            model.IsClosed = Convert.ToBoolean(objBatchDetail.IsClosed);
            model.Created = Convert.ToDateTime(objBatchDetail.Created);



            return View(model);
        }
        //
        // POST: /BatchDetail/Create
        [HttpPost]
        public ActionResult Create(BatchDetailViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    BatchDetail objBatchDetail = new BatchDetail();

                    objBatchDetail.Title = model.Title;
                    objBatchDetail.CourseID = model.CourseID;
                    objBatchDetail.StartDate = Convert.ToDateTime(model.StartDate);
                    objBatchDetail.EndDate = Convert.ToDateTime(model.EndDate);
                    objBatchDetail.TimeFrom = model.TimeFromHH + ":" + model.TimeFromMM;

                    objBatchDetail.IsStarted = model.IsStarted;
                    objBatchDetail.IsClosed = model.IsClosed;
                    objBatchDetail.Created = DateTime.Now;

                    _repositoryBatchDetail.Insert(objBatchDetail);

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }

        //
        // GET: /BatchDetail/Edit/5
        public ActionResult Edit(int id)
        {
            BatchDetail objBatchDetail = _repositoryBatchDetail.GetById(id);

            BatchDetailViewModel model = new BatchDetailViewModel();

            model.ID = objBatchDetail.ID;
            model.Title = objBatchDetail.Title;
            model.CourseID = Convert.ToInt32(objBatchDetail.CourseID);
            model.StartDate = Convert.ToDateTime(objBatchDetail.StartDate);
            model.EndDate = Convert.ToDateTime(objBatchDetail.EndDate);

            if (objBatchDetail.TimeFrom != null && objBatchDetail.TimeFrom.Length > 0 && objBatchDetail.TimeFrom.Contains(":"))
            {
                model.TimeFromHH = objBatchDetail.TimeFrom.ToString().Split(':')[0];
                model.TimeFromMM = objBatchDetail.TimeFrom.ToString().Split(':')[1];
            }

            model.IsStarted = Convert.ToBoolean(objBatchDetail.IsStarted);
            model.IsClosed = Convert.ToBoolean(objBatchDetail.IsClosed);


            return View(model);
        }

        //
        // POST: /BatchDetail/Edit/5
        [HttpPost]
        public ActionResult Edit(BatchDetailViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    BatchDetail objBatchDetail = _repositoryBatchDetail.GetById(model.ID);

                    objBatchDetail.Title = model.Title;
                    objBatchDetail.CourseID = model.CourseID;
                    objBatchDetail.StartDate = model.StartDate;
                    objBatchDetail.EndDate = model.EndDate;
                    objBatchDetail.TimeFrom = model.TimeFromHH + ":" + model.TimeFromMM;

                    objBatchDetail.IsStarted = model.IsStarted;
                    objBatchDetail.IsClosed = model.IsClosed;

                    _repositoryBatchDetail.Update(objBatchDetail);

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }
    }
}