﻿using BLL;
using Core;
using DAL;
using FNSWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FNSWeb.Controllers
{
    [SessionAuthorize]
    public class NotificationTypeController : Controller
    {
        private IRepository<NotificationTypeMaster> _repositoryNotificationType;

        public NotificationTypeController()
        {
            this._repositoryNotificationType = new Repository<NotificationTypeMaster>();
        }
        //
        // GET: /NotificationType/
        public ActionResult Index()
        {
            List<NotificationTypeMaster> listObj = _repositoryNotificationType.GetAll().ToList();
            return View(listObj);
        }

        //
        // GET: /NotificationType/Details/5
        public ActionResult Details(int id)
        {
            NotificationTypeViewModel model = new NotificationTypeViewModel();
            NotificationTypeMaster objNotificationTypeMaster = _repositoryNotificationType.GetById(id);
            model.ID = objNotificationTypeMaster.ID;
            model.Title = objNotificationTypeMaster.Title;

            model.Created = Convert.ToDateTime(objNotificationTypeMaster.Created);

            return View(model);
        }

        //
        // GET: /NotificationType/Create
        public ActionResult Create()
        {
            NotificationTypeViewModel model = new NotificationTypeViewModel();
            return View(model);
        }

        //
        // POST: /NotificationType/Create
        [HttpPost]
        public ActionResult Create(NotificationTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    NotificationTypeMaster objNotificationTypeMaster = new NotificationTypeMaster();

                    objNotificationTypeMaster.Title = model.Title;

                    objNotificationTypeMaster.Created = DateTime.Now;

                    _repositoryNotificationType.Insert(objNotificationTypeMaster);

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }

        //
        // GET: /NotificationType/Edit/5
        public ActionResult Edit(int id)
        {
            NotificationTypeMaster objNotificationTypeMaster = _repositoryNotificationType.GetById(id);

            NotificationTypeViewModel model = new NotificationTypeViewModel();
            model.ID = objNotificationTypeMaster.ID;
            model.Title = objNotificationTypeMaster.Title;

            model.Created = Convert.ToDateTime(objNotificationTypeMaster.Created);

            return View(model);
        }

        //
        // POST: /NotificationType/Edit/5
        [HttpPost]
        public ActionResult Edit(NotificationTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    NotificationTypeMaster objEdit = _repositoryNotificationType.GetById(model.ID);

                    objEdit.Title = model.Title;
                    _repositoryNotificationType.Update(objEdit);

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }
    }
}