﻿using BLL;
using Core;
using DAL;
using FNSWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FNSWeb.Controllers
{
    [SessionAuthorize]
    public class KioskMasterController : Controller
    {
        private IRepository<KioskMaster> _repositoryKiosk;
        private IRepository<tblUser> _repositoryUser;

        public KioskMasterController()
        {
            this._repositoryKiosk = new Repository<KioskMaster>();
            this._repositoryUser = new Repository<tblUser>();
        }
        //
        // GET: /KioskMaster/
        public ActionResult Index()
        {
            List<KioskMaster> listObj = _repositoryKiosk.GetAll().ToList();
            return View(listObj);
        }

        //
        // GET: /KioskMaster/Details/5
        public ActionResult Details(int id)
        {
            KioskMaster objKiosk = _repositoryKiosk.GetById(id);
            return View(objKiosk);
        }

        //
        // GET: /KioskMaster/Create
        public ActionResult Create()
        {
            KioskMasterModel model = new KioskMasterModel();
            UserMasterModel objUser = new UserMasterModel();
            model.UserDetail = objUser;
            return View(model);
        }

        //
        // POST: /KioskMaster/Create
        [HttpPost]
        public ActionResult Create(KioskMasterModel model)
        {
            ModelState.Remove("KioskRegistrationNO");
            if (ModelState.IsValid)
            {
                try
                {
                    string[] saAllowedCharacters = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };

                    tblUser objUser = new tblUser();

                    objUser.FirstName = model.UserDetail.FirstName;
                    objUser.LastName = model.UserDetail.LastName;
                    objUser.Email = model.UserDetail.Email;
                    objUser.LoginPassword = CommonUtility.GenerateRandomOTP(8, saAllowedCharacters);
                    objUser.UserLogo = model.UserDetail.UserLogo;
                    objUser.PhoneNo = model.UserDetail.PhoneNo;
                    objUser.StateID = model.UserDetail.State;
                    objUser.CityID = model.UserDetail.City;
                    objUser.CountryID = 1;
                    objUser.Address = model.UserDetail.Address;
                    objUser.Created = DateTime.Now;
                    objUser.UserTypeId = 2;
                    objUser.IsActive = model.UserDetail.IsActive;
                    objUser.IsLoggedIn = false;

                    _repositoryUser.Insert(objUser);

                    KioskMaster objKioskMaster = new KioskMaster();
                    objKioskMaster.UserId = objUser.ID;
                    objKioskMaster.KioskName = model.KioskName;
                    objKioskMaster.LatLong = model.LatLong;
                    //objKioskMaster.KioskregistrationNO = model.KioskRegistrationNO;
                    objKioskMaster.Address = model.Address;
                    objKioskMaster.SCIdentity = model.SCIdentity;
                    objKioskMaster.DCIdentity = model.DCIdentity;
                    objKioskMaster.PaymentTransactionID = model.PaymentTransactionID;
                    objKioskMaster.Created = DateTime.Now;

                    _repositoryKiosk.Insert(objKioskMaster);

                    if (model.UserLogoImageFile != null)
                    {
                        string FileExtension = Path.GetExtension(model.UserLogoImageFile.FileName);
                        objUser.UserLogo = Convert.ToString(objUser.ID) + "-" + "UserLogo" + FileExtension;
                        string UploadPath = Server.MapPath(SiteSettings.UserLogoImagePath);
                        string FilePath = UploadPath + objUser.UserLogo;
                        model.UserLogoImageFile.SaveAs(FilePath);

                        _repositoryUser.Update(objUser);
                    }

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }

        //
        // GET: /KioskMaster/Edit/5
        public ActionResult Edit(int id)
        {
            KioskMaster objKiosk = _repositoryKiosk.GetById(id);
            //tblUser objUser = _repositoryUser.GetAll().Where(x => x.UserRefID == objKiosk.ID && x.UserTypeId == 2).FirstOrDefault();

            UserMasterModel userModel = new UserMasterModel();
            if (objKiosk.tblUser != null)
            {
                userModel.ID = objKiosk.tblUser.ID;
                userModel.FirstName = objKiosk.tblUser.FirstName;
                userModel.LastName = objKiosk.tblUser.LastName;
                userModel.Email = objKiosk.tblUser.Email;
                userModel.PhoneNo = objKiosk.tblUser.PhoneNo;
                userModel.Address = objKiosk.tblUser.Address;
                userModel.City = objKiosk.tblUser.CityID;
                userModel.State = objKiosk.tblUser.StateID;
                userModel.IsActive = Convert.ToBoolean(objKiosk.tblUser.IsActive);
                userModel.UserLogo = objKiosk.tblUser.UserLogo;
            }
            KioskMasterModel model = new KioskMasterModel();
            model.ID = objKiosk.ID;
            model.KioskName = objKiosk.KioskName;
            model.LatLong = objKiosk.LatLong;
            model.SCIdentity = objKiosk.SCIdentity;
            model.DCIdentity = objKiosk.DCIdentity;
            model.PaymentTransactionID = objKiosk.PaymentTransactionID;
            //model.KioskRegistrationNO = objKiosk.KioskregistrationNO;
            model.Address = objKiosk.Address;
            model.UserDetail = userModel;

            return View(model);
        }

        //
        // POST: /KioskMaster/Edit/5
        [HttpPost]
        public ActionResult Edit(KioskMasterModel model)
        {
            ModelState.Remove("KioskRegistrationNO");
            if (ModelState.IsValid)
            {
                try
                {
                    KioskMaster objEdit = _repositoryKiosk.GetById(model.ID);

                    objEdit.KioskName = model.KioskName;
                    objEdit.LatLong = model.LatLong;
                    objEdit.SCIdentity = model.SCIdentity;
                    objEdit.DCIdentity = model.DCIdentity;
                    objEdit.PaymentTransactionID = model.PaymentTransactionID;
                    //objEdit.KioskregistrationNO = model.KioskRegistrationNO;
                    objEdit.Address = model.Address;

                    _repositoryKiosk.Update(objEdit);

                    tblUser objUser = _repositoryUser.GetById(model.UserDetail.ID);
                    if (objUser != null)
                    {
                        objUser.FirstName = model.UserDetail.FirstName;
                        objUser.LastName = model.UserDetail.LastName;
                        objUser.Email = model.UserDetail.Email;
                        objUser.PhoneNo = model.UserDetail.PhoneNo;
                        objUser.StateID = model.UserDetail.State;
                        objUser.CityID = model.UserDetail.City;
                        objUser.Address = model.UserDetail.Address;
                        objUser.IsActive = model.UserDetail.IsActive;

                        if (model.UserLogoImageFile != null)
                        {
                            string FileExtension = Path.GetExtension(model.UserLogoImageFile.FileName);
                            objUser.UserLogo = Convert.ToString(objUser.ID) + "-" + "UserLogo" + FileExtension;
                            string UploadPath = Server.MapPath(SiteSettings.UserLogoImagePath);
                            string FilePath = UploadPath + objUser.UserLogo;
                            model.UserLogoImageFile.SaveAs(FilePath);
                        }
                        _repositoryUser.Update(objUser);
                    }
                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult GetCityList(string stateID)
        {
            var city = BLL.CommonUtility.GetCity(Convert.ToInt32(stateID));
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string result = javaScriptSerializer.Serialize(city);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
