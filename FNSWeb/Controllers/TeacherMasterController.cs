﻿using BLL;
using Core;
using DAL;
using FNSWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FNSWeb.Controllers
{
    [SessionAuthorize]
    public class TeacherMasterController : Controller
    {
        private IRepository<Teacher> _repositoryTeacher;
        private IRepository<tblUser> _repositoryUser;
        private IRepository<vwTeacherSubjectBinding> _repositoryTeacherSubject;

        public TeacherMasterController()
        {
            this._repositoryTeacher = new Repository<Teacher>();
            this._repositoryUser = new Repository<tblUser>();
            this._repositoryTeacherSubject = new Repository<vwTeacherSubjectBinding>();
        }
        //
        // GET: /Teacher/
        public ActionResult Index()
        {
            List<Teacher> listObj = _repositoryTeacher.GetAll().ToList();
            return View(listObj);
        }

        //
        // GET: /Teacher/Details/5
        public ActionResult Details(int id)
        {
            Teacher objTeacher = _repositoryTeacher.GetById(id);
            return View(objTeacher);
        }

        //
        // GET: /Teacher/Create
        public ActionResult Create()
        {
            TeacherViewModel model = new TeacherViewModel();
            UserMasterModel objUser = new UserMasterModel();
            model.userMasterModel = objUser;
            return View(model);
        }

        //
        // POST: /Teacher/Create
        [HttpPost]
        public ActionResult Create(TeacherViewModel model)
        {
            //ModelState["UserMasterModel.PhoneNo"].Errors.Clear();
            //ModelState["UserMasterModel.State"].Errors.Clear();
            //ModelState["UserMasterModel.City"].Errors.Clear();
            //ModelState["UserMasterModel.Address"].Errors.Clear();
            ModelState.Remove("UserMasterModel.PhoneNo");
            ModelState.Remove("UserMasterModel.State");
            ModelState.Remove("UserMasterModel.City");
            ModelState.Remove("UserMasterModel.Address");
            if (ModelState.IsValid)
            {
                try
                {
                    string[] saAllowedCharacters = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };

                    tblUser objUser = new tblUser();

                    objUser.FirstName = model.userMasterModel.FirstName;
                    objUser.LastName = model.userMasterModel.LastName;
                    objUser.Email = model.userMasterModel.Email;
                    objUser.LoginPassword = CommonUtility.GenerateRandomOTP(8, saAllowedCharacters);
                    objUser.UserLogo = ""; //model.userMasterModel.UserLogo;
                    objUser.PhoneNo = ""; //model.userMasterModel.PhoneNo;
                    objUser.StateID = 1;//model.userMasterModel.State;
                    objUser.CityID = 1;// model.userMasterModel.City;
                    objUser.CountryID = 1;
                    objUser.Address = ""; //model.userMasterModel.Address;
                    objUser.Created = DateTime.Now;
                    objUser.UserTypeId = 5;
                    objUser.IsActive = true;
                    objUser.IsLoggedIn = false;

                    _repositoryUser.Insert(objUser);

                    Teacher objTeacher = new Teacher();

                    objTeacher.UserId = objUser.ID;
                    objTeacher.QualificationDetail = ""; //model.QualificationDetail;
                    objTeacher.QualificationLevelID = 1; //model.QualificationLevelID;
                    objTeacher.DOB = ""; //model.DOB;
                    objTeacher.ExperienceDetail = model.ExperienceDetail;
                    objTeacher.Created = DateTime.Now;

                    _repositoryTeacher.Insert(objTeacher);


                    //if (model.UserLogoImageFile.FileName != null)
                    //{
                    //    string FileExtension = Path.GetExtension(model.UserLogoImageFile.FileName);
                    //    objUser.UserLogo = Convert.ToString(objUser.ID) + "-" + "UserLogo" + FileExtension;
                    //    string UploadPath = Server.MapPath(SiteSettings.UserLogoImagePath);
                    //    string FilePath = UploadPath + objUser.UserLogo;
                    //    model.UserLogoImageFile.SaveAs(FilePath);

                    //    _repositoryUser.Update(objUser);
                    //}

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }

        //
        // GET: /Teacher/Edit/5
        public ActionResult Edit(int id)
        {
            Teacher objTeacher = _repositoryTeacher.GetById(id);

            UserMasterModel userModel = new UserMasterModel();
            if (objTeacher != null)
            {
                userModel.ID = objTeacher.tblUser.ID;
                userModel.FirstName = objTeacher.tblUser.FirstName;
                userModel.LastName = objTeacher.tblUser.LastName;
                userModel.Email = objTeacher.tblUser.Email;
                userModel.PhoneNo = ""; //objTeacher.tblUser.PhoneNo;
                userModel.Address = ""; //objTeacher.tblUser.Address;
                userModel.City = 1; //objTeacher.tblUser.CityID;
                userModel.State = 1; //objTeacher.tblUser.StateID;
                userModel.UserLogo = "";//objTeacher.tblUser.UserLogo;
            }
            TeacherViewModel model = new TeacherViewModel();

            model.QualificationDetail = ""; //objTeacher.QualificationDetail;
            model.QualificationLevelID = 1; //Convert.ToInt32(objTeacher.QualificationLevelID);
            model.DOB = "";//objTeacher.DOB;
            model.ExperienceDetail = objTeacher.ExperienceDetail;
            model.userMasterModel = userModel;

            return View(model);
        }

        //
        // POST: /Teacher/Edit/5
        [HttpPost]
        public ActionResult Edit(TeacherViewModel model)
        {
            //ModelState["UserMasterModel.PhoneNo"].Errors.Clear();
            //ModelState["UserMasterModel.State"].Errors.Clear();
            //ModelState["UserMasterModel.City"].Errors.Clear();
            //ModelState["UserMasterModel.Address"].Errors.Clear();
            ModelState.Remove("UserMasterModel.PhoneNo");
            ModelState.Remove("UserMasterModel.State");
            ModelState.Remove("UserMasterModel.City");
            ModelState.Remove("UserMasterModel.Address");
            if (ModelState.IsValid)
            {
                try
                {
                    Teacher objEdit = _repositoryTeacher.GetById(model.ID);

                    objEdit.QualificationDetail = "";// model.QualificationDetail;
                    objEdit.QualificationLevelID = 1; //model.QualificationLevelID;
                    objEdit.DOB = ""; //model.DOB;
                    objEdit.ExperienceDetail = model.ExperienceDetail;

                    _repositoryTeacher.Update(objEdit);

                    tblUser objUser = _repositoryUser.GetById(objEdit.UserId);
                    if (objUser != null)
                    {
                        objUser.FirstName = model.userMasterModel.FirstName;
                        objUser.LastName = model.userMasterModel.LastName;
                        objUser.Email = model.userMasterModel.Email;
                        objUser.PhoneNo = "";// model.userMasterModel.PhoneNo;
                        objUser.StateID = 1; //model.userMasterModel.State;
                        objUser.CityID = 1; //model.userMasterModel.City;
                        //objUser.Address = model.userMasterModel.Address;

                        //if (model.UserLogoImageFile != null)
                        //{
                        //    string FileExtension = Path.GetExtension(model.UserLogoImageFile.FileName);
                        //    objUser.UserLogo = Convert.ToString(objUser.ID) + "-" + "UserLogo" + FileExtension;
                        //    string UploadPath = Server.MapPath(SiteSettings.UserLogoImagePath);
                        //    string FilePath = UploadPath + objUser.UserLogo;
                        //    model.UserLogoImageFile.SaveAs(FilePath);
                        //}
                        _repositoryUser.Update(objUser);
                    }
                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult TeacherSubject(int id)
        {
            List<vwTeacherSubjectBinding> model = _repositoryTeacherSubject.GetAll().Where(x => x.TeacherId == id).ToList();
            if (model == null)
            {
                model = new List<vwTeacherSubjectBinding>();
            }
            ViewData["Data"] = _repositoryTeacher.GetById(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult TeacherSubject(IEnumerable<vwTeacherSubjectBinding> model)
        {
            FinanceEduEntities dbContext = new FinanceEduEntities();
            foreach (vwTeacherSubjectBinding item in model)
            {
                TeacherAndSubject objTAndS = dbContext.TeacherAndSubjects.Where(x => x.SubjectID == item.SubjectId && x.TeacherID == item.TeacherId).FirstOrDefault();
                if (item.Checked == true && objTAndS == null)
                {
                    TeacherAndSubject objNew = new TeacherAndSubject()
                    {
                        TeacherID = item.TeacherId,
                        SubjectID = item.SubjectId,
                        Created = DateTime.Now
                    };
                    dbContext.TeacherAndSubjects.Add(objNew);
                    dbContext.SaveChanges();
                }
                else if (item.Checked == false && objTAndS != null)
                {
                    dbContext.TeacherAndSubjects.Remove(objTAndS);
                    dbContext.SaveChanges();
                }
            }
            return Redirect("/TeacherMaster/TeacherSubject?id=" + model.Min(x => x.TeacherId));
        }
    }
}
