﻿using BLL;
using Core;
using DAL;
using FNSWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FNSWeb.Controllers
{
    public class HomeController : Controller
    {
        private IRepository<KioskMaster> _repositoryKiosk;
        private IRepository<tblUser> _repositoryUser;

        public HomeController()
        {
            this._repositoryKiosk = new Repository<KioskMaster>();
            this._repositoryUser = new Repository<tblUser>();
        }
        //
        // GET: /Home/
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Home/
        public ActionResult ContactUS()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ContactUS(ContactUSModel model)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    clsMail obj = new clsMail();

                    obj.From = model.Email;
                    obj.FromName = model.FirstName + " " + Convert.ToString(model.LastName);
                    obj.Subject = model.ContactSubject;
                    obj.MailBody = model.Enquiry + " and " + model.PhoneNumber;
                    obj.Send();

                    TempData["Success"] = "Request send Successfully!";

                    return RedirectToAction("ContactUS");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }

        //
        // GET: /Home/
        public ActionResult RegisterKiosk()
        {
            KioskMasterModel model = new KioskMasterModel();
            UserMasterModel objUser = new UserMasterModel();
            model.UserDetail = objUser;
            return View(model);

        }
        [HttpPost]
        public ActionResult RegisterKiosk(KioskMasterModel model)
        {
            ModelState.Remove("LatLong");
            ModelState.Remove("KioskRegistrationNO");
            if (ModelState.IsValid)
            {
                try
                {
                    //KioskMaster Exists = _repositoryKiosk.GetAll().Where(x => x.KioskregistrationNO == model.KioskRegistrationNO).FirstOrDefault();
                    //if (Exists == null)
                    //{
                    string[] saAllowedCharacters = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };

                    tblUser objUser = new tblUser();

                    objUser.FirstName = model.UserDetail.FirstName;
                    objUser.LastName = model.UserDetail.LastName;
                    objUser.Email = model.UserDetail.Email;
                    objUser.LoginPassword = CommonUtility.GenerateRandomOTP(8, saAllowedCharacters);

                    objUser.PhoneNo = model.UserDetail.PhoneNo;
                    objUser.StateID = model.UserDetail.State;
                    objUser.CityID = model.UserDetail.City;
                    objUser.CountryID = 1;
                    objUser.Address = model.UserDetail.Address;
                    objUser.Created = DateTime.Now;
                    objUser.UserTypeId = 2;
                    objUser.IsActive = false;
                    objUser.IsLoggedIn = false;

                    _repositoryUser.Insert(objUser);

                    KioskMaster objKioskMaster = new KioskMaster();

                    objKioskMaster.UserId = objUser.ID;
                    objKioskMaster.KioskName = model.KioskName;
                    objKioskMaster.KioskregistrationNO = "";//model.KioskRegistrationNO;
                    objKioskMaster.Address = model.Address;
                    objKioskMaster.Created = DateTime.Now;

                    _repositoryKiosk.Insert(objKioskMaster);

                    TempData["Success"] = "Kiosk successfully registered!";

                    KioskMasterModel BlankModel = new KioskMasterModel();
                    UserMasterModel objBlankUser = new UserMasterModel();
                    BlankModel.UserDetail = objBlankUser;
                    return View(BlankModel);
                    //}
                    //TempData["Success"] = "This registration already exists!";
                }
                catch
                {
                    TempData["Success"] = "Kiosk not registered, please contact support!";
                    return View(model);
                }
            }
            return View(model);
        }

        //
        // GET: /Home/
        public ActionResult StudentCorner()
        {
            return View();
        }
        //
        // GET: /Home/
        public ActionResult Faq()
        {
            return View();
        }
    }
}