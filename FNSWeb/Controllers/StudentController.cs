﻿using BLL;
using Core;
using DAL;
using FNSWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FNSWeb.Controllers
{
    [SessionAuthorize]
    public class StudentController : Controller
    {
        //
        // GET: /Student/
        private IRepository<tblStudent> _repository;

        public StudentController()
        {
            this._repository = new Repository<tblStudent>();
        }

        public ActionResult Index()
        {
            List<tblStudent> obj = _repository.GetAll().ToList();
            if (obj != null && Session["UserMaster"] != null)
            {
                if (((UserMaster)Session["UserMaster"]).UserTypeId == 2)
                {
                    int CreatedBy = ((UserMaster)Session["UserMaster"]).ID;
                    obj = obj.Where(x => x.CreatedBy == CreatedBy).ToList();
                }
            }

            return View(obj);
        }

        //
        // GET: /Student/Details/5
        public ActionResult Details(int id)
        {
            tblStudent objStudent = _repository.GetById(id);
            return View(objStudent);
        }

        //
        // GET: /Student/Create
        public ActionResult Create()
        {
            StudentViewModel model = new StudentViewModel();
            return View(model);
        }

        //
        // POST: /Student/Create
        [HttpPost]
        public ActionResult Create(StudentViewModel objStudent)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string[] saAllowedCharacters = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };

                    int CreatedBy = (Session["UserMaster"] != null ? ((UserMaster)Session["UserMaster"]).ID : 1);

                    tblStudent objStudentModel = new tblStudent();
                    objStudentModel.StudentName = objStudent.StudentName;
                    objStudentModel.MotherName = objStudent.MotherName;
                    objStudentModel.FatherName = objStudent.FatherName;
                    objStudentModel.MobileNo = objStudent.MobileNo;
                    objStudentModel.DOB = objStudent.DOB;
                    objStudentModel.MailingAddress = objStudent.MailingAddress;
                    objStudentModel.PermanentAddress = objStudent.PermanentAddress;
                    objStudentModel.AddressProofID = objStudent.AddressProofID;
                    objStudentModel.AddressProofText = objStudent.AddressProofText;
                    objStudentModel.CreatedBy = CreatedBy;
                    objStudentModel.UserName = Guid.NewGuid().ToString().Replace("-", "").Substring(12); //objStudent.UserName;
                    objStudentModel.Email = objStudent.Email;
                    objStudentModel.Password = CommonUtility.GenerateRandomOTP(8, saAllowedCharacters);
                    objStudentModel.IsActive = true;
                    objStudentModel.CityID = objStudent.CityID;
                    objStudentModel.StateID = objStudent.StateID;
                    objStudentModel.Created = DateTime.Now;
                    objStudentModel.StudentPic = objStudent.StudentPic;
                    objStudentModel.Gender = objStudent.Gender;

                    _repository.Insert(objStudentModel);
                    if (objStudent.StudentPicImageFile != null)
                    {
                        string FileExtension = Path.GetExtension(objStudent.StudentPicImageFile.FileName);
                        objStudentModel.StudentPic = Convert.ToString(objStudentModel.StudentID) + "-" + "StudentPic" + FileExtension;
                        string UploadPath = Server.MapPath(SiteSettings.StudentPicImagePath);
                        string FilePath = UploadPath + objStudentModel.StudentPic;
                        objStudent.StudentPicImageFile.SaveAs(FilePath);

                        _repository.Update(objStudentModel);
                    }

                    return RedirectToAction("Index");
                }
                return View(objStudent);
            }
            catch
            {
                return View(objStudent);
            }
        }

        public ActionResult GetStudentBatchDetail(int id)
        {
            StudentBatchViewModel model = new StudentBatchViewModel();
            return View(model);
        }
        //
        // GET: /Student/Edit/5
        public ActionResult Edit(int id)
        {
            tblStudent objStudent = _repository.GetById(id);
            StudentViewModel objStudentModel = new StudentViewModel();
            objStudentModel.StudentID = objStudent.StudentID;
            objStudentModel.StudentName = objStudent.StudentName;
            objStudentModel.MotherName = objStudent.MotherName;
            objStudentModel.FatherName = objStudent.FatherName;
            objStudentModel.MobileNo = objStudent.MobileNo;
            objStudentModel.DOB = objStudent.DOB;
            objStudentModel.MailingAddress = objStudent.MailingAddress;
            objStudentModel.PermanentAddress = objStudent.PermanentAddress;
            objStudentModel.AddressProofID = objStudent.AddressProofID;
            objStudentModel.AddressProofText = objStudent.AddressProofText;
            //objStudentModel.CreatedBy = Convert.ToString(objStudent.CreatedBy);
            //objStudentModel.UserName = objStudent.UserName;
            objStudentModel.Email = objStudent.Email;
            objStudentModel.Password = objStudent.Password;
            objStudentModel.IsActive = true;
            objStudentModel.CityID = objStudent.CityID;
            objStudentModel.StateID = objStudent.StateID;
            objStudentModel.Created = DateTime.Now;
            objStudentModel.StudentPic = objStudent.StudentPic;
            objStudentModel.Gender = objStudent.Gender;

            return View(objStudentModel);
        }

        //
        // POST: /Student/Edit/5
        [HttpPost]
        public ActionResult Edit(StudentViewModel objStudent)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    tblStudent objStudentModel = _repository.GetById(objStudent.StudentID);
                    objStudentModel.StudentName = objStudent.StudentName;
                    objStudentModel.MotherName = objStudent.MotherName;
                    objStudentModel.FatherName = objStudent.FatherName;
                    objStudentModel.MobileNo = objStudent.MobileNo;
                    objStudentModel.DOB = objStudent.DOB;
                    objStudentModel.MailingAddress = objStudent.MailingAddress;
                    objStudentModel.PermanentAddress = objStudent.PermanentAddress;
                    objStudentModel.AddressProofID = objStudent.AddressProofID;
                    objStudentModel.AddressProofText = objStudent.AddressProofText;
                    //objStudentModel.CreatedBy = Convert.ToInt32(1);
                    //objStudentModel.UserName = objStudent.UserName;
                    objStudentModel.Email = objStudent.Email;
                    objStudentModel.CityID = objStudent.CityID;
                    objStudentModel.StateID = objStudent.StateID;
                    objStudentModel.Gender = objStudent.Gender;
                    if (objStudent.StudentPicImageFile != null)
                    {
                        string FileExtension = Path.GetExtension(objStudent.StudentPicImageFile.FileName);
                        objStudentModel.StudentPic = Convert.ToString(objStudentModel.StudentID) + "-" + "StudentPic" + FileExtension;
                        string UploadPath = Server.MapPath(SiteSettings.StudentPicImagePath);
                        string FilePath = UploadPath + objStudentModel.StudentPic;
                        objStudent.StudentPicImageFile.SaveAs(FilePath);
                    }
                    _repository.Update(objStudentModel);

                    return RedirectToAction("Index");
                }
                return View(objStudent);
            }
            catch
            {
                return View(objStudent);
            }
        }

        //
        // GET: /Student/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Student/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
