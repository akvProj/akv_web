﻿using BLL;
using Core;
using DAL;
using FNSWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FNSWeb.Controllers
{
    [SessionAuthorize]
    public class CourseController : Controller
    {


        private IRepository<Course> _repositoryCourse;
        private IRepository<Subject> _repositorySubject;
        private IRepository<CourseAndSubject> _repositoryCourseAndSubject;
        private IRepository<vwCourseSubjectBinding> _repositoryViewCourseAndSubject;
        public CourseController()
        {
            this._repositoryCourse = new Repository<Course>();
            this._repositorySubject = new Repository<Subject>();
            this._repositoryCourseAndSubject = new Repository<CourseAndSubject>();
            this._repositoryViewCourseAndSubject = new Repository<vwCourseSubjectBinding>();

        }
       
        //
        // GET: /Course/
        public ActionResult Index()
        {
            List<Course> listObj = _repositoryCourse.GetAll().ToList();
            return View(listObj);
        }

        //
        // GET: /Course/Details/5
        public ActionResult Details(int id)
        {
            CourseViewModel model = new CourseViewModel();
            Course objCourse = _repositoryCourse.GetById(id);
            model.ID = objCourse.CourseId;
            model.CourseName = objCourse.CourseName;
            model.Description = objCourse.Description;
            model.CourseType = objCourse.CourseType.Name;
            model.CourseTimeinMonth = Convert.ToInt32(objCourse.CourseTimeinMonth);
            model.CoursePriceAmount = Convert.ToDecimal(objCourse.CoursePriceAmount);
            model.Created = Convert.ToDateTime(objCourse.Created);

            return View(model);
        }

        //
        // GET: /Course/Create
        public ActionResult Create()
        {
            CourseViewModel model = new CourseViewModel();
            return View(model);
        }

        //
        // POST: /Course/Create
        [HttpPost]
        public ActionResult Create(CourseViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Course objCourse = new Course();

                    objCourse.CourseName = model.CourseName;
                    objCourse.Description = model.Description;
                    objCourse.CourseTypeID = model.CourseTypeID;
                    objCourse.CoursePriceAmount = Convert.ToDecimal(model.CoursePriceAmount);
                    objCourse.CourseTimeinMonth = Convert.ToInt32(model.CourseTimeinMonth);
                    objCourse.Created = DateTime.Now;

                    _repositoryCourse.Insert(objCourse);

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }

        //
        // GET: /Course/Edit/5
        public ActionResult Edit(int id)
        {
            Course objCourse = _repositoryCourse.GetById(id);

            CourseViewModel model = new CourseViewModel();
            model.ID = objCourse.CourseId;
            model.CourseName = objCourse.CourseName;
            model.Description = objCourse.Description;
            model.CourseTypeID = Convert.ToInt32(objCourse.CourseTypeID);
            model.CourseType = objCourse.CourseType.Name;
            model.CourseTimeinMonth = Convert.ToInt32(objCourse.CourseTimeinMonth);
            model.CoursePriceAmount = Convert.ToDecimal(objCourse.CoursePriceAmount);
            model.Created = Convert.ToDateTime(objCourse.Created);

            return View(model);
        }

        //
        // POST: /Course/Edit/5
        [HttpPost]
        public ActionResult Edit(CourseViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Course objEdit = _repositoryCourse.GetById(model.ID);

                    objEdit.CourseName = model.CourseName;
                    objEdit.Description = model.Description;
                    objEdit.CourseTypeID = model.CourseTypeID;
                    objEdit.CoursePriceAmount = Convert.ToDecimal(model.CoursePriceAmount);
                    objEdit.CourseTimeinMonth = Convert.ToInt32(model.CourseTimeinMonth);
                    _repositoryCourse.Update(objEdit);

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }

        [HttpGet]
       public ActionResult CourseSubject(int id)
       {
           List<vwCourseSubjectBinding> model = _repositoryViewCourseAndSubject.GetAll().Where(x => x.CourseId == id).ToList();
           if (model == null)
           {
               model = new List<vwCourseSubjectBinding>();
           }
           ViewData["Data"] = _repositoryCourse.GetById(id);
           return View(model);
       }

       [HttpPost]
       public ActionResult CourseSubject(IEnumerable<vwCourseSubjectBinding> model)
       {
           FinanceEduEntities dbContext = new FinanceEduEntities();
           foreach (vwCourseSubjectBinding item in model)
           {
               CourseAndSubject objCAndS = dbContext.CourseAndSubjects.Where(x => x.SubjectID == item.SubjectId && x.CourseID == item.CourseId).FirstOrDefault();
               if (item.Checked == true && objCAndS == null)
               {
                   CourseAndSubject objNew = new CourseAndSubject()
                   {
                       CourseID = item.CourseId,
                       SubjectID = item.SubjectId,
                       Created = DateTime.Now
                   };
                   dbContext.CourseAndSubjects.Add(objNew);
                   dbContext.SaveChanges();
               }
               else if (item.Checked == false && objCAndS != null)
               {
                   dbContext.CourseAndSubjects.Remove(objCAndS);
                   dbContext.SaveChanges();
               }
           }
           return Redirect("/Course/CourseSubject?id=" + model.Min(x => x.CourseId));
       }

        //
        // GET: /Course/Create
        public ActionResult BindSubjecttoCourse(int id)
        {
            CourseViewModel model = new CourseViewModel();
            if (id > 0)
            {
                Course objCourse = _repositoryCourse.GetById(id);

                model.ID = objCourse.CourseId;
                model.CourseName = objCourse.CourseName;
                model.CourseType = objCourse.CourseType.Name;
                model.CourseTimeinMonth = Convert.ToInt32(objCourse.CourseTimeinMonth);
                model.CoursePriceAmount = Convert.ToDecimal(objCourse.CoursePriceAmount);
                model.Created = Convert.ToDateTime(objCourse.Created);

                List<Subject> listObj = _repositorySubject.GetAll().Where(x => x.IsActive == true).ToList();

                ViewData["AssociatedSubjectList"] = _repositoryCourseAndSubject.GetAll().Where(x => x.CourseID == objCourse.CourseId).ToArray();

                List<SubjectViewModel> lstSubjectViewModel = new List<SubjectViewModel>();

                if (listObj.Count > 0)
                {

                    foreach (Subject obj in listObj)
                    {
                        SubjectViewModel objSubjectViewModel = new SubjectViewModel();
                        objSubjectViewModel.ID = obj.ID;
                        objSubjectViewModel.Name = obj.Name;
                        lstSubjectViewModel.Add(objSubjectViewModel);
                    }

                }

                model.lstSubjectViewModel = lstSubjectViewModel;
            }
            return View(model);
        }
    }
}