﻿using BLL;
using Core;
using DAL;
using FNSWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FNSWeb.Controllers
{
    [SessionAuthorize]
    public class SubjectController : Controller
    {
        private IRepository<Subject> _repositorySubject;
        public SubjectController()
        {
            this._repositorySubject = new Repository<Subject>();
        }
        //
        // GET: /Subject/
        public ActionResult Index()
        {
            List<Subject> listObj = _repositorySubject.GetAll().ToList();
            return View(listObj);
        }

        //
        // GET: /Subject/Details/5
        public ActionResult Details(int id)
        {
            SubjectViewModel model = new SubjectViewModel();
            Subject objSubject = _repositorySubject.GetById(id);
            model.ID = objSubject.ID;
            model.Name = objSubject.Name;
            model.Description = objSubject.Description;
            model.IsActive = Convert.ToBoolean(objSubject.IsActive);
            model.Created = Convert.ToDateTime(objSubject.Created);

            return View(model);
        }

        //
        // GET: /Subject/Create
        public ActionResult Create()
        {
            SubjectViewModel model = new SubjectViewModel();
            return View(model);
        }

        //
        // POST: /Subject/Create
        [HttpPost]
        public ActionResult Create(SubjectViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Subject objSubject = new Subject();

                    objSubject.Name = model.Name;
                    objSubject.Description = model.Description;
                    objSubject.IsActive = model.IsActive;
                    objSubject.Created = DateTime.Now;

                    _repositorySubject.Insert(objSubject);

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }

        //
        // GET: /Subject/Edit/5
        public ActionResult Edit(int id)
        {
            Subject objSubject = _repositorySubject.GetById(id);

            SubjectViewModel model = new SubjectViewModel();
            model.ID = objSubject.ID;
            model.Name = objSubject.Name;
            model.Description = objSubject.Description;
            model.IsActive = Convert.ToBoolean(objSubject.IsActive);
            model.Created = Convert.ToDateTime(objSubject.Created);

            return View(model);
        }

        //
        // POST: /Subject/Edit/5
        [HttpPost]
        public ActionResult Edit(SubjectViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Subject objEdit = _repositorySubject.GetById(model.ID);

                    objEdit.Name = model.Name;
                    objEdit.Description = model.Description;
                    objEdit.IsActive = model.IsActive;

                    _repositorySubject.Update(objEdit);

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }
    }
}