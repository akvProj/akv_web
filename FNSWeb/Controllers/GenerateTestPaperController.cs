﻿using BLL;
using Core;
using DAL;
using FNSWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace FNSWeb.Controllers
{
    [SessionAuthorize]
    public class GenerateTestPaperController : Controller
    {
        private IRepository<GenerateTestPaper> _repositoryGenerateTestPaper;

        private IRepository<Course> _repositoryCourse;
        private IRepository<CourseType> _repositoryCourseType;
        private IRepository<Subject> _repositorySubject;
        private IRepository<Topic> _repositoryTopic;
        private IRepository<QuestionLevel> _repositoryQuestionLevel;
        private IRepository<TestType> _repositoryTestType;


        public GenerateTestPaperController()
        {
            this._repositoryGenerateTestPaper = new Repository<GenerateTestPaper>();

            this._repositoryCourse = new Repository<Course>();
            this._repositoryCourseType = new Repository<CourseType>();
            this._repositorySubject = new Repository<Subject>();
            this._repositoryTopic = new Repository<Topic>();
            this._repositoryQuestionLevel = new Repository<QuestionLevel>();
            this._repositoryTestType = new Repository<TestType>();
        }
        //
        // GET: /GenerateTestPaper/
        public ActionResult Index()
        {
            List<GenerateTestPaper> listObj = _repositoryGenerateTestPaper.GetAll().ToList();
            List<GenerateTestPaperViewModel> model = new List<GenerateTestPaperViewModel>();
            if (listObj.Count > 0)
            {
                foreach (GenerateTestPaper obj in listObj)
                {
                    GenerateTestPaperViewModel objGenerateTestPaperViewModel = new GenerateTestPaperViewModel();
                    objGenerateTestPaperViewModel.ID = obj.ID;
                    objGenerateTestPaperViewModel.Title = obj.Title;
                    objGenerateTestPaperViewModel.Description = obj.Description;
                    objGenerateTestPaperViewModel.QuestionLimit = Convert.ToInt32(obj.QuestionLimit);
                    objGenerateTestPaperViewModel.TestTimeinMinuts = Convert.ToInt32(obj.TestTimeinMinuts);
                    objGenerateTestPaperViewModel.Created = Convert.ToDateTime(obj.Created);

                    if (obj.CourseTypeID > 0)
                    {
                        CourseType objCourseType = _repositoryCourseType.GetAll().Where(x => x.ID == obj.CourseTypeID).FirstOrDefault();
                        objGenerateTestPaperViewModel.courseTypeName = (objCourseType == null ? "" : objCourseType.Name);
                    }
                    if (obj.CourseID > 0)
                    {
                        Course objCourse = _repositoryCourse.GetAll().Where(x => x.CourseId == obj.CourseID).FirstOrDefault();
                        objGenerateTestPaperViewModel.courseName = (objCourse == null ? "" : objCourse.CourseName);
                    }
                    if (obj.SubjectID > 0)
                    {
                        Subject objSubject = _repositorySubject.GetAll().Where(x => x.ID == obj.SubjectID).FirstOrDefault();
                        objGenerateTestPaperViewModel.subjectName = (objSubject == null ? "" : objSubject.Name);
                    }
                    if (obj.TopicID > 0)
                    {
                        Topic objTopic = _repositoryTopic.GetAll().Where(x => x.ID == obj.TopicID).FirstOrDefault();
                        objGenerateTestPaperViewModel.topicName = (objTopic == null ? "" : objTopic.Name);
                    }
                    if (obj.TestTypeID > 0)
                    {
                        TestType objTestType = _repositoryTestType.GetAll().Where(x => x.ID == obj.TestTypeID).FirstOrDefault();
                        objGenerateTestPaperViewModel.testTypeName = (objTestType == null ? "" : objTestType.Title);
                    }
                    if (obj.QuestionLevelID > 0)
                    {
                        QuestionLevel objQuestionLevel = _repositoryQuestionLevel.GetAll().Where(x => x.ID == obj.QuestionLevelID).FirstOrDefault();
                        objGenerateTestPaperViewModel.questionLevelName = (objQuestionLevel == null ? "" : objQuestionLevel.Title);
                    }

                    model.Add(objGenerateTestPaperViewModel);

                }
            }

            return View(model);
        }

        //
        // GET: /GenerateTestPaper/Details/5
        public ActionResult Details(int id)
        {
            GenerateTestPaper objGenerateTestPaper = _repositoryGenerateTestPaper.GetById(id);

            GenerateTestPaperViewModel model = new GenerateTestPaperViewModel();

            model.ID = objGenerateTestPaper.ID;
            model.Title = objGenerateTestPaper.Title;
            model.Description = objGenerateTestPaper.Description;
            model.QuestionLimit = Convert.ToInt32(objGenerateTestPaper.QuestionLimit);
            model.TestTimeinMinuts = Convert.ToInt32(objGenerateTestPaper.TestTimeinMinuts);
            model.Created = Convert.ToDateTime(objGenerateTestPaper.Created);

            if (objGenerateTestPaper.CourseTypeID > 0)
            {
                CourseType objCourseType = _repositoryCourseType.GetAll().Where(x => x.ID == objGenerateTestPaper.CourseTypeID).FirstOrDefault();
                model.courseTypeName = (objCourseType == null ? "" : objCourseType.Name);
            }
            if (objGenerateTestPaper.CourseID > 0)
            {
                Course objCourse = _repositoryCourse.GetAll().Where(x => x.CourseId == objGenerateTestPaper.CourseID).FirstOrDefault();
                model.courseName = (objCourse == null ? "" : objCourse.CourseName);
            }
            if (objGenerateTestPaper.SubjectID > 0)
            {
                Subject objSubject = _repositorySubject.GetAll().Where(x => x.ID == objGenerateTestPaper.SubjectID).FirstOrDefault();
                model.subjectName = (objSubject == null ? "" : objSubject.Name);
            }
            if (objGenerateTestPaper.TopicID > 0)
            {
                Topic objTopic = _repositoryTopic.GetAll().Where(x => x.ID == objGenerateTestPaper.TopicID).FirstOrDefault();
                model.topicName = (objTopic == null ? "" : objTopic.Name);
            }
            if (objGenerateTestPaper.TestTypeID > 0)
            {
                TestType objTestType = _repositoryTestType.GetAll().Where(x => x.ID == objGenerateTestPaper.TestTypeID).FirstOrDefault();
                model.testTypeName = (objTestType == null ? "" : objTestType.Title);
            }
            if (objGenerateTestPaper.QuestionLevelID > 0)
            {
                QuestionLevel objQuestionLevel = _repositoryQuestionLevel.GetAll().Where(x => x.ID == objGenerateTestPaper.QuestionLevelID).FirstOrDefault();
                model.questionLevelName = (objQuestionLevel == null ? "" : objQuestionLevel.Title);
            }

            return View(model);
        }

        //
        // GET: /GenerateTestPaper/Create
        public ActionResult Create()
        {
            GenerateTestPaperViewModel model = new GenerateTestPaperViewModel();
            return View(model);
        }

        //
        // POST: /GenerateTestPaper/Create
        [HttpPost]
        public ActionResult Create(GenerateTestPaperViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    GenerateTestPaper objGenerateTestPaper = new GenerateTestPaper();

                    objGenerateTestPaper.Title = model.Title;
                    objGenerateTestPaper.Description = model.Description;
                    objGenerateTestPaper.QuestionLimit = Convert.ToInt32(model.QuestionLimit);
                    objGenerateTestPaper.TestTimeinMinuts = Convert.ToInt32(model.TestTimeinMinuts);

                    objGenerateTestPaper.QuestionLevelID = model.QuestionLevelID;
                    objGenerateTestPaper.CourseID = model.CourseID;
                    objGenerateTestPaper.SubjectID = model.SubjectID;
                    objGenerateTestPaper.TopicID = model.TopicID;
                    objGenerateTestPaper.CourseTypeID = model.CourseTypeID;
                    //objGenerateTestPaper.TestTypeID = model.TestTypeID;


                    objGenerateTestPaper.Created = DateTime.Now;

                    _repositoryGenerateTestPaper.Insert(objGenerateTestPaper);

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }

        //
        // GET: /GenerateTestPaper/Edit/5
        public ActionResult Edit(int id)
        {
            GenerateTestPaper objGenerateTestPaper = _repositoryGenerateTestPaper.GetById(id);

            GenerateTestPaperViewModel model = new GenerateTestPaperViewModel();
            model.ID = objGenerateTestPaper.ID;
            model.Title = objGenerateTestPaper.Title;
            model.Description = objGenerateTestPaper.Description;
            model.QuestionLimit = Convert.ToInt32(objGenerateTestPaper.QuestionLimit);
            model.TestTimeinMinuts = Convert.ToInt32(objGenerateTestPaper.TestTimeinMinuts);

            model.QuestionLevelID = Convert.ToInt32(objGenerateTestPaper.QuestionLevelID);
            model.CourseID = Convert.ToInt32(objGenerateTestPaper.CourseID);
            model.SubjectID = Convert.ToInt32(objGenerateTestPaper.SubjectID);
            model.TopicID = Convert.ToInt32(objGenerateTestPaper.TopicID);
            model.CourseTypeID = Convert.ToInt32(objGenerateTestPaper.CourseTypeID);
            //model.TestTypeID = Convert.ToInt32(objGenerateTestPaper.TestTypeID);

            return View(model);
        }

        //
        // POST: /GenerateTestPaper/Edit/5
        [HttpPost]
        public ActionResult Edit(GenerateTestPaperViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    GenerateTestPaper objGenerateTestPaper = _repositoryGenerateTestPaper.GetById(model.ID);

                    objGenerateTestPaper.Title = model.Title;
                    objGenerateTestPaper.Description = model.Description;
                    objGenerateTestPaper.QuestionLimit = Convert.ToInt32(model.QuestionLimit);
                    objGenerateTestPaper.TestTimeinMinuts = Convert.ToInt32(model.TestTimeinMinuts);

                    objGenerateTestPaper.QuestionLevelID = model.QuestionLevelID;
                    objGenerateTestPaper.CourseID = model.CourseID;
                    objGenerateTestPaper.SubjectID = model.SubjectID;
                    objGenerateTestPaper.TopicID = model.TopicID;
                    objGenerateTestPaper.CourseTypeID = model.CourseTypeID;
                    //objGenerateTestPaper.TestTypeID = model.TestTypeID;

                    _repositoryGenerateTestPaper.Update(objGenerateTestPaper);

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }
    }
}