﻿using BLL;
using Core;
using DAL;
using FNSWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FNSWeb.Controllers
{
    [SessionAuthorize]
    public class NotificationMasterController : Controller
    {
        private IRepository<NotificationMaster> _repositoryNotificationMaster;
        public NotificationMasterController()
        {
            this._repositoryNotificationMaster = new Repository<NotificationMaster>();
        }

        //
        // GET: /NotificationMaster/
        public ActionResult Index()
        {
            List<NotificationMaster> listObj = _repositoryNotificationMaster.GetAll().ToList();
            return View(listObj);
        }

        //
        // GET: /NotificationMaster/Details/5
        public ActionResult Details(int id)
        {
            NotificationMasterViewModel model = new NotificationMasterViewModel();
            NotificationMaster objNotificationMaster = _repositoryNotificationMaster.GetById(id);
            model.ID = objNotificationMaster.ID;
            model.Title = objNotificationMaster.Title;
            model.Description = objNotificationMaster.Description;
            model.Image = Convert.ToString(objNotificationMaster.Image);

            NotificationTypeMaster objty = objNotificationMaster.NotificationTypeMaster;

            string ttl = (objNotificationMaster == null ? "null" : objNotificationMaster.NotificationTypeMaster.Title);
            NotificationTypeViewModel obj = new NotificationTypeViewModel();
            obj.Title = objNotificationMaster.NotificationTypeMaster.Title;
            model.notificationTypeViewModel = obj; //
            model.IsActive = Convert.ToBoolean(objNotificationMaster.IsActive);
            model.Created = Convert.ToDateTime(objNotificationMaster.Created);

            return View(model);

        }
        //
        // GET: /NotificationMaster/Create
        public ActionResult Create()
        {
            NotificationMasterViewModel model = new NotificationMasterViewModel();
            return View(model);
        }

        //
        // POST: /NotificationMaster/Create
        [HttpPost]
        public ActionResult Create(NotificationMasterViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    NotificationMaster objNotificationMaster = new NotificationMaster();

                    objNotificationMaster.Title = model.Title;
                    objNotificationMaster.Description = model.Description;
                    objNotificationMaster.NotificationTypeMasterID = Convert.ToInt32(model.NotificationTypeMasterID);
                    objNotificationMaster.IsActive = Convert.ToBoolean(model.IsActive);
                    objNotificationMaster.Created = DateTime.Now;

                    if (model.ImageFile != null)
                    {
                        string FileExtension = Path.GetExtension(model.ImageFile.FileName);
                        string gd = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10);
                        model.Image = "NotificationImage" + gd + FileExtension;
                        string UploadPath = Server.MapPath(SiteSettings.NotificationImagePath);
                        string FilePath = UploadPath + model.Image;
                        model.ImageFile.SaveAs(FilePath);
                        objNotificationMaster.Image = model.Image;
                    }
                    _repositoryNotificationMaster.Insert(objNotificationMaster);

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }

        //
        // GET: /NotificationMaster/Edit/5
        public ActionResult Edit(int id)
        {
            NotificationMaster objNotificationMaster = _repositoryNotificationMaster.GetById(id);

            NotificationMasterViewModel model = new NotificationMasterViewModel();
            model.ID = objNotificationMaster.ID;
            model.Title = objNotificationMaster.Title;
            model.Description = objNotificationMaster.Description;
            model.NotificationTypeMasterID = Convert.ToString(objNotificationMaster.NotificationTypeMasterID);
            model.Image = objNotificationMaster.Image;
            model.IsActive = Convert.ToBoolean(objNotificationMaster.IsActive);

            return View(model);
        }

        //
        // POST: /NotificationMaster/Edit/5
        [HttpPost]
        public ActionResult Edit(NotificationMasterViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    NotificationMaster objNotificationMaster = _repositoryNotificationMaster.GetById(model.ID);

                    objNotificationMaster.Title = model.Title;
                    objNotificationMaster.Description = model.Description;
                    objNotificationMaster.NotificationTypeMasterID = Convert.ToInt32(model.NotificationTypeMasterID);
                    objNotificationMaster.IsActive = Convert.ToBoolean(model.IsActive);

                    if (model.ImageFile != null)
                    {

                        string FileExtension = Path.GetExtension(model.ImageFile.FileName);
                        string gd = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10);
                        model.Image = "NotificationImage" + gd + FileExtension;
                        string UploadPath = Server.MapPath(SiteSettings.NotificationImagePath);
                        string FilePath = UploadPath + model.Image;
                        model.ImageFile.SaveAs(FilePath);

                        objNotificationMaster.Image = model.Image;
                    }

                    _repositoryNotificationMaster.Update(objNotificationMaster);

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }
    }
}